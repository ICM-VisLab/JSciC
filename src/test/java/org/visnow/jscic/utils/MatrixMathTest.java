/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 *
 * @author know
 */
public class MatrixMathTest
{

    /**
     * Test of matrixTranspose method, of class MatrixMath.
     */
    @Test
    public void testMatrixTranspose_floatArrArr()
    {
        float[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        float[][] expResult = {{1, 4, 7}, {2, 5, 8}, {3, 6, 9}};
        float[][] result = MatrixMath.matrixTranspose(A);
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of matrixTranspose method, of class MatrixMath.
     */
    @Test
    public void testMatrixTranspose_doubleArrArr()
    {
        double[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] expResult = {{1, 4, 7}, {2, 5, 8}, {3, 6, 9}};
        double[][] result = MatrixMath.matrixTranspose(A);
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of matrixMult method, of class MatrixMath.
     */
    @Test
    public void testMatrixMult_doubleArrArr_doubleArrArr()
    {
        double[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] B = {{1, 1, 1}, {0, 1, 1}, {0, 0, 1}};
        double[][] expResult = {{1, 3, 6}, {4, 9, 15}, {7, 15, 24}};
        double[][] result = MatrixMath.matrixMult(A, B);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of matrixMult method, of class MatrixMath.
     */
    @Test
    public void testMatrixMult_floatArrArr_floatArrArr()
    {
        float[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        float[][] B = {{1, 1, 1}, {0, 1, 1}, {0, 0, 1}};
        float[][] expResult = {{1, 3, 6}, {4, 9, 15}, {7, 15, 24}};
        float[][] result = MatrixMath.matrixMult(A, B);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of matrixAdd method, of class MatrixMath.
     */
    @Test
    public void testMatrixAdd()
    {
        double[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] B = {{1, 1, 1}, {0, 1, 1}, {0, 0, 1}};
        double[][] expResult = {{2, 3, 4}, {4, 6, 7}, {7, 8, 10}};
        double[][] result = MatrixMath.matrixAdd(A, B);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of matrixVectorMult method, of class MatrixMath.
     */
    @Test
    public void testFloatMatrixVectorMult()
    {
        float[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        float[] b = {0, 1, 2};
        float[] expResult = {8, 17, 26};
        float[] result = MatrixMath.matrixVectorMult(A, b);
        assertRelativeArrayEquals(expResult, result);
    }

    /**
     * Test of matrixVectorMult method, of class MatrixMath.
     */
    @Test
    public void testMatrixVectorMult()
    {
        double[][] A = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[] b = {0, 1, 2};
        double[] expResult = {8, 17, 26};
        double[] result = MatrixMath.matrixVectorMult(A, b);
        assertRelativeArrayEquals(expResult, result);
    }

    /**
     * Test of jacobi method, of class MatrixMath.
     */
    @Test
    public void testJacobi_4args_1()
    {
        Random random = new Random(0);
        int n = 6;
        float[][] a = new float[n][n];
        float[][] A = new float[n][n];
        float[] d = new float[n];
        float[][] v = new float[n][n];
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++)
                A[i][j] = a[i][j] = A[j][i] = a[j][i] = random.nextFloat();
        MatrixMath.jacobi(A, d, v, 1000);
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++) {
                float p = 0;
                for (int k = 0; k < n; k++)
                    p += v[i][k] * v[j][k];
                if (i == j && Math.abs(p - 1) > .00001 ||
                    i != j && Math.abs(p) > .00001)
                    fail("eigenvectors not orthonormal");
            }
        for (int i = 0; i < n; i++) {
            float[] w = new float[n];
            for (int j = 0; j < v.length; j++) {
                for (int k = 0; k < w.length; k++)
                    w[j] += a[j][k] * v[i][k];
                if (Math.abs(w[j] - d[i] * v[i][j]) >
                    .001 * (Math.abs(w[j]) + Math.abs(d[i] * v[i][j])))
                    fail("eigenvectors not eigen");
            }
        }
    }

    /**
     * Test of jacobi method, of class MatrixMath.
     */
    @Test
    public void testJacobi_4args_2()
    {
        Random random = new Random(0);
        int n = 6;
        double[][] a = new double[n][n];
        double[][] A = new double[n][n];
        double[] d = new double[n];
        double[][] v = new double[n][n];
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++)
                A[i][j] = a[i][j] = A[j][i] = a[j][i] = random.nextDouble();
        MatrixMath.jacobi(A, d, v, 1000);
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++) {
                double p = 0;
                for (int k = 0; k < n; k++)
                    p += v[i][k] * v[j][k];
                if (i == j && Math.abs(p - 1) > .000001 ||
                    i != j && Math.abs(p) > .000001)
                    fail("eigenvectors not orthonormal");
            }
        for (int i = 0; i < n; i++) {
            double[] w = new double[n];
            for (int j = 0; j < v.length; j++) {
                for (int k = 0; k < w.length; k++)
                    w[j] += a[j][k] * v[i][k];
                if (Math.abs(w[j] - d[i] * v[i][j]) >
                    .00001 * (Math.abs(w[j]) + Math.abs(d[i] * v[i][j])))
                    fail("eigenvectors not eigen");
            }
        }
    }

    /**
     * Test of transformAffine method, of class MatrixMath.
     */
    @Test
    public void testTransformAffine()
    {
        //TODO
    }

    /**
     * Test of lsolve method, of class MatrixMath.
     */
    @Test
    public void testLsolve_floatArrArr_floatArr()
    {
        Random random = new Random(0);
        int n = 6;
        float[][] A = new float[n][n];
        float[] expResult = new float[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                A[i][j] = random.nextFloat();
            expResult[i] = random.nextFloat();
        }
        float[] b = MatrixMath.matrixVectorMult(A, expResult);
        float[] result = MatrixMath.lsolve(A, b);
        assertRelativeArrayEquals(expResult, result);
    }

    /**
     * Test of lsolve method, of class MatrixMath.
     */
    @Test
    public void testLsolve_doubleArrArr_doubleArr()
    {
        Random random = new Random(0);
        int n = 6;
        double[][] A = new double[n][n];
        double[] expResult = new double[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                A[i][j] = random.nextDouble();
            expResult[i] = random.nextDouble();
        }
        double[] b = MatrixMath.matrixVectorMult(A, expResult);
        double[] result = MatrixMath.lsolve(A, b);
        assertRelativeArrayEquals(expResult, result);
    }

    /**
     * Test of lsolve3x3 method, of class MatrixMath.
     */
    @Test
    public void testLsolve3x3_floatArrArr_floatArr()
    {
        Random random = new Random(0);
        int n = 3;
        float[][] A = new float[n][n];
        float[] expResult = new float[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                A[i][j] = random.nextFloat();
            expResult[i] = random.nextFloat();
        }
        float[] b = MatrixMath.matrixVectorMult(A, expResult);
        float[] result = MatrixMath.lsolve3x3(A, b);
        assertRelativeArrayEquals(expResult, result);
    }

    /**
     * Test of lsolve3x3 method, of class MatrixMath.
     */
    @Test
    public void testLsolve3x3_doubleArrArr_doubleArr()
    {
        Random random = new Random(0);
        int n = 3;
        double[][] A = new double[n][n];
        double[] expResult = new double[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                A[i][j] = random.nextDouble();
            expResult[i] = random.nextDouble();
        }
        double[] b = MatrixMath.matrixVectorMult(A, expResult);
        double[] result = MatrixMath.lsolve3x3(A, b);
        assertRelativeArrayEquals(expResult, result);
    }

    /**
     * Test of invert method, of class MatrixMath.
     */
    @Test
    public void testInvert_floatArrArr_floatArrArr()
    {
        Random r = new Random(0);
        for (int n = 5; n < 10; n++) {
            float[][] A = new float[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++)
                    A[i][j] = r.nextFloat();
            }
            float[][] invA = new float[n][n];

            MatrixMath.invert(A, invA);

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    float out = 0;
                    for (int l = 0; l < n; l++)
                        out += A[i][l] * invA[l][j];
                    if (i == j && Math.abs(out - 1) > .00001 ||
                        i != j && Math.abs(out) > .00001)
                        fail("A x invA is not unit matrix");
                }
        }
    }

    /**
     * Test of invert method, of class MatrixMath.
     */
    @Test
    public void testInvert_doubleArrArr_doubleArrArr()
    {
        Random random = new Random(0);
        for (int n = 5; n < 10; n++) {
            double[][] A = new double[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++)
                    A[i][j] = random.nextDouble();
            }
            double[][] invA = new double[n][n];

            MatrixMath.invert(A, invA);

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    double out = 0;
                    for (int l = 0; l < n; l++)
                        out += A[i][l] * invA[l][j];
                    if (i == j && Math.abs(out - 1) > .000001 ||
                        i != j && Math.abs(out) > .000001)
                        fail("A x invA is not unit matrix");
                }
        }
    }

    /**
     * Test of matrixSqrt method, of class MatrixMath.
     */
    @Test
    public void testMatrixSqrt()
    {
        Random random = new Random(0);
        int n = 6;
        double[][] A = new double[n][n];
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++)
                A[i][j] = A[j][i] = random.nextFloat();
        for (int i = 0; i < n; i++)
            A[i][i] += 6;
        double[][] result = MatrixMath.matrixSqrt(A);
        double[][] r = MatrixMath.matrixMult(result, result);
        assertArrayEquals(A, r);
    }

    /**
     * Test of matrixSqrtInverse method, of class MatrixMath.
     */
    @Test
    public void testMatrixSqrtInverse()
    {
        Random random = new Random(0);
        int n = 6;
        double[][] A = new double[n][n];
        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++)
                A[i][j] = A[j][i] = random.nextFloat();
        for (int i = 0; i < n; i++)
            A[i][i] += 6;
        double[][] result = MatrixMath.matrixSqrtInverse(A);
        double[][] r = MatrixMath.matrixMult(result, result);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                double out = 0;
                for (int l = 0; l < n; l++)
                    out += A[i][l] * r[l][j];
                if (i == j && Math.abs(out - 1) > .0001 ||
                    i != j && Math.abs(out) > .0001)
                    fail("A x result^-2 is not unit matrix");
            }
    }

    void assertArrayEquals(float[][] expResult, float[][] result)
    {
        if (expResult.length != result.length)
            fail("Different sizes of compared arrays.");
        for (int k = 0; k < result.length; k++) {
            if (expResult[k].length != result[k].length)
                fail("Different sizes of compared arrays.");
            assertRelativeArrayEquals(expResult[k], result[k]);
        }
    }

    void assertArrayEquals(double[][] expResult, double[][] result)
    {
        if (expResult.length != result.length)
            fail("Different sizes of compared arrays.");
        for (int k = 0; k < result.length; k++) {
            if (expResult[k].length != result[k].length)
                fail("Different sizes of compared arrays.");
            assertRelativeArrayEquals(expResult[k], result[k]);
        }
    }

}

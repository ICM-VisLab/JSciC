/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.math.BigDecimal;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.visnow.jscic.utils.EngineeringFormattingUtils.formatToExponent;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class EngineeringFormattingUtilsTest
{

    public EngineeringFormattingUtilsTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testToPrecision()
    {
        String numberString;

        numberString = "1";
        assertEquals("1.000", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, true, -4, 6));

        numberString = "999.9";
        assertEquals("1000", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, true, -4, 6));
        assertEquals("1000", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, true, -4, 6));
        assertEquals("999.9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, true, -4, 6));
        assertEquals("999.9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, true, false, -4, 6));
        assertEquals("999.9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, true, -4, 6));
        assertEquals("999.9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, true, -4, 6));

        numberString = "99.9";
        assertEquals("99.9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, true, -4, 6));
        assertEquals("99.9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, true, -4, 6));
        assertEquals("100", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));

        numberString = "9.9999";
        assertEquals("10.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, true, -4, 6));
        assertEquals("10.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, true, -4, 6));
        assertEquals("10.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -4, 6));
        assertEquals("10", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("10.00", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, true, -4, 6));
        assertEquals("10.00", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, true, true, -4, 6));
        assertEquals("10.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, true, false, -4, 6));
        assertEquals("10", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, false, -4, 6));

        numberString = "990E6";
        assertEquals("990E6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("990.0E6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, false, -4, 6));
        assertEquals("1.0E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, true, false, -4, 6));
        assertEquals("1E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, false, false, -4, 6));
        assertEquals("1E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, false, true, -4, 6));

        numberString = "999E6";
        assertEquals("999E6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, true, -4, 6));

        numberString = "999999.123";
        assertEquals("999999.1", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 7, false, false, -4, 6));
        assertEquals("1E6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 5, false, false, -4, 6));
        assertEquals("1.0000E6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 5, false, true, -4, 6));

        numberString = "6.666E10";
        assertEquals("66.7E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("67E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));
        assertEquals("67E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, false, false, -4, 6));
        assertEquals("66.7E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, true, false, -4, 6));
        numberString = "4.444E10";
        assertEquals("44.4E9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));

        numberString = "666.666";
        assertEquals("666.67", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 5, false, false, -4, 6));
        assertEquals("666.7", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, false, -4, 6));
        assertEquals("667", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("667", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));
        assertEquals("667", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, false, false, -4, 6));

        numberString = "999e-5";
        assertEquals("0.01", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));
        assertEquals("0.00999", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));

        numberString = "0.00999";
        assertEquals("0.01", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));

        numberString = "999.2e-6";
        assertEquals("999E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("0.001", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));
        assertEquals("0.001", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, false, -4, 6));
        assertEquals("0.0010", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, true, -4, 6));
        assertEquals("0.0010", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, true, -4, 6));

        numberString = "999.9e-6";
        assertEquals("0.001", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("0.00100", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, true, -4, 6));

        numberString = "9e-4";
        assertEquals("900E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, false, false, -4, 6));
        assertEquals("900.0E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, true, false, -4, 6));
        assertEquals("900E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, true, -4, 6));

        numberString = "999.2e-9";
        assertEquals("1E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));

        numberString = "963.7e-9";
        assertEquals("1.0E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, true, true, -4, 6));
        assertEquals("964E-9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));

        numberString = "96.37e-9";
        assertEquals("96.4E-9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, false, -4, 6));

        numberString = "999.9E-9";
        assertEquals("999.9E-9", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, false, -4, 6));
        assertEquals("1E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("1.0E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -4, 6));
        assertEquals("1.00E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, true, -4, 6));

        numberString = "999e-9";
        assertEquals("1E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -4, 6));

        numberString = "67.89e-5";
        assertEquals("679E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -4, 6));
        assertEquals("678.9E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -4, 6));
    }

    @Test
    public void testToPrecisionNonStandardExponent()
    {
        String numberString;

        numberString = "99900000";
        assertEquals("99900000", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -3, 8));
        assertEquals("100E6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -3, 8));
        assertEquals("99900000.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, false, -3, 8));

        numberString = "0.0099999";
        assertEquals("9.9999E-3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 5, false, false, -3, 6));
        assertEquals("0.01", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 4, false, false, -3, 6));

        numberString = "0.0099";
        assertEquals("9.9E-3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, true, false, -3, 6));
        assertEquals("0.01", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, true, false, -3, 6));
        assertEquals("0.01", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 1, false, false, -3, 6));

        numberString = "99900";
        assertEquals("100E3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -3, 4));
        assertEquals("99.9E3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -3, 4));

        numberString = "9990";
        assertEquals("10E3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 2, false, false, -3, 4));
        assertEquals("9990", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, false, false, -3, 4));

        numberString = "0.000999";
        assertEquals("999.0E-6", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "0.00999";
        assertEquals("9.99E-3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "0.0999";
        assertEquals("99.9E-3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "0.999";
        assertEquals("999.0E-3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "9.99";
        assertEquals("9.99E0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "99.9";
        assertEquals("99.9E0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "999";
        assertEquals("999.0E0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "9990";
        assertEquals("9.99E3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
        numberString = "99900";
        assertEquals("99.9E3", EngineeringFormattingUtils.formatToPrecision(new BigDecimal(numberString), 3, true, false, -1, 0));
    }

    @Test
    public void testToExponent()
    {
        assertEquals("900.0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("9E8"), 9, true, true, -4, 6, 0));

        assertEquals("1E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("900E6"), 9, false, true, -4, 6, 0));
        assertEquals("900.0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("900E6"), 9, true, true, -4, 6, 0));
        assertEquals("0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("90E6"), 9, false, true, -4, 6, 0));
        assertEquals("90.0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("90E6"), 9, true, true, -4, 6, 0));
        assertEquals("0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("9E6"), 9, false, true, -4, 6, 0));
        assertEquals("0.0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("9E6"), 9, true, true, -4, 6, 0));

        assertEquals("0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("100E6"), 9, false, true, -4, 6, 0));
        assertEquals("100.0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("100E6"), 9, true, true, -4, 6, 0));
        assertEquals("0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("10E6"), 9, false, true, -4, 6, 0));
        assertEquals("0.0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("10E6"), 9, true, true, -4, 6, 0));
        assertEquals("0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("1E6"), 9, false, true, -4, 6, 0));
        assertEquals("0.0E9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("1E6"), 9, true, true, -4, 6, 0));

        assertEquals("7.6543210987654321099E27", EngineeringFormattingUtils.formatToExponent(new BigDecimal("7654321098765432109876543210.1234"), 8, true, false, -4, 6, 0));
        assertEquals("7.6543210987654321001E27", EngineeringFormattingUtils.formatToExponent(new BigDecimal("7654321098765432100076543210.1234"), 8, true, false, -4, 6, 0));
        assertEquals("7.6543210987654321E27", EngineeringFormattingUtils.formatToExponent(new BigDecimal("7654321098765432100006543210.1234"), 8, true, false, -4, 6, 0));
        assertEquals("7.6543210987654321000E27", EngineeringFormattingUtils.formatToExponent(new BigDecimal("7654321098765432100006543210.1234"), 8, false, true, -4, 6, 0));
        assertEquals("0.0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), 8, true, false, -4, 6, 0));
        assertEquals("0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), 8, false, false, -4, 6, 0));
        //this should be different but it's not because BigDecimal doesn't support ZEROs with larger precision than 1.
        assertEquals("0.0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), 8, true, false, -4, 6, 0));
        assertEquals("5628.5", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -1, false, false, -4, 6, 0));
        assertEquals("5628.5", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -1, true, false, -4, 6, 0));
        assertEquals("5628.45", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -2, false, false, -4, 6, 0));
        assertEquals("5628.455", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -3, false, false, -4, 6, 0));
        assertEquals("5628.4545", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -4, false, false, -4, 6, 0));
        assertEquals("5628.4545", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -5, false, false, -4, 6, 0));
        assertEquals("5628.45450", EngineeringFormattingUtils.formatToExponent(new BigDecimal("5628.4545"), -5, true, true, -4, 6, 0));

        assertEquals("0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.5628"), 8, false, false, -4, 6, 0));
        assertEquals("0E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.3628"), 8, false, false, -4, 6, 0));
        assertEquals("0.36282346", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.36282345612667857856847845854545"), -8, false, false, -4, 6, 0));
        assertEquals("1", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.9628"), -1, false, false, -4, 6, 0));
        assertEquals("1.0", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.9628"), -1, false, true, -4, 6, 0));
        assertEquals("1.0", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.9628"), -1, true, false, -4, 6, 0));
        assertEquals("962.8E-6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.00096282345612667857"), -7, true, false, -4, 6, 0));
        assertEquals("0.001", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.00099997345612667857"), -7, true, false, -4, 6, 0));
        assertEquals("0.0010000", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.00099997345612667857"), -7, true, true, -4, 6, 0));

        assertEquals("100.0000000000000000000000000E18", EngineeringFormattingUtils.formatToExponent(new BigDecimal("1e20"), -7, true, true, -4, 6, 0));
        assertEquals("100.00000E18", EngineeringFormattingUtils.formatToExponent(new BigDecimal("1e20"), 13, true, true, -4, 6, 0));

        assertEquals("999.9734561", EngineeringFormattingUtils.formatToExponent(new BigDecimal("999.973456126678578568"), -7, true, true, -4, 6, 0));
        assertEquals("999.9", EngineeringFormattingUtils.formatToExponent(new BigDecimal("999.94345"), 2, true, true, -4, 6, 0));
        assertEquals("1.0000E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("999973.456126"), 2, true, true, -4, 6, 0));
        assertEquals("911.7", EngineeringFormattingUtils.formatToExponent(new BigDecimal("911.67345"), 2, true, true, -4, 6, 0));
        assertEquals("0", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0"), 2, false, false, -4, 6, 0));
        assertEquals("0", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.0"), -2, false, false, -4, 6, 0));
        assertEquals("0.0", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.0"), -2, true, false, -4, 6, 0));
        assertEquals("0.00", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.0"), -2, true, true, -4, 6, 0));
        assertEquals("0.00", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.0"), -2, false, true, -4, 6, 0));

        assertEquals("999.0E-6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.000999"), -6, true, false, -4, 6, 0));
        assertEquals("0.00100", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.000999"), -5, true, true, -4, 6, 0));
        assertEquals("1.00E-6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.000000999"), -8, true, true, -4, 6, 0));
        assertEquals("1.0000E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("999999"), 2, true, true, -4, 6, 0));

        assertEquals("1", EngineeringFormattingUtils.formatToExponent(new BigDecimal("1.234"), 0, false, true, -4, 6, 0));
        assertEquals("1.2", EngineeringFormattingUtils.formatToExponent(new BigDecimal("1.234"), 0, true, true, -4, 6, 0));
        assertEquals("123456.1", EngineeringFormattingUtils.formatToExponent(new BigDecimal("123456.12"), 0, true, true, -4, 6, 0));
        assertEquals("5.6E-6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.00000555"), -5, true, true, -4, 6, 0));
        assertEquals("0.000", EngineeringFormattingUtils.formatToExponent(new BigDecimal("0.00000555"), -3, true, true, -4, 6, 0));
        assertEquals("100.00", EngineeringFormattingUtils.formatToExponent(new BigDecimal("99.999"), -2, true, true, -4, 6, 0));
        assertEquals("100000.0", EngineeringFormattingUtils.formatToExponent(new BigDecimal("99999.99"), -1, true, true, -4, 6, 0));
        assertEquals("1.0000000E6", EngineeringFormattingUtils.formatToExponent(new BigDecimal("999999.99"), -1, true, true, -4, 6, 0));
    }

    @Test
    public void testException()
    {
        Random r = new Random(123);
        double num = 0;
        int lse = 0;
        try {
            for (int i = 0; i < 10000; i++) {
                num = Math.pow(10, r.nextDouble() * 200 - 100);
                lse = r.nextInt() % 40 - 20;
                formatToExponent(new BigDecimal(Double.toString(num)), lse, true, false, -4, 6, 0);
            }
        } catch (Exception ex) {
            assertTrue(num + " " + lse + " " + ex.getMessage(), false);
        }
    }

    @Test
    public void testZeroes()
    {
        assertEquals("0.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal("0.0E-15"), -2, true, true, 6, 0));
        assertEquals("0.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal("0"), 3, true, false, -4, 0));
        assertEquals("0.0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal("0"), 3, true, false, -1, 0));
        assertEquals("0", EngineeringFormattingUtils.formatToPrecision(new BigDecimal("0"), 3, false, false, -1, 0));
    }
}

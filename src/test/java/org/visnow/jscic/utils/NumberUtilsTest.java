/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.visnow.jscic.utils.NumberUtils.*;

/**
 *
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class NumberUtilsTest
{

    public NumberUtilsTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testLog10()
    {
        assertEquals(1, floorLog10(toBigDecimal(10.0)));
        assertEquals(1, floorLog10(toBigDecimal(10.8)));
        assertEquals(1, floorLog10(toBigDecimal(99.999999999)));
        assertEquals(1, floorLog10(new BigDecimal("99.99999999999999999999999999999999999999999999999999")));

        assertEquals(0, floorLog10(new BigDecimal("1")));
        assertEquals(0, floorLog10(toBigDecimal(1)));
        assertEquals(0, floorLog10(new BigDecimal("9.999999999999")));
        assertEquals(0, floorLog10(toBigDecimal(1.01)));

        assertEquals(-1, floorLog10(new BigDecimal("0.1")));
        assertEquals(-1, floorLog10(toBigDecimal(0.999)));
        assertEquals(-1, floorLog10(new BigDecimal("0.9999999999999")));
        assertEquals(-1, floorLog10(toBigDecimal(0.1)));

        assertEquals(1, ceilLog10(toBigDecimal(10.0)));
        assertEquals(2, ceilLog10(toBigDecimal(10.8)));
        assertEquals(2, ceilLog10(toBigDecimal(99.999999999)));
        assertEquals(2, ceilLog10(new BigDecimal("99.99999999999999999999999999999999999999999999999999")));

        assertEquals(0, ceilLog10(new BigDecimal("1")));
        assertEquals(0, ceilLog10(toBigDecimal(1)));
        assertEquals(1, ceilLog10(new BigDecimal("9.999999999999")));
        assertEquals(1, ceilLog10(toBigDecimal(1.01)));

        assertEquals(-1, ceilLog10(new BigDecimal("0.1")));
        assertEquals(-1, ceilLog10(toBigDecimal(0.1)));
        assertEquals(0, ceilLog10(toBigDecimal(0.999)));
        assertEquals(0, ceilLog10(new BigDecimal("0.9999999999999")));
    }
}

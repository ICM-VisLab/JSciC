/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.math3.util.FastMath;
import static org.apache.commons.math3.util.FastMath.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import static org.visnow.jlargearrays.FloatingPointAsserts.*;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
@RunWith(value = Parameterized.class)
public class DataArrayStatisticsTest
{

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters()
    {
        final int[] threads = {1, 2, 8};

        final ArrayList<Object[]> parameters = new ArrayList<>();
        for (int i = 0; i < threads.length; i++) {
            parameters.add(new Object[]{threads[i]});
        }
        return parameters;
    }

    public DataArrayStatisticsTest(int nthreads)
    {
        ConcurrencyUtils.setNumberOfThreads(nthreads);
        if (nthreads > 1) {
            ConcurrencyUtils.setConcurrentThreshold(1);
        }
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testSum()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[][] res = DataArrayStatistics.sum(da);
        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        assertRelativeEquals(10.0, res[0][0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.sum(da);

        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += da.getDoubleElement(i)[0];
        }
        assertRelativeEquals(sum, res[0][0]);

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayStatistics.sum(da);

        assertEquals(1, res.length);
        assertEquals(3, res[0].length);
        double[] sumv = new double[3];
        for (int i = 0; i < n; i++) {
            double[] elem = da.getDoubleElement(i);
            for (int v = 0; v < 3; v++) {
                sumv[v] += elem[v];
            }
        }

        assertRelativeArrayEquals(sumv, res[0]);

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayStatistics.sum(da);
        assertEquals(3, res.length);
        assertEquals(2, res[0].length);

        double[][] expected = new double[3][2];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    expected[t][v] += a.getDouble(i * 2 + v);
                }
            }
            t++;
        }
        for (int i = 0; i < expected.length; i++) {
            assertRelativeArrayEquals(expected[i], res[i]);
        }
    }

    @Test
    public void testSumKahan()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[][] res = DataArrayStatistics.sumKahan(da);
        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        assertRelativeEquals(10.0, res[0][0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.sumKahan(da);

        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += da.getDoubleElement(i)[0];
        }
        assertRelativeEquals(sum, res[0][0]);

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayStatistics.sumKahan(da);

        assertEquals(1, res.length);
        assertEquals(3, res[0].length);
        double[] sumv = new double[3];
        for (int i = 0; i < n; i++) {
            double[] elem = da.getDoubleElement(i);
            for (int v = 0; v < 3; v++) {
                sumv[v] += elem[v];
            }
        }

        assertRelativeArrayEquals(sumv, res[0]);

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayStatistics.sumKahan(da);
        assertEquals(3, res.length);
        assertEquals(2, res[0].length);

        double[][] expected = new double[3][2];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    expected[t][v] += a.getDouble(i * 2 + v);
                }
            }
            t++;
        }
        for (int i = 0; i < expected.length; i++) {
            assertRelativeArrayEquals(expected[i], res[i]);
        }
    }

    @Test
    public void testAvg()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[][] res = DataArrayStatistics.avg(da);
        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        assertRelativeEquals(1.0, res[0][0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.avg(da);

        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += da.getDoubleElement(i)[0];
        }
        assertRelativeEquals(sum / n, res[0][0]);

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayStatistics.avg(da);

        assertEquals(1, res.length);
        assertEquals(3, res[0].length);
        double[] sumv = new double[3];
        for (int i = 0; i < n; i++) {
            double[] elem = da.getDoubleElement(i);
            for (int v = 0; v < 3; v++) {
                sumv[v] += elem[v];
            }
        }
        for (int v = 0; v < 3; v++) {
            sumv[v] /= n;
        }

        assertRelativeArrayEquals(sumv, res[0]);

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayStatistics.avg(da);
        assertEquals(3, res.length);
        assertEquals(2, res[0].length);

        double[][] expected = new double[3][2];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    expected[t][v] += a.getDouble(i * 2 + v);
                }
            }
            t++;
        }
        for (int i = 0; i < expected.length; i++) {
            for (int v = 0; v < 2; v++) {
                expected[i][v] /= n;
            }
            assertRelativeArrayEquals(expected[i], res[i]);
        }
    }

    @Test
    public void testAvgKahan()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[][] res = DataArrayStatistics.avgKahan(da);
        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        assertRelativeEquals(1.0, res[0][0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.avgKahan(da);

        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += da.getDoubleElement(i)[0];
        }
        assertRelativeEquals(sum / n, res[0][0]);

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayStatistics.avgKahan(da);

        assertEquals(1, res.length);
        assertEquals(3, res[0].length);
        double[] sumv = new double[3];
        for (int i = 0; i < n; i++) {
            double[] elem = da.getDoubleElement(i);
            for (int v = 0; v < 3; v++) {
                sumv[v] += elem[v];
            }
        }
        for (int v = 0; v < 3; v++) {
            sumv[v] /= n;
        }

        assertRelativeArrayEquals(sumv, res[0]);

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayStatistics.avgKahan(da);
        assertEquals(3, res.length);
        assertEquals(2, res[0].length);

        double[][] expected = new double[3][2];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    expected[t][v] += a.getDouble(i * 2 + v);
                }
            }
            t++;
        }
        for (int i = 0; i < expected.length; i++) {
            for (int v = 0; v < 2; v++) {
                expected[i][v] /= n;
            }
            assertRelativeArrayEquals(expected[i], res[i]);
        }
    }

    @Test
    public void testStd()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[][] res = DataArrayStatistics.std(da);
        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        assertRelativeEquals(0.0, res[0][0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.std(da);

        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        double sum = 0;
        double sum2 = 0;
        for (int i = 0; i < n; i++) {
            sum += da.getDoubleElement(i)[0];
            sum2 += da.getDoubleElement(i)[0] * da.getDoubleElement(i)[0];
        }
        assertRelativeEquals(sqrt(FastMath.max(0.0, (sum2 / n) - (sum / n) * (sum / n))), res[0][0]);

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayStatistics.std(da);

        assertEquals(1, res.length);
        assertEquals(3, res[0].length);
        double[] sumv = new double[3];
        double[] sumv2 = new double[3];
        for (int i = 0; i < n; i++) {
            double[] elem = da.getDoubleElement(i);
            for (int v = 0; v < 3; v++) {
                sumv[v] += elem[v];
                sumv2[v] += elem[v] * elem[v];
            }
        }
        for (int i = 0; i < sumv.length; i++) {
            assertRelativeEquals(sqrt(FastMath.max(0.0, (sumv2[i] / n) - (sumv[i] / n) * (sumv[i] / n))), res[0][i]);
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayStatistics.std(da);
        assertEquals(3, res.length);
        assertEquals(2, res[0].length);

        double[][] expected = new double[3][2];
        double[][] expected2 = new double[3][2];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    expected[t][v] += a.getDouble(i * 2 + v);
                    expected2[t][v] += a.getDouble(i * 2 + v) * a.getDouble(i * 2 + v);
                }
            }
            t++;
        }
        for (int i = 0; i < expected.length; i++) {
            for (int v = 0; v < 2; v++) {
                assertRelativeEquals(sqrt(FastMath.max(0.0, (expected2[i][v] / n) - (expected[i][v] / n) * (expected[i][v] / n))), res[i][v]);
            }
        }
    }

    @Test
    public void testStdKahan()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[][] res = DataArrayStatistics.stdKahan(da);
        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        assertRelativeEquals(0.0, res[0][0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.stdKahan(da);

        assertEquals(1, res.length);
        assertEquals(1, res[0].length);
        double sum = 0;
        double sum2 = 0;
        for (int i = 0; i < n; i++) {
            sum += da.getDoubleElement(i)[0];
            sum2 += da.getDoubleElement(i)[0] * da.getDoubleElement(i)[0];
        }
        assertRelativeEquals(sqrt(FastMath.max(0.0, (sum2 / n) - (sum / n) * (sum / n))), res[0][0]);

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayStatistics.stdKahan(da);

        assertEquals(1, res.length);
        assertEquals(3, res[0].length);
        double[] sumv = new double[3];
        double[] sumv2 = new double[3];
        for (int i = 0; i < n; i++) {
            double[] elem = da.getDoubleElement(i);
            for (int v = 0; v < 3; v++) {
                sumv[v] += elem[v];
                sumv2[v] += elem[v] * elem[v];
            }
        }
        for (int i = 0; i < sumv.length; i++) {
            assertRelativeEquals(sqrt(FastMath.max(0.0, (sumv2[i] / n) - (sumv[i] / n) * (sumv[i] / n))), res[0][i]);
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayStatistics.stdKahan(da);
        assertEquals(3, res.length);
        assertEquals(2, res[0].length);

        double[][] expected = new double[3][2];
        double[][] expected2 = new double[3][2];
        int t = 0;
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    expected[t][v] += a.getDouble(i * 2 + v);
                    expected2[t][v] += a.getDouble(i * 2 + v) * a.getDouble(i * 2 + v);
                }
            }
            t++;
        }
        for (int i = 0; i < expected.length; i++) {
            for (int v = 0; v < 2; v++) {
                assertRelativeEquals(sqrt(FastMath.max(0.0, (expected2[i][v] / n) - (expected[i][v] / n) * (expected[i][v] / n))), res[i][v]);
            }
        }
    }

    @Test
    public void testMin()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[] res = DataArrayStatistics.min(da);
        assertEquals(1, res.length);
        assertRelativeEquals(1.0, res[0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.min(da);

        assertEquals(1, res.length);
        double min = Double.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (da.getDoubleElement(i)[0] < min) {
                min = da.getDoubleElement(i)[0];
            }
        }
        assertRelativeEquals(min, res[0]);

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 1, "da");

        res = DataArrayStatistics.min(da);
        assertEquals(3, res.length);

        double[] expected = new double[3];
        int t = 0;
        for (Float time : timeSeries) {
            min = Double.MAX_VALUE;
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                double val = a.getDouble(i);
                if (val < min) {
                    min = val;
                }
            }
            expected[t++] = min;
        }
        for (int i = 0; i < expected.length; i++) {
            assertRelativeEquals(expected[i], res[i]);
        }
    }

    @Test
    public void testMax()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        double[] res = DataArrayStatistics.max(da);
        assertEquals(1, res.length);
        assertRelativeEquals(1.0, res[0]);

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayStatistics.max(da);

        assertEquals(1, res.length);
        double max = -Double.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (da.getDoubleElement(i)[0] > max) {
                max = da.getDoubleElement(i)[0];
            }
        }
        assertRelativeEquals(max, res[0]);

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 1, "da");

        res = DataArrayStatistics.max(da);
        assertEquals(3, res.length);

        double[] expected = new double[3];
        int t = 0;
        for (Float time : timeSeries) {
            max = -Double.MAX_VALUE;
            LargeArray a = td.getValue(time);
            for (int i = 0; i < n; i++) {
                double val = a.getDouble(i);
                if (val > max) {
                    max = val;
                }
            }
            expected[t++] = max;
        }
        for (int i = 0; i < expected.length; i++) {
            assertRelativeEquals(expected[i], res[i]);
        }
    }

    @Test
    public void testHistogram()
    {
        int n = 1000;
        int nbin = 256;
        long[] res = null;
        //----------------------------------------------------------------------------------------------------------------------------------------------------------    
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        res = DataArrayStatistics.histogram(da, 1, 1, nbin, true, null, true);
        assertEquals(nbin, res.length);
        assertEquals(n, res[0]);
        //----------------------------------------------------------------------------------------------------------------------------------------------------------    
        //veclen = 1 with no mask
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        double min = da.getMinValue();
        double max = da.getMaxValue();
        double binSize = (max - min) / nbin;
        res = DataArrayStatistics.histogram(da, min, max, nbin, true, null, true);
        long[] expRes = new long[nbin];
        LargeArray dta = da.getRawArray();
        for (int ii = 0; ii < n; ii++) {
            int bin = (int) ((dta.getDouble(ii) - min) / binSize);
            if (bin < 0 || bin > nbin - 1) {
                continue;
            }
            expRes[bin] += 1;
        }
        Assert.assertArrayEquals(expRes, res);
        //veclen = 1 with mask
        // the same da , dta, min, max, binSize
        //creating mask 
        LogicLargeArray mask = new LogicLargeArray(n, true);
        for (int ii = 0; ii < n; ii++) {
            mask.setBoolean_safe(ii, (ii > n / 2));
        }
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        ArrayList<LargeArray> maskSeries = new ArrayList<>(1);
        maskSeries.add(mask);
        TimeData timeMask = new TimeData(timeSeries, maskSeries, 0.0f);
        //calculations        
        res = DataArrayStatistics.histogram(da, min, max, nbin, true, timeMask, true);
        expRes = new long[nbin];

        for (int ii = 0; ii < n; ii++) {
            if (mask.getBoolean_safe(ii)) {
                int bin = (int) ((dta.getDouble(ii) - min) / binSize);
                if (bin < 0 || bin > nbin - 1) {
                    continue;
                }
                expRes[bin] += 1;
            }
        } //endfor
        Assert.assertArrayEquals(expRes, res);
        //----------------------------------------------------------------------------------------------------------------------------------------------------------        
        //time steps > 1, veclen = 1 with no mask
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData timeData = new TimeData(timeSeries, dataSeries, 0.0f);
        da = DataArray.create(timeData, 1, "da", "", null);
        min = da.getMinValue();
        max = da.getMaxValue();
        binSize = (max - min) / nbin;
        res = DataArrayStatistics.histogram(da, min, max, nbin, true, null, true);
        expRes = new long[nbin];
        for (int step = 0; step < da.getNFrames(); step++) {
            dta = timeData.getValues().get(step);
            for (int ii = 0; ii < n; ii++) {
                int bin = (int) ((dta.getDouble(ii) - min) / binSize);
                if (bin < 0 || bin > nbin - 1) {
                    continue;
                }
                expRes[bin] += 1;
            }
        }
        Assert.assertArrayEquals(expRes, res);
        //veclen = 1 with timeMask
        // the same da , timeSeries, min, max, binSize
        //creating timeMask 

        maskSeries = new ArrayList<>(3);
        for (int step = 0; step < 3; step++) {
            mask = new LogicLargeArray(n, true);
            for (int ii = 0; ii < n; ii++) {
                mask.setBoolean_safe(ii, (ii > (n / 2 + step * 100)));
            }
            maskSeries.add(mask);
        }
        timeMask = new TimeData(timeSeries, maskSeries, 0.0f);
        //calculations        
        res = DataArrayStatistics.histogram(da, min, max, nbin, true, timeMask, true);
        expRes = new long[nbin];
        for (int step = 0; step < da.getNFrames(); step++) {
            dta = timeData.getValues().get(step);
            mask = (LogicLargeArray) timeMask.getValues().get(step);
            for (int ii = 0; ii < n; ii++) {
                if (mask.getBoolean_safe(ii)) {
                    int bin = (int) ((dta.getDouble(ii) - min) / binSize);
                    if (bin < 0 || bin > nbin - 1) {
                        continue;
                    }
                    expRes[bin] += 1;
                }
            }
        }
        Assert.assertArrayEquals(expRes, res);
        //veclen = 1 with timeMask
        // the same da (with different currentTime), timeSeries, min, max, binSize
        //the same mask
        // computeGlobalHistogram == false; testing currentTime consistency
        da.setCurrentTime(1.0f);
        //calculations        
        res = DataArrayStatistics.histogram(da, min, max, nbin, true, timeMask, false);
        expRes = new long[nbin];

        dta = timeData.getCurrentValue();
        mask = (LogicLargeArray) timeMask.getValue(da.getCurrentTime());
        for (int ii = 0; ii < n; ii++) {
            if (mask.getBoolean_safe(ii)) {
                int bin = (int) ((dta.getDouble(ii) - min) / binSize);
                if (bin < 0 || bin > nbin - 1) {
                    continue;
                }
                expRes[bin] += 1;
            }
        }
        Assert.assertArrayEquals(expRes, res);

    }

}

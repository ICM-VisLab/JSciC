/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import java.awt.Color;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * check for correctness of writing of the category description array, parsing the resulting string and creation of the index array
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class CategoryDescriptionTest
{

    public CategoryDescriptionTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of toString method, of class CategoryDescription.
     */
    @Test
    public void testToString()
    {
        String expResult = "  6: aqq: 128 128 128 255";
        String result = "";
        try {
            CategoryDescription instance = new CategoryDescription(6, "aqq", Color.gray);
            result = instance.toString();
            assertEquals(expResult, result);
        } catch (Exception e) {
            fail(result + " != " + expResult);
        }
    }

    /**
     * Test of fromString method, of class CategoryDescription.
     */
    @Test
    public void testFromString()
    {
        CategoryDescription expResult = new CategoryDescription(6, "aqq", Color.gray);
        String s = "6: aqq: 128 128 128 255";
        try {
            CategoryDescription result = CategoryDescription.fromString(s);
            assertEquals(expResult, result);
        } catch (Exception e) {
            fail("result != " + expResult);
        }
    }

    /**
     * Test of createIndex method, of class CategoryDescription.
     */
    @Test
    public void testCreateIndex()
    {
        CategoryDescription[] ds = new CategoryDescription[]
        {
            CategoryDescription.fromString("1: aqq: 128 128 128 255"),
            CategoryDescription.fromString("3: aqq: 128 128 128 255"),
            CategoryDescription.fromString("4: aqq: 128 128 128 255"),
        };
        int[] expResult = new int[] {-1,0,-1,1,2};
        int[] result =    new int[] {};
        try {
            result = CategoryDescription.createIndex(ds);
            assertArrayEquals(expResult, result);
        } catch (Exception e) {
            fail("result != " + expResult);
        }
    }

}

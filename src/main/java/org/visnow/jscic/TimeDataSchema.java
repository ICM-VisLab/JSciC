/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Objects;
import org.visnow.jscic.dataarrays.DataArrayType;
import org.visnow.jscic.utils.ArrayUtils;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jscic.utils.InfinityAction;
import org.visnow.jscic.utils.NaNAction;

/**
 * Holds general information about time data.
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class TimeDataSchema implements java.io.Serializable
{

    private static final long serialVersionUID = 4917963138337338455L;

    /**
     * Enumeration used for testing if a time moment is within time range. EMPTY if no timesteps are present. BEFORE if the time parameter is before the
     * earliest stored moment. IN if time is within the range of available timesteps. AFTER - when after all the stored timesteps.
     */
    public static enum Position
    {

        BEFORE, IN, AFTER, EMPTY
    };

    /**
     * Type of data array.
     */
    private DataArrayType type;

    /**
     * Array holding moments in time.
     */
    private ArrayList<Float> timeSeries;

    /**
     * User specified data.
     */
    private ArrayList<String[]> userData;

    /**
     * Current moment in time.
     */
    private float currentTime = 0;

    /**
     * NaN action.
     */
    private NaNAction nanAction;

    /**
     * Infinity action.
     */
    private InfinityAction infinityAction;

    /**
     * Creates a new instance of TimeDataSchema.
     *
     * @param type        data array type
     * @param timeSeries  moments in time
     * @param currentTime current moment in time
     */
    public TimeDataSchema(DataArrayType type, ArrayList<Float> timeSeries, float currentTime)
    {
        this(type, timeSeries, currentTime, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of TimeDataSchema.
     *
     * @param type        data array type
     * @param timeSeries  moments in time
     * @param currentTime current moment in time
     * @param nanAction   NaN action
     * @param infAction   Infinity action
     */
    public TimeDataSchema(DataArrayType type, ArrayList<Float> timeSeries, float currentTime, NaNAction nanAction, InfinityAction infAction)
    {
        if (type == null || type == DataArrayType.FIELD_DATA_UNKNOWN) {
            throw new IllegalArgumentException("Unknown data type");
        }
        this.type = type;
        if (timeSeries == null) {
            throw new IllegalArgumentException("Time series argument cannot be null.");
        }
        if (!ArrayUtils.isSorted(timeSeries)) {
            throw new IllegalArgumentException("The time series list is not sorted.");
        }
        this.timeSeries = timeSeries;
        this.currentTime = currentTime;
        this.nanAction = nanAction;
        this.infinityAction = infAction;
        this.userData = new ArrayList<>(this.timeSeries.size());
        for (int i = 0; i < this.timeSeries.size(); i++) {
            this.userData.add(null);
        }
    }

    /**
     * Creates a new instance of TimeDataSchema.
     *
     * @param type        data array type
     * @param timeSeries  moments in time
     * @param currentTime current moment in time
     * @param nanAction   NaN action
     * @param infAction   Infinity action
     * @param userData    user specified data
     */
    public TimeDataSchema(DataArrayType type, ArrayList<Float> timeSeries, float currentTime, NaNAction nanAction, InfinityAction infAction, ArrayList<String[]> userData)
    {
        if (type == null || type == DataArrayType.FIELD_DATA_UNKNOWN) {
            throw new IllegalArgumentException("Unknown data type");
        }
        this.type = type;
        if (timeSeries == null) {
            throw new IllegalArgumentException("Time series argument cannot be null.");
        }
        if (!ArrayUtils.isSorted(timeSeries)) {
            throw new IllegalArgumentException("The time series list is not sorted.");
        }
        this.timeSeries = timeSeries;
        this.currentTime = currentTime;
        this.nanAction = nanAction;
        this.infinityAction = infAction;
        if (userData == null) {
            throw new IllegalArgumentException("User data argument cannot be null.");
        }
        if (this.timeSeries.size() != userData.size()) {
            throw new IllegalArgumentException("The size of time series and user data must be the same.");
        }
        this.userData = userData;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof TimeDataSchema)) {
            return false;
        }
        TimeDataSchema tdc = (TimeDataSchema) o;
        boolean equal = type == tdc.type && currentTime == tdc.currentTime && nanAction == tdc.nanAction && infinityAction == tdc.infinityAction;
        if (equal == false) {
            return false;
        }
        if (!timeSeries.equals(tdc.timeSeries)) {
            return false;
        }
        return userData.equals(tdc.userData);
    }

    @Override
    public int hashCode()
    {
        int fprint = 7;
        fprint = 53 * fprint + Objects.hashCode(type);
        fprint = 53 * fprint + Objects.hashCode(currentTime);
        fprint = 53 * fprint + Objects.hashCode(infinityAction);
        fprint = 53 * fprint + Objects.hashCode(nanAction);
        fprint = 53 * fprint + Objects.hashCode(timeSeries);
        return 53 * fprint + Objects.hashCode(userData);
    }

    /**
     * Returns true if time series is empty, false otherwise
     *
     * @return true if time series is empty, false otherwise
     */
    public boolean isEmpty()
    {
        return timeSeries.isEmpty();
    }

    /**
     * Returns the type of data array.
     *
     * @return type of data array
     */
    public DataArrayType getType()
    {
        return type;
    }

    /**
     * Returns current moment in time.
     *
     * @return current moment in time
     */
    public float getCurrentTime()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty.");
        }
        return currentTime;
    }

    /**
     * Returns all times steps as list. This method always returns a reference to the internal list.
     *
     * @return all times steps as list
     */
    public ArrayList<Float> getTimesAsList()
    {
        return timeSeries;
    }

    /**
     * Returns all times steps as array.
     *
     * @return all times steps as array
     */
    public float[] getTimesAsArray()
    {
        float[] out = new float[timeSeries.size()];
        for (int i = 0; i < out.length; i++) {
            out[i] = timeSeries.get(i);
        }
        return out;
    }

    /**
     * Returns the first time step.
     *
     * @return first time step
     */
    public float getStartTime()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        return timeSeries.get(0);
    }

    /**
     * Returns the last time step.
     *
     * @return last time step
     */
    public float getEndTime()
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        return timeSeries.get(timeSeries.size() - 1);
    }

    /**
     * Returns the time step corresponding to a given position in the time series array.
     *
     * @param frame position in the time series array
     *
     * @return time step corresponding to a given position in the time series array
     */
    public float getTime(int frame)
    {
        if (isEmpty()) {
            throw new IllegalArgumentException("Time data is empty");
        }

        if (frame < 0) {
            frame = 0;
        }
        if (frame > timeSeries.size() - 1) {
            frame = timeSeries.size() - 1;
        }

        return timeSeries.get(frame);
    }

    /**
     * Returns true if a given moment is a time step, false otherwise
     *
     * @param t moment in time
     *
     * @return true if a given moment is a time step, false otherwise
     */
    public boolean isTimestep(float t)
    {
        for (int i = 0; i < timeSeries.size(); i++) {
            if (t == timeSeries.get(i)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a position in the time series array corresponding to a given moment. Returns -1 if time series is empty or a given moment is not a time step.
     *
     * @param time moment in time
     *
     * @return a position in the time series array corresponding to a given moment
     */
    public int getStep(float time)
    {
        if (getTimesAsList().isEmpty()) {
            return -1;
        }
        for (int i = 0; i < getTimesAsList().size(); i++) {
            if (getTimesAsList().get(i) == time) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Set the current moment in time to a given value.
     *
     * @param time new value of current moment in time
     *
     */
    void setCurrentTime(float time)
    {
        currentTime = time;
    }

    /**
     * Adds the specified time moment to the end of time series.
     *
     * @param time time moment
     */
    void addTime(float time)
    {
        if (isEmpty()) {
            timeSeries.add(time);
            userData.add(null);
        } else if (getEndTime() < time) {
            timeSeries.add(time);
            userData.add(null);
        } else {
            throw new IllegalArgumentException("Invalid value of time argument.");
        }
    }

    /**
     * Adds the specified time moment at the specified index.
     *
     * @param frame position in the time series array
     * @param time  time moment
     */
    void addTime(int frame, float time)
    {
        if ((frame > 0 && frame < timeSeries.size() && timeSeries.get(frame-1) < time && time < timeSeries.get(frame)) || (frame == 0 && time < timeSeries.get(0))) {
            timeSeries.add(frame, time);
            userData.add(frame, null);
        } else {
            throw new IllegalArgumentException("Invalid value of frame or time arguments.");
        }
    }

    /**
     * Removes the specified time moment from time series.
     *
     * @param time time moment
     */
    void removeTime(float time)
    {
        if (isTimestep(time)) {
            timeSeries.remove(getStep(time));
            userData.remove(getStep(time));
        } else {
            throw new IllegalArgumentException("Specified time moment is not a time step.");
        }
    }

    /**
     * Checks if a given time moment is within time range.
     *
     * @param time moment in time
     *
     * @return EMPTY if no timesteps are present, BEFORE if the time parameter is before the earliest stored moment, IN if time is within the range of available
     *         timesteps, AFTER - when after all the stored timesteps
     */
    public Position getTimePosition(float time)
    {
        if (timeSeries.isEmpty()) {
            return Position.EMPTY;
        }
        if (time < timeSeries.get(0)) {
            return Position.BEFORE;
        } else if (time > timeSeries.get(timeSeries.size() - 1)) {
            return Position.AFTER;
        } else {
            return Position.IN;
        }
    }

    /**
     * Returns not a number action
     *
     * @return not a number action
     */
    public NaNAction getNanAction()
    {
        return nanAction;
    }

    /**
     * Sets not a number action
     *
     * @param nanAction a new not a number action
     */
    void setNanAction(NaNAction nanAction)
    {
        this.nanAction = nanAction;
    }

    /**
     * Returns infinity action.
     *
     * @return infinity action
     */
    public InfinityAction getInfinityAction()
    {
        return infinityAction;
    }

    /**
     * Sets infinity action.
     *
     * @param infinityAction infinity action
     */
    void setInfinityAction(InfinityAction infinityAction)
    {
        this.infinityAction = infinityAction;
    }

    /**
     * Returns the user data.
     *
     * @return user data
     */
    public ArrayList<String[]> getUserData()
    {
        return userData;
    }

    /**
     * Returns the value of user data at specified moment in time.
     *
     * @param time moment in time
     *
     * @return the value of user data at specified moment in time
     */
    public String[] getUserData(float time)
    {
        if (isTimestep(time)) {
            return this.userData.get(getStep(time));
        } else {
            return null;
        }
    }

    /**
     * Set the user data.
     *
     * @param userData new user data
     */
    public void setUserData(ArrayList<String[]> userData)
    {
        if (userData == null || userData.size() != timeSeries.size()) {
            throw new IllegalArgumentException("userData == null || userData.size() != timeSeries.size()");
        }
        this.userData = userData;
    }

    /**
     * Set the value of user data at specified moment in time.
     *
     * @param userData new value of user data at specified moment in time
     * @param time     moment in time
     */
    public void setUserData(String[] userData, float time)
    {
        if (isTimestep(time)) {
            this.userData.set(getStep(time), userData);
        } else {
            throw new IllegalArgumentException("Invalid time " + time);
        }
    }

    /**
     * Compares two TimeDataSchemas. Returns true if data types and all time moments are equal in this schema and the input schema, false otherwise.
     *
     * @param	s time data schema to be compared
     *
     * @return	true if data types and all time moments are equal in both schemas, false otherwise.
     */
    public boolean isCompatibleWith(TimeDataSchema s)
    {
        if (s == null) {
            return false;
        }
        return type == s.type && timeSeries.equals(s.timeSeries);
    }

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public TimeDataSchema cloneDeep()
    {
        ArrayList<Float> timeSeriesClone = new ArrayList<>(timeSeries.size());
        for (int i = 0; i < timeSeries.size(); i++) {
            timeSeriesClone.add(timeSeries.get(i));
        }
        ArrayList<String[]> userDataClone = new ArrayList<>(userData.size());
        for (int i = 0; i < userData.size(); i++) {
            userDataClone.add(userData.get(i) != null ? userData.get(i).clone() : null);
        }
        return new TimeDataSchema(type, timeSeriesClone, currentTime, nanAction, infinityAction, userDataClone);
    }

    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException
    {
        // perform the default de-serialization first
        aInputStream.defaultReadObject();

        if (userData == null) {
            userData = new ArrayList<>(timeSeries.size());
            for (int i = 0; i < timeSeries.size(); i++) {
                userData.add(null);
            }
        }
    }
}

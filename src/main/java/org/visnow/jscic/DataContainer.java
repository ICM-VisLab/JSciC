/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;

/**
 * Abstract data container. Holds a list of DataArrays (components).
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public abstract class DataContainer implements java.io.Serializable
{

    private static final long serialVersionUID = 1126292088461510116L;

    /**
     * List of components.
     */
    protected ArrayList<DataArray> components = new ArrayList<>();

    /**
     * Schema of this container.
     */
    protected DataContainerSchema schema = new DataContainerSchema("Data container");

    /**
     * Current timestamp.
     */
    protected long timestamp = 0;

    /**
     * Current time step.
     */
    protected float currentTime = 0;

    @Override
    public int hashCode()
    {
        return hashCode(1f);
    }

    /**
     * Generates an approximate hash code using a quality argument. For quality equal to 1 this method is equivalent to hashCode().
     *
     * @param quality a number between 0 and 1.
     *
     * @return an approximate hash code
     */
    public int hashCode(float quality)
    {
        if (quality < 0 || quality > 1) {
            throw new IllegalArgumentException("The quality argument should be between 0 and 1");
        }
        int fprint = 3;
        fprint = 89 * fprint + Objects.hashCode(this.currentTime);
        fprint = 89 * fprint + Objects.hashCode(this.schema);
        for (DataArray da : this.components) {
            fprint = 89 * fprint + da.hashCode(quality);
        }
        return fprint;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof DataContainer))
            return false;
        DataContainer dc = (DataContainer) o;
        if (this.currentTime != dc.currentTime) {
            return false;
        }
        if (!this.schema.equals(dc.schema)) {
            return false;
        }
        if (!this.components.equals(dc.components)) {
            return false;
        }
        return true;
    }

    /**
     * Returns a shallow copy of this instance.
     *
     * @return a shallow copy of this instance
     */
    public abstract DataContainer cloneShallow();

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public abstract DataContainer cloneDeep();

    /**
     * Returns the name of the container.
     *
     * @return name of the container
     */
    public String getName()
    {
        return schema.getName();
    }

    /**
     * Sets the name of the container.
     *
     * @param name new name of the container
     */
    public void setName(String name)
    {
        schema.setName(name);
        timestamp = System.nanoTime();
    }

    /**
     * Returns the number of components stored in this DataContainer.
     *
     * @return number of components stored in this DataContainer
     */
    public int getNComponents()
    {
        return components.size();
    }

    /**
     * Returns true if a given DataContainer is compatible with this DataContainer,
     * false otherwise.
     *
     * @param dc DataContainer
     *
     * @return true if a given DataContainer is compatible with this DataContainer,
     *         false otherwise
     */
    public boolean isCompatibleWith(DataContainer dc)
    {
        return schema.isCompatibleWith(dc.getSchema());
    }

    /**
     * Returns true if this DataContainer does not have any components, false otherwise.
     *
     * @return true if this DataContainer does not have any components, false otherwise
     */
    public boolean isEmpty()
    {
        return components.size() < 1;
    }

    /**
     * Get all times steps from data.
     *
     * @return all times steps from data
     */
    public float[] getTimesteps()
    {
        Set<Float> tSteps = new HashSet<>();
        for (DataArray da : components) {
            for (Float t : da.getTimeSeries()) {
                tSteps.add(t);
            }
        }
        float[] tStep = new float[tSteps.size()];
        int i = 0;
        for (Float t : tSteps) {
            tStep[i] = t;
            i += 1;
        }
        Arrays.sort(tStep);
        return tStep;
    }

    /**
     * Returns true if data are time dependent, false otherwise
     *
     * @return true if data are time dependent, false otherwise
     */
    public boolean isTimeDependant()
    {
        for (DataArray da : components) {
            if (da.isTimeDependant()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the number of elements in each component.
     *
     * @return number of elements in each component
     */
    public long getNElements()
    {
        return schema.getNElements();
    }

    /**
     * Returns the current time step.
     *
     * @return current time step
     */
    public float getCurrentTime()
    {
        return currentTime;
    }

    /**
     * Sets the current time step.
     *
     * @param currentTime the value of current time step
     */
    public void setCurrentTime(float currentTime)
    {
        this.currentTime = currentTime;
        for (DataArray dataArray : components) {
            dataArray.setCurrentTime(currentTime);
        }
        timestamp = System.nanoTime();
    }

    /**
     * Returns container's schema.
     *
     * @return container's schema
     */
    public DataContainerSchema getSchema()
    {
        return schema;
    }

    /**
     * Sets container's schema.
     *
     * @param schema new container's schema
     */
    public void setSchema(DataContainerSchema schema)
    {
        if (schema == null) throw new IllegalArgumentException("Data container schema cannot be null");
        if (this.components.size() > 0 && !this.schema.isCompatibleWith(schema, true, true))
            throw new IllegalArgumentException("The new schema is not compatible with the existing schema.");
        this.schema = schema;
    }

    /**
     * Returns current timestamp.
     *
     * @return current timestamp
     */
    public long getTimestamp()
    {
        return timestamp;
    }

    /**
     * Sets the System.nanoTime() as the value of the timestamp
     */
    public void updateTimestamp()
    {
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if the DataArray changed since the give timestamp, false otherwise.
     *
     * @param timestamp timestamp
     *
     * @return true if the DataArray changed since the given timestamp, false otherwise
     */
    public boolean changedSince(long timestamp)
    {
        return this.timestamp > timestamp;
    }

    /**
     * Returns the component with a specified name.
     *
     * @param s name of the component
     *
     * @return component with a specified name
     */
    public DataArray getComponent(String s)
    {
        for (DataArray dataArray : components)
            if (dataArray.getName().equals(s))
                return dataArray;
        return null;
    }

    /**
     * Returns the component at a specified index.
     *
     * @param i index
     *
     * @return component at a specified index
     */
    public DataArray getComponent(int i)
    {
        if (i < 0 || i >= components.size())
            return null;
        return components.get(i);
    }

    /**
     * Returns all components.
     *
     * @return all components
     */
    public ArrayList<DataArray> getComponents()
    {
        return components;
    }

    /**
     * Simple cumulated schema view for all (or filtered) DataArrays within this DataContainer.
     */
    public static class ComponentSimpleView
    {

        //component names
        public String[] componentNames;
        //and their corresponding preferred phys min/max ranges (double[][2])
        public double[][] preferredPhysMinMaxRanges;
        //along with their corresponding time ranges (float[][2])
        public float[][] timeRanges;

        /**
         * Creates new instance of ComponentSimpleView
         *
         * @param componentNames            names of components
         * @param preferredPhysMinMaxRanges preferred min/max ranges
         * @param timeRanges                time ranges
         */
        public ComponentSimpleView(String[] componentNames, double[][] preferredPhysMinMaxRanges, float[][] timeRanges)
        {
            this.componentNames = componentNames;
            this.preferredPhysMinMaxRanges = preferredPhysMinMaxRanges;
            this.timeRanges = timeRanges;
        }
    }

    /**
     * Returns simple cumulated components schema view.
     *
     * @param numericOnly if true, then only numeric components are returned
     * @param scalarOnly  if true, then only scalar components are returned
     *
     * @return components schema view
     */
    public ComponentSimpleView getComponentSimpleView(boolean numericOnly, boolean scalarOnly)
    {
        List<String> componentNames = new ArrayList<>();
        List<double[]> componentMinMaxRanges = new ArrayList<>();
        List<float[]> componentTimeRanges = new ArrayList<>();

        for (DataArray component : components) {
            DataArraySchema s = component.getSchema();
            if ((!numericOnly || s.isNumeric()) && (!scalarOnly || s.getVectorLength() == 1)) {
                componentNames.add(s.getName());
                componentMinMaxRanges.add(new double[]{s.getPreferredPhysMinValue(), s.getPreferredPhysMaxValue()});
                if (component.getTimeData() == null)
                    componentTimeRanges.add(new float[]{0, 0});
                else
                    componentTimeRanges.add(new float[]{component.getStartTime(), component.getEndTime()});
            }
        }

        return new ComponentSimpleView(componentNames.toArray(new String[0]),
                                       componentMinMaxRanges.toArray(new double[0][]),
                                       componentTimeRanges.toArray(new float[0][]));
    }

    /**
     * Returns the names of the components.
     *
     * @return names of the components
     */
    public String[] getComponentNames()
    {
        String[] names = new String[components.size()];
        for (int i = 0; i < names.length; i++) {
            names[i] = components.get(i).getName();
        }
        return names;
    }

    /**
     * Returns the vector lengths of the components.
     *
     * @return vector lengths of the components
     */
    public int[] getComponentVectorLengths()
    {
        int[] vecLenS = new int[components.size()];
        for (int i = 0; i < components.size(); i++)
            vecLenS[i] = components.get(i).getVectorLength();
        return vecLenS;
    }

    /**
     * Returns the names of numeric components.
     *
     * @return names of numeric components
     */
    public String[] getNumericComponentNames()
    {
        List<String> names = new ArrayList<>();
        for (DataArray da : components)
            if (da.isNumeric()) names.add(da.getName());
        return names.toArray(new String[names.size()]);
    }

    /**
     * Returns the names of scalar components.
     *
     * @return names of scalar components
     */
    public String[] getScalarComponentNames()
    {
        List<String> names = new ArrayList<>();
        for (DataArray da : components)
            if (da.isNumeric() && da.getVectorLength() == 1) names.add(da.getName());
        return names.toArray(new String[names.size()]);
    }

    /**
     * Returns the names of vector components.
     *
     * @return names of vector components
     */
    public String[] getVectorComponentNames()
    {
        List<String> names = new ArrayList<>();
        for (DataArray da : components)
            if (da.isNumeric() && da.getVectorLength() > 1) names.add(da.getName());
        return names.toArray(new String[names.size()]);
    }

    /**
     * Sets the new array of components.
     *
     * @param data array of components
     */
    public void setComponents(ArrayList<DataArray> data)
    {
        if (data == null) throw new IllegalArgumentException("The list of components cannot be null");
        long nElem = -1;
        if (schema.getNElements() >= 0) {
            nElem = schema.getNElements();
        }
        if (data.size() > 0) {
            if (nElem < 0) {
                nElem = data.get(0).getNElements();
            }
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getNElements() != nElem) {
                    throw new IllegalArgumentException("All compoments must have the same number of elements equal to " + nElem);
                }
                if (data.get(i).hasParent()) {
                    throw new IllegalArgumentException("The component " + data.get(i).getName() + " already has a parent.");
                }
            }
        }
        removeComponents();
        for (int i = 0; i < data.size(); i++) {
            data.get(i).setHasParent(true);
            data.get(i).setCurrentTime(this.currentTime);
        }
        if (schema.getNElements() < 0 && nElem >= 0) {
            schema.setNElements(nElem);
        }
        this.components = data;
        schema.setComponentSchemasFromData(data);
        timestamp = System.nanoTime();
    }

    /**
     * Sets the component at specified index.
     *
     * @param i         index
     * @param dataArray component
     */
    public void setComponent(DataArray dataArray, int i)
    {

        if (dataArray == null) throw new IllegalArgumentException("The component cannot be null");
        if (i < 0)
            throw new IllegalArgumentException("i < 0");
        if (dataArray.hasParent()) throw new IllegalArgumentException("The component " + dataArray.getName() + " already has a parent.");
        if (components.size() > 0 && dataArray.getNElements() != getNElements())
            throw new IllegalArgumentException("The component has an invalid number of elements.");
        if (schema.getNElements() < 0) {
            schema.setNElements(dataArray.getNElements());
        }
        dataArray.setHasParent(true);
        dataArray.setCurrentTime(this.currentTime);
        if (i >= components.size())
            components.add(dataArray);
        else
            components.set(i, dataArray);
        schema.setComponentSchemasFromData(components);
        timestamp = System.nanoTime();
    }

    /**
     * Adds the component at a specified position. If a component with the same name already exists, the name of the input component is changed ("_" is added).
     *
     * @param dataArray component
     * @param position  index
     *
     * @return the input component (the name of the component may be modified)
     */
    public DataArray addComponent(DataArray dataArray, int position)
    {
        if (dataArray == null) throw new IllegalArgumentException("The component cannot be null");
        if (components.contains(dataArray)) {
            throw new IllegalArgumentException("The component already exists");
        }
        if (dataArray.hasParent()) throw new IllegalArgumentException("The component " + dataArray.getName() + " already has a parent.");
        if (components.size() > 0 && dataArray.getNElements() != getNElements())
            throw new IllegalArgumentException("The component has an invalid number of elements.");
        if (schema.getNElements() < 0) {
            schema.setNElements(dataArray.getNElements());
        }
        //check naming
        if (this.getComponent(dataArray.getName()) != null) {
            dataArray.setName(dataArray.getName() + "_");
            this.addComponent(dataArray, position);
            return dataArray;
        }
        dataArray.setHasParent(true);
        dataArray.setCurrentTime(this.currentTime);
        components.add(position, dataArray);
        schema.addDataArraySchema(position, dataArray.getSchema());
        timestamp = System.nanoTime();
        return dataArray;
    }

    /**
     * Adds the component to the end of the list. If a component with the same name already exists, the name of the input component is changed ("_" is added).
     *
     * @param dataArray component
     *
     * @return the input component (the name of the component may be modified)
     */
    public DataArray addComponent(DataArray dataArray)
    {
        addComponent(dataArray, components.size());
        return dataArray;
    }

    /**
     * Removes the component at a specified index.
     *
     * @param i index
     *
     * @return true, if the component was removed, false otherwise
     */
    public boolean removeComponent(int i)
    {
        if (i < 0 || i >= components.size())
            return false;

        schema.removeDataArraySchema(components.get(i).getSchema());
        components.get(i).setHasParent(false);
        components.remove(i);
        timestamp = System.nanoTime();
        return true;
    }

    /**
     * Removes all components.
     */
    public void removeComponents()
    {
        for (DataArray component : components) {
            component.setHasParent(false);
        }
        components.clear();
        schema.setComponentSchemasFromData(components);
        timestamp = System.nanoTime();
    }

    /**
     * Removes the component with a specified name.
     *
     * @param name name of the component
     *
     * @return true, if the component was removed, false otherwise
     */
    public boolean removeComponent(String name)
    {
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).getName().equals(name)) {
                return removeComponent(i);
            }
        }
        return false;
    }


    /**
     * check if at least one of the components is a category type component
     * @return true if there is a category component, false otherwise
     */
    public boolean hasCategoryComponent()
    {
        return schema.hasCategoryComponent();
    }

}

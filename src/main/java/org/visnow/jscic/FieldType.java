/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

/**
 *
 * Supported types of fields.
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public enum FieldType
{

    FIELD_UNKNOWN(-1),
    FIELD_GENERAL(0),
    FIELD_REGULAR(1),
    FIELD_IRREGULAR(2),
    FIELD_POINT(3);

    private final int val;

    private FieldType(int v)
    {
        val = v;
    }

    public int getValue()
    {
        return val;
    }

    public static FieldType getType(int value)
    {
        switch (value) {
            case 0:
                return FIELD_GENERAL;
            case 1:
                return FIELD_REGULAR;
            case 2:
                return FIELD_IRREGULAR;
            case 3:
                return FIELD_POINT;
            default:
                return FIELD_UNKNOWN;

        }
    }

    @Override
    public String toString()
    {
        switch (val) {
            case 0:
                return "general";
            case 1:
                return "regular";
            case 2:
                return "irregular";
            case 3:
                return "point";
            default:
                return "unknown";
        }
    }
}

/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import org.visnow.jscic.TimeData;
import org.visnow.jlargearrays.LongLargeArray;

/**
 *
 * DataArray that stores long elements.
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
public class LongDataArray extends DataArray
{

    private static final long serialVersionUID = 4472677135698534629L;

    /**
     * Creates a new instance of LongDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public LongDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_LONG) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_LONG);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
    }

    /**
     * Creates a new instance of ByteDataArray.
     *
     * @param ndata          number of data elements in the ByteDataArray
     * @param initValue      initialization value
     * @param createConstant if true, then a constant array is created
     */
    public LongDataArray(long ndata, Long initValue, boolean createConstant)
    {
        super(DataArrayType.FIELD_DATA_LONG, ndata, true);
        timeData = new TimeData(DataArrayType.FIELD_DATA_LONG);
        timeData.addValue(new LongLargeArray(ndata, initValue, createConstant));
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Creates a new instance of LongDataArray.
     *
     * @param ndata  number of data elements in the LongDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public LongDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_LONG, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_LONG);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
    }

    /**
     * Creates a new instance of LongDataArray.
     *
     * @param data   integer array to be included in the generated LongDataArray
     * @param schema DataArray schema.
     */
    public LongDataArray(LongLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_LONG) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_LONG);
        timeData.addValue(data);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Creates a new instance of LongDataArray.
     *
     * @param tData  integer array to be included in the generated LongDataArray
     * @param schema DataArray schema.
     */
    public LongDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_LONG) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_LONG) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    @Override
    public LongDataArray cloneShallow()
    {
        LongDataArray clone;
        if (timeData.isEmpty()) {
            clone = new LongDataArray(schema.cloneDeep());
        } else {
            clone = new LongDataArray(timeData.cloneShallow(), schema.cloneDeep());
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public LongDataArray cloneDeep()
    {
        LongDataArray clone;
        if (timeData.isEmpty()) {
            clone = new LongDataArray(schema.cloneDeep());
        } else {
            clone = new LongDataArray(timeData.cloneDeep(), schema.cloneDeep());
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public LongLargeArray getRawArray()
    {
        return (LongLargeArray) timeData.getCurrentValue();
    }

    @Override
    public LongLargeArray getRawArray(float time)
    {
        return (LongLargeArray) timeData.getValue(time);
    }

    @Override
    public LongLargeArray produceData(float time)
    {
        return (LongLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

}

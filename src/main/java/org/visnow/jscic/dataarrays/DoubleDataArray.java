/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import java.util.ArrayList;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.utils.FloatingPointUtils;
import org.visnow.jscic.utils.InfinityAction;
import org.visnow.jscic.utils.NaNAction;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.LargeArray;

/**
 *
 * DataArray that stores double precision floating point elements.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class DoubleDataArray extends DataArray
{

    private static final long serialVersionUID = 3672664118918770273L;

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public DoubleDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param ndata          number of data elements in the DoubleDataArray
     * @param initValue      initialization value
     * @param createConstant if true, then a constant array is created
     */
    public DoubleDataArray(long ndata, Double initValue, boolean createConstant)
    {
        super(DataArrayType.FIELD_DATA_DOUBLE, ndata, true);
        timeData = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
        timeData.addValue(new DoubleLargeArray(ndata, initValue, createConstant));
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param ndata  number of data elements in the DoubleDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public DoubleDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_DOUBLE, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_DOUBLE);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param data   double array to be included in the generated DoubleDataArray
     * @param schema DataArray schema.
     */
    public DoubleDataArray(DoubleLargeArray data, DataArraySchema schema)
    {
        this(data, schema, true, FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction);
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param data           double array to be included in the generated DoubleDataArray
     * @param schema         DataArray schema.
     * @param testNanInf     if true, then the DataArray is tested for NaNs and infinities
     * @param nanAction      not a number action
     * @param infinityAction infinity action
     */
    public DoubleDataArray(DoubleLargeArray data, DataArraySchema schema, boolean testNanInf, NaNAction nanAction, InfinityAction infinityAction)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        if (testNanInf) {
            FloatingPointUtils.processNaNs(data, nanAction, infinityAction);
        }
        ArrayList<Float> timeSeries = new ArrayList<>(1);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(1);
        timeSeries.add(0f);
        dataSeries.add(data);
        timeData = new TimeData(timeSeries, dataSeries, 0f, false, nanAction, infinityAction);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param tData  double array to be included in the generated DoubleDataArray
     * @param schema DataArray schema.
     */
    public DoubleDataArray(TimeData tData, DataArraySchema schema)
    {
        this(tData, schema, true);
    }

    /**
     * Creates a new instance of DoubleDataArray.
     *
     * @param tData      double array to be included in the generated DoubleDataArray
     * @param schema     DataArray schema.
     * @param testNanInf if true, then the DataArray is tested for NaNs and infinities
     */
    public DoubleDataArray(TimeData tData, DataArraySchema schema, boolean testNanInf)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_DOUBLE) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (testNanInf) {
            for (int i = 0; i < tData.getNSteps(); i++) {
                FloatingPointUtils.processNaNs((DoubleLargeArray) tData.getValues().get(i), tData.getNanAction(), tData.getInfinityAction());
            }
        }
        timeData = tData;
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    @Override
    public DoubleDataArray cloneShallow()
    {
        DoubleDataArray clone;
        if (timeData.isEmpty()) {
            clone = new DoubleDataArray(schema.cloneDeep());

        } else {
            clone = new DoubleDataArray(timeData.cloneShallow(), schema.cloneDeep());
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public DoubleDataArray cloneDeep()
    {
        DoubleDataArray clone;
        if (timeData.isEmpty()) {
            clone = new DoubleDataArray(schema.cloneDeep());
        } else {
            clone = new DoubleDataArray(timeData.cloneDeep(), schema.cloneDeep());
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public void setTimeData(TimeData tData)
    {
        if (tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()) {
            throw new IllegalArgumentException("tData == null || tData.isEmpty() || tData.getType() != timeData.getType() || tData.length() != getNElements() * getVectorLength()");
        }

        for (int i = 0; i < tData.getNSteps(); i++) {
            FloatingPointUtils.processNaNs((DoubleLargeArray) tData.getValues().get(i), tData.getNanAction(), tData.getInfinityAction());
        }

        timeData = tData;
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    @Override
    public DoubleLargeArray getRawArray(float time)
    {
        return (DoubleLargeArray) timeData.getValue(time);
    }

    @Override
    public DoubleLargeArray produceData(float time)
    {
        return (DoubleLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }

    @Override
    public DoubleLargeArray getRawArray()
    {
        return (DoubleLargeArray) timeData.getCurrentValue();
    }

    /**
     * Returns not a number action
     *
     * @return not a number action
     */
    public NaNAction getNanAction()
    {
        return timeData.getNanAction();
    }

    /**
     * Sets not a number action
     *
     * @param nanAction a new not a number action
     */
    public void setNanAction(NaNAction nanAction)
    {
        this.timeData.setNanAction(nanAction);
    }

    /**
     * Returns infinity action.
     *
     * @return infinity action
     */
    public InfinityAction getInfinityAction()
    {
        return timeData.getInfinityAction();
    }

    /**
     * Sets infinity action.
     *
     * @param infinityAction infinity action
     */
    public void setInfinityAction(InfinityAction infinityAction)
    {
        this.timeData.setInfinityAction(infinityAction);
    }
}

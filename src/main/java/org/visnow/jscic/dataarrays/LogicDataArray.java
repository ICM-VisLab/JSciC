/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import org.visnow.jscic.TimeData;
import org.visnow.jlargearrays.LogicLargeArray;

/**
 *
 * DataArray that stores bit elements (0 and 1).
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public class LogicDataArray extends DataArray
{

    private static final long serialVersionUID = 2516204388960776188L;

    /**
     * Creates a new instance of LogicDataArray.
     *
     * @param	schema	DataArray schema.
     */
    public LogicDataArray(DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_LOGIC) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_LOGIC);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
    }

    /**
     * Creates a new instance of LogicDataArray.
     *
     * @param ndata          number of data elements in the LogicDataArray
     * @param initValue      initialization value
     * @param createConstant if true, then a constant array is created
     */
    public LogicDataArray(long ndata, Byte initValue, boolean createConstant)
    {
        super(DataArrayType.FIELD_DATA_LOGIC, ndata, true);
        timeData = new TimeData(DataArrayType.FIELD_DATA_LOGIC);
        timeData.addValue(new LogicLargeArray(ndata, initValue, createConstant));
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Creates a new instance of LogicDataArray.
     *
     * @param ndata  number of data elements in the LogicDataArray
     * @param veclen vector length (1 for scalar data)
     */
    public LogicDataArray(long ndata, int veclen)
    {
        super(DataArrayType.FIELD_DATA_LOGIC, ndata, veclen);
        timeData = new TimeData(DataArrayType.FIELD_DATA_LOGIC);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
    }

    /**
     * Creates a new instance of LogicDataArray.
     *
     * @param data   logic array to be included in the generated LogicDataArray
     * @param schema DataArray schema.
     */
    public LogicDataArray(LogicLargeArray data, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_LOGIC) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (schema.getNElements() != data.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        if (schema.isConstant() != data.isConstant()) {
            throw new IllegalArgumentException("schema.isConstant() != data.isConstant()");
        }
        timeData = new TimeData(DataArrayType.FIELD_DATA_LOGIC);
        timeData.addValue(data);
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    /**
     * Creates a new instance of LogicDataArray.
     *
     * @param tData  logic array to be included in the generated LogicDataArray
     * @param schema DataArray schema.
     */
    public LogicDataArray(TimeData tData, DataArraySchema schema)
    {
        super(schema);
        if (schema.getType() != DataArrayType.FIELD_DATA_LOGIC) {
            throw new IllegalArgumentException("Schema type does not match array type.");
        }

        if (tData.getType() != DataArrayType.FIELD_DATA_LOGIC) {
            throw new IllegalArgumentException("Data type does not match array type.");
        }

        if (schema.getNElements() != tData.length() / schema.getVectorLength()) {
            throw new IllegalArgumentException("Schema does not match array length.");
        }
        timeData = tData;
        this.schema.setTimeDataSchema(this.timeData.getTimeDataSchema());
        recomputeStatistics();
    }

    @Override
    public LogicDataArray cloneShallow()
    {
        LogicDataArray clone;
        if (timeData.isEmpty()) {
            clone = new LogicDataArray(schema.cloneDeep());
        } else {
            clone = new LogicDataArray(timeData.cloneShallow(), schema.cloneDeep());
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public LogicDataArray cloneDeep()
    {
        LogicDataArray clone;
        if (timeData.isEmpty()) {
            clone = new LogicDataArray(schema.cloneDeep());
        } else {
            clone = new LogicDataArray(timeData.cloneDeep(), schema.cloneDeep());
        }
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public LogicLargeArray getRawArray()
    {
        return (LogicLargeArray) timeData.getCurrentValue();
    }

    @Override
    public LogicLargeArray getRawArray(float time)
    {
        return (LogicLargeArray) timeData.getValue(time);
    }

    @Override
    public LogicLargeArray produceData(float time)
    {
        return (LogicLargeArray) timeData.produceValue(time, getVectorLength() * getNElements());
    }
}

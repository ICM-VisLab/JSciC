/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import org.visnow.jlargearrays.LargeArrayType;
import static org.visnow.jlargearrays.LargeArrayType.*;

/**
 *
 * Supported types of a DataArray.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 * @author Bartosz Borucki, University of Warsaw, ICM
 */
public enum DataArrayType
{
//                 value size numeric  name      nameAnalogies                                      largeArrayType
    FIELD_DATA_UNKNOWN (-1, 1,  true, "unknown", new String[]{"unknown"},                           null),
    FIELD_DATA_LOGIC   ( 0, 1,  true, "  logic", new String[]{"logic", "log","boolean","bool"},     LOGIC),
    FIELD_DATA_BYTE    ( 1, 1,  true, "   byte", new String[]{"byte", "char"},                      UNSIGNED_BYTE),
    FIELD_DATA_SHORT   ( 2, 2,  true, "  short", new String[]{"short"},                             SHORT),
    FIELD_DATA_INT     ( 3, 4,  true, "    int", new String[]{"int", "integer"},                    INT),
    FIELD_DATA_FLOAT   ( 4, 4,  true, "  float", new String[]{"float", "real"},                     FLOAT),
    FIELD_DATA_DOUBLE  ( 5, 8,  true, " double", new String[]{"double", "double precision", "dbl"}, DOUBLE),
    FIELD_DATA_COMPLEX ( 6, 8,  true, "complex", new String[]{"complex"},                           COMPLEX_FLOAT),
    FIELD_DATA_STRING  ( 7, 1, false, " string", new String[]{"string"},                            STRING),
    FIELD_DATA_OBJECT  ( 8, 1, false, " object", new String[]{"object"},                            OBJECT),
    FIELD_DATA_LONG    ( 9, 8,  true, "   long", new String[]{"long", "long int"},                  LONG);

    private final int value;
    private final int size;
    private final boolean numeric;
    private final String name;
    private final String[] nameAnalogies;
    private final LargeArrayType largeArrayType;

    private DataArrayType(int value, int size, boolean numeric, String name, String[] nameAnalogies, LargeArrayType largeArrayType)
    {
        this.value = value;
        this.size = size;
        this.numeric = numeric;
        this.name = name;
        this.nameAnalogies = nameAnalogies;
        this.largeArrayType = largeArrayType;
    }

    @Override
    public String toString()
    {
        return name;
    }

    /**
     * Returns the list of name analogies.
     *
     * @return list of name analogies
     */
    public String[] getNameAnalogies()
    {
        return nameAnalogies;
    }

    /**
     * Returns true if a given type is numeric, false otherwise.
     *
     * @return true if a given type is numeric, false otherwise
     */
    public boolean isNumericType()
    {
        return numeric;

    }

    /**
     * Returns a LargeArray type corresponding to a given DataArray type. Returns null for FIELD_DATA_UNKNOWN
     *
     * @return a LargeArray type corresponding to a given DataArray type.
     */
    public LargeArrayType toLargeArrayType()
    {
        return largeArrayType;
    }

    /**
     * Returns the value of a given DataArray type.
     *
     * @return value of a given DataArray type
     */
    public int getValue()
    {
        return value;
    }

    /**
     * Returns the size of a given DataArray type in bytes.
     *
     * @return size of a given DataArray type in bytes
     */
    public int getSize()
    {
        return size;
    }

    /**
     * Returns the array type corresponding to a given value.
     *
     * @param value value of an array type.
     *
     * @return array type corresponding to a given value
     */
    public static DataArrayType getType(int value)
    {
        switch (value) {
            case -1:
                return FIELD_DATA_UNKNOWN;
            case 0:
                return FIELD_DATA_LOGIC;
            case 1:
                return FIELD_DATA_BYTE;
            case 2:
                return FIELD_DATA_SHORT;
            case 3:
                return FIELD_DATA_INT;
            case 9:
                return FIELD_DATA_LONG;
            case 4:
                return FIELD_DATA_FLOAT;
            case 5:
                return FIELD_DATA_DOUBLE;
            case 6:
                return FIELD_DATA_COMPLEX;
            case 7:
                return FIELD_DATA_STRING;
            case 8:
                return FIELD_DATA_OBJECT;
            default:
                return FIELD_DATA_UNKNOWN;

        }
    }

    /**
     * Returns the value of the last supported type. Values of the types are sorted in ascending order.
     *
     * @return value of the last supported type
     */
    public static int getMaxValue()
    {
        //Values need to be sorted in ascending order
        return values()[values().length - 1].getValue();
    }

}

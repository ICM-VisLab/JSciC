/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.dataarrays;

import java.awt.Color;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM

 CategoryDescription of data items in the map and category data
 Contains value to be found in the data array, map area/category name and a color for this area/category
 */
public class CategoryDescription implements Cloneable
{
    /**
     * a category index that will be used in the category array
     */
    public final int value;
    /**
     * category/map item name to be used as label in legends, glyphs etc.
     */
    public final String name;
    /**
     * Standard RGBA color used to map this category
     */
    public Color  color;

    public CategoryDescription(int value, String name, Color color)
    {
        this.value = value;
        this.name = name;
        this.color = color;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof CategoryDescription))
            return false;
        CategoryDescription cd = (CategoryDescription)o;
        return cd.value == value &&
               cd.name.equals(name) &&
               Arrays.equals(cd.color.getComponents(null), color.getComponents(null));
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 17 * hash + this.value;
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + Objects.hashCode(this.color);
        return hash;
    }

    @Override
    public CategoryDescription clone()
    {
        return new CategoryDescription(value, name, color);
    }

    @Override
    public String toString()
    {
        return String.format("%3d: %s: %3d %3d %3d %3d", value, name, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }

    /**
     * parses a string read in from the user data section and generates a single CategoryDescription object
     * @param s parsed string;
     * @return  compiled category description
     */
    public static CategoryDescription fromString(String s)
    {
        String[] fields = s.trim().split(": *");
        int value = 0;
        if (fields.length < 2)
            throw new IllegalArgumentException("bad label item " + s);
        try {
            value = Integer.parseInt(fields[0].trim());
        } catch (Exception e) {
            throw new IllegalArgumentException("bad value field in the label item " + s);
        }
        String name  = fields[1];
        Color color = new Color(0, 0, 0, 0);
        if (fields.length > 2) {
            String[] colorDescription = fields[2].trim().split(" +");
            if (colorDescription.length >= 3) {
                int r, g, b, a;
                try {
                    r = Integer.parseInt(colorDescription[0].trim());
                    g = Integer.parseInt(colorDescription[1].trim());
                    b = Integer.parseInt(colorDescription[2].trim());
                    if (colorDescription.length >= 4) {
                        a = Integer.parseInt(colorDescription[3].trim());
                        color = new Color(r, g, b, a);
                    }
                    else
                        color = new Color(r, g, b);
                } catch (NumberFormatException e) {
                    // could not parse color components properly - uses default
                    return new CategoryDescription(value, name, color);
                }
            }
        }
       return new CategoryDescription(value, name, color);
    }
   /**
    * checks if the category name suggests that this is a "background" 
    * and should ne ignored in display and typical calculations
    * @return true if the name is an abbreviation or is similar to the "background" string
    */
    public boolean isBgr()
    {
        return name.toLowerCase().startsWith("bgr") || 
               name.toLowerCase().startsWith("bck") || 
               name.toLowerCase().startsWith("back");
    }
    /**
     * Getter for color
     * @return color value
     */
    public Color getColor()
    {
        return color;
    }

    /**
     * color setter for future colormap modification
     * @param color new color for this category
     */
    public void setColor(Color color)
    {
        this.color = color;
    }
    /**
     * generation of an index data value to category (in applications, data values need not be real indices into the category description array)
     * @param ds category description
     * @return reverse index value to category (uncategorized values are mapped to -1)
     */
    public static int[] createIndex(CategoryDescription[] ds)
    {
        int[] valIndex;
        int maxInd = 0;
        for (CategoryDescription d : ds)
            if (d.value > maxInd)
                maxInd = d.value;
        valIndex = new int[maxInd + 1];
        Arrays.fill(valIndex, -1);
        for (int i = 0; i < ds.length; i++)
            valIndex[ds[i].value] = i;
        return valIndex;
    }

    /**
     * in a given category description array converts unassigned colors to the standard rainbow colormap
     * @param ds a category description array
     * @param full if true, full circle (red/magenta/.../yellow/red) colormap is used
     * otherwise, reduced colormap (magenta/blue/.../red) is used
     */
    public static void convertToRainbow(CategoryDescription[] ds, boolean full)
    {
        int n = ds.length;
        float lowHue = full ? 1 : 5.f/6.f;
        for (int i = 0; i < n; i++) {
            CategoryDescription d = ds[i];
            Color c = d.color;
            if (c.getAlpha() == 0 && c.getRed() == 0 && c.getGreen() == 0 && c.getBlue() == 0) {
                float h = n > 1 ? (float)(n - 1 - i) / (n-1) * lowHue : 0;
                d.setColor(Color.getHSBColor(h,1,1));
            }
        }
    }
}

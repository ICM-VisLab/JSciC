/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic;

import java.util.ArrayList;
import java.util.Arrays;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.utils.FieldUtils;

/**
 * Holds general information about RegularField.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularFieldSchema extends FieldSchema
{

    private static final long serialVersionUID = 3788914382207829277L;

    /**
     * Dimensions
     */
    private long[] dims;

    /**
     * Creates a new instance of RegularFieldSchema.
     */
    public RegularFieldSchema()
    {
        super();
    }

    /**
     * Creates a new instance of RegularFieldSchema.
     *
     * @param name field name
     */
    public RegularFieldSchema(String name)
    {
        super(name);
    }

    @Override
    public int hashCode()
    {
        int hash = super.hashCode();
        hash = 29 * hash + Arrays.hashCode(this.dims);
        return hash;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null || !(o instanceof RegularFieldSchema))
            return false;
        RegularFieldSchema rfs = (RegularFieldSchema) o;
        return super.equals(o) && Arrays.equals(this.dims, rfs.dims);
    }

    /**
     * Returns the dimensions.
     *
     * @return dimensions
     */
    public long[] getDims()
    {
        return dims;
    }

    /**
     * Returns the number of dimensions.
     *
     * @return number of dimensions
     */
    public int getNDims()
    {
        return dims.length;
    }

    /**
     * Sets the dimensions.
     *
     * @param dims dimensions
     */
    public void setDims(long[] dims)
    {
        FieldUtils.checkDimensions(dims);
        this.dims = dims;
    }

    /**
     * Compares two RegularFieldSchemas. Returns true if all components are compatible in this schema and the input schema and both fields are of the same
     * dimension, false otherwise.
     *
     * @param	s RegularFieldSchema to be compared
     *
     * @return	true if all components are compatible in this schema and the input schema and both fields are of the same dimension, false otherwise
     */
    public boolean isCompatibleWith(RegularFieldSchema s)
    {
        return super.isCompatibleWith(s) && Arrays.equals(this.dims, s.dims);
    }

    @Override
    public RegularFieldSchema cloneDeep()
    {
        RegularFieldSchema clone = new RegularFieldSchema(this.name);
        clone.nElements = this.nElements;
        if (this.dims != null) {
            clone.dims = this.dims.clone();
        }
        ArrayList<DataArraySchema> componentSchemasClone = new ArrayList<>();
        if (this.componentSchemas != null && this.componentSchemas.size() > 0) {
            for (DataArraySchema item : this.componentSchemas) componentSchemasClone.add(item.cloneDeep());
        }
        clone.componentSchemas = componentSchemasClone;
        if (coordsUnits != null) {
            System.arraycopy(coordsUnits, 0, clone.coordsUnits, 0, coordsUnits.length);
        }
        clone.timeUnit = timeUnit;

        ArrayList<DataArraySchema> pseudoComponentSchemasClone = new ArrayList<>();
        if (this.pseudoComponentSchemas != null && this.pseudoComponentSchemas.size() > 0) {
            for (DataArraySchema item : this.pseudoComponentSchemas) pseudoComponentSchemasClone.add(item.cloneDeep());
        }
        clone.pseudoComponentSchemas = pseudoComponentSchemasClone;
        clone.intializedPreferredExtents = intializedPreferredExtents;

        clone.extents = new float[2][3];
        for (int i = 0; i < extents.length; i++)
            System.arraycopy(this.extents[i], 0, clone.extents[i], 0, 3);
        clone.preferredExtents = new float[2][3];
        for (int i = 0; i < preferredExtents.length; i++)
            System.arraycopy(this.preferredExtents[i], 0, clone.preferredExtents[i], 0, 3);
        clone.physMappingCoeffs = new double[3][2];
        for (int i = 0; i < physMappingCoeffs.length; i++)
            System.arraycopy(this.physMappingCoeffs[i], 0, clone.physMappingCoeffs[i], 0, 2);
        return clone;
    }

}

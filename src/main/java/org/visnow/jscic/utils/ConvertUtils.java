/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jscic.dataarrays.DataObjectInterface;
import org.visnow.jlargearrays.UnsignedByteLargeArray;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.DoubleLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LongLargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jlargearrays.ShortLargeArray;

/**
 * Array conversion utilities.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class ConvertUtils
{

    /**
     * Converts any LargeArray to UnsignedByteLargeArray.
     *
     * @param in input array
     *
     * @return byte array
     */
    public static UnsignedByteLargeArray convertToUnsignedByteLargeArray(LargeArray in)
    {
        return ConvertUtils.convertToUnsignedByteLargeArray(in, false, 0, 1);
    }

    /**
     * Converts any LargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    public static UnsignedByteLargeArray convertToUnsignedByteLargeArray(LargeArray in, boolean normalize, float min, float max)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
                return (UnsignedByteLargeArray) LargeArrayUtils.convert(in, LargeArrayType.UNSIGNED_BYTE);
            case UNSIGNED_BYTE:
                return (UnsignedByteLargeArray) in;
            case SHORT:
                return ConvertUtils.convertToUnsignedByteLargeArray((ShortLargeArray) in, normalize, min, max);
            case INT:
                return ConvertUtils.convertToUnsignedByteLargeArray((IntLargeArray) in, normalize, min, max);
            case LONG:
                return ConvertUtils.convertToUnsignedByteLargeArray((LongLargeArray) in, normalize, min, max);
            case FLOAT:
                return ConvertUtils.convertToUnsignedByteLargeArray((FloatLargeArray) in, normalize, min, max);
            case DOUBLE:
                return ConvertUtils.convertToUnsignedByteLargeArray((DoubleLargeArray) in, normalize, min, max);
            case COMPLEX_FLOAT:
                return ConvertUtils.convertToUnsignedByteLargeArray((ComplexFloatLargeArray) in, normalize, min, max);
            case COMPLEX_DOUBLE:
                return ConvertUtils.convertToUnsignedByteLargeArray((ComplexDoubleLargeArray) in, normalize, min, max);
            case STRING:
                return (UnsignedByteLargeArray) LargeArrayUtils.convert(in, LargeArrayType.UNSIGNED_BYTE);
            case OBJECT:
                return convertToUnsignedByteLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts a ShortLArgeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final ShortLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (in.getShort(0) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & in.getShort(0)), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(d * (in.getShort(i) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & in.getShort(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an IntLargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final IntLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (in.getInt(0) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & in.getInt(0)), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(d * (in.getInt(i) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & in.getInt(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an LongLargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final LongLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (in.getLong(0) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & in.getLong(0)), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(d * (in.getLong(i) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & in.getLong(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a FloatLargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final FloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (in.getFloat(0) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(in.getFloat(0))), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(d * (in.getFloat(i) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(in.getFloat(i))));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexFloatLargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final ComplexFloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final FloatLargeArray re = in.getRealArray();
        final FloatLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            float v1 = re.getFloat(0);
            float v2 = im.getFloat(0);
            if (normalize) {
                float d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (sqrt(v1 * v1 + v2 * v2) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(sqrt(v1 * v1 + v2 * v2))), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setByte(i, (byte) (0xff & (int) round(d * (sqrt(v1 * v1 + v2 * v2) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setByte(i, (byte) (0xff & (int) round(sqrt(v1 * v1 + v2 * v2))));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexFloatLargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final ComplexDoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final DoubleLargeArray re = in.getRealArray();
        final DoubleLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            double v1 = re.getDouble(0);
            double v2 = im.getDouble(0);
            if (normalize) {
                float d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (sqrt(v1 * v1 + v2 * v2) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(sqrt(v1 * v1 + v2 * v2))), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setByte(i, (byte) (0xff & (int) round(d * (sqrt(v1 * v1 + v2 * v2) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setByte(i, (byte) (0xff & (int) round(sqrt(v1 * v1 + v2 * v2))));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a DoubleLargeArray to UnsignedByteLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final DoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                double d = 255 / (max - min);
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(d * (in.getDouble(0) - min))), true);
            } else {
                return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(in.getDouble(0))), true);
            }
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        double d = 255 / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(d * (in.getDouble(i) - min))));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setByte(i, (byte) (0xff & (int) round(in.getDouble(i))));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an ObjectLargeArray to UnsignedByteLargeArray.
     *
     * @param in input array
     *
     * @return byte array
     */
    private static UnsignedByteLargeArray convertToUnsignedByteLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new UnsignedByteLargeArray(n, (byte) (0xff & (int) round(((DataObjectInterface) in.get(0)).toFloat())), true);
        }
        final UnsignedByteLargeArray out = new UnsignedByteLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setByte(i, (byte) (0xff & (int) round(((DataObjectInterface) in.get(i)).toFloat())));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to ShortLargeArray.
     *
     * @param in input array
     *
     * @return short array
     */
    public static ShortLargeArray convertToShortLargeArray(LargeArray in)
    {
        return convertToShortLargeArray(in, false, 0, 1);
    }

    /**
     * Converts any LargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    public static ShortLargeArray convertToShortLargeArray(LargeArray in, boolean normalize, float min, float max)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
                return (ShortLargeArray) LargeArrayUtils.convert(in, LargeArrayType.SHORT);
            case SHORT:
                return (ShortLargeArray) in;
            case INT:
                return convertToShortLargeArray((IntLargeArray) in, normalize, min, max);
            case LONG:
                return convertToShortLargeArray((LongLargeArray) in, normalize, min, max);
            case FLOAT:
                return convertToShortLargeArray((FloatLargeArray) in, normalize, min, max);
            case DOUBLE:
                return convertToShortLargeArray((DoubleLargeArray) in, normalize, min, max);
            case COMPLEX_FLOAT:
                return convertToShortLargeArray((ComplexFloatLargeArray) in, normalize, min, max);
            case COMPLEX_DOUBLE:
                return convertToShortLargeArray((ComplexDoubleLargeArray) in, normalize, min, max);
            case STRING:
                return (ShortLargeArray) LargeArrayUtils.convert(in, LargeArrayType.SHORT);
            case OBJECT:
                return convertToShortLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts an IntLargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final IntLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                return new ShortLargeArray(n, (short) round(Short.MIN_VALUE + d * (in.getInt(0) - min)), true);
            } else {
                return new ShortLargeArray(n, (short) in.getInt(0), true);
            }
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) round(Short.MIN_VALUE + d * (in.getInt(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) in.getInt(i));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a LongLargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final LongLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                return new ShortLargeArray(n, (short) round(Short.MIN_VALUE + d * (in.getLong(0) - min)), true);
            } else {
                return new ShortLargeArray(n, (short) in.getLong(0), true);
            }
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) round(Short.MIN_VALUE + d * (in.getLong(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) in.getLong(i));
                        }
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a FloatLargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize ==
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize ==
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final FloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                return new ShortLargeArray(n, (short) round(Short.MIN_VALUE + d * (in.getFloat(0) - min)), true);
            } else {
                return new ShortLargeArray(n, (short) round(in.getFloat(0)), true);
            }
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) round(Short.MIN_VALUE + d * (in.getFloat(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) round(in.getFloat(i)));
                        }
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexFloatLargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final ComplexFloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final FloatLargeArray re = in.getRealArray();
        final FloatLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            float v1 = re.getFloat(0);
            float v2 = im.getFloat(0);
            if (normalize) {
                float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                return new ShortLargeArray(n, (short) round(Short.MIN_VALUE + d * (sqrt(v1 * v1 + v2 * v2) - min)), true);
            } else {
                return new ShortLargeArray(n, (short) round(sqrt(v1 * v1 + v2 * v2)), true);
            }
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setShort(i, (short) round(Short.MIN_VALUE + d * (sqrt(v1 * v1 + v2 * v2) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setShort(i, (short) round(sqrt(v1 * v1 + v2 * v2)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexDoubleLargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final ComplexDoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final DoubleLargeArray re = in.getRealArray();
        final DoubleLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            double v1 = re.getDouble(0);
            double v2 = im.getDouble(0);
            if (normalize) {
                double d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                return new ShortLargeArray(n, (short) round(Short.MIN_VALUE + d * (sqrt(v1 * v1 + v2 * v2) - min)), true);
            } else {
                return new ShortLargeArray(n, (short) round(sqrt(v1 * v1 + v2 * v2)), true);
            }
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setShort(i, (short) round(Short.MIN_VALUE + d * (sqrt(v1 * v1 + v2 * v2) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setShort(i, (short) round(sqrt(v1 * v1 + v2 * v2)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a DoubleLargeArray to ShortLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final DoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                double d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                return new ShortLargeArray(n, (short) round(Short.MIN_VALUE + d * (in.getDouble(0) - min)), true);
            } else {
                return new ShortLargeArray(n, (short) round(in.getDouble(0)), true);
            }
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        double d = (Short.MAX_VALUE - Short.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) round(Short.MIN_VALUE + d * (in.getDouble(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setShort(i, (short) round(in.getDouble(i)));
                        }
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an ObjectLargeArray to ShortLargeArray.
     *
     * @param in input array
     *
     * @return short array
     */
    private static ShortLargeArray convertToShortLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new ShortLargeArray(n, (short) round(((DataObjectInterface) in.get(0)).toFloat()), true);
        }
        final ShortLargeArray out = new ShortLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setShort(i, (short) round(((DataObjectInterface) in.get(i)).toFloat()));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to IntLargeArray.
     *
     * @param in input array
     *
     * @return int array
     */
    public static IntLargeArray convertToIntLargeArray(LargeArray in)
    {
        return convertToIntLargeArray(in, false, 0, 1);
    }

    /**
     * Converts any LargeArray to IntLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return int array
     */
    public static IntLargeArray convertToIntLargeArray(LargeArray in, boolean normalize, float min, float max)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
                return (IntLargeArray) LargeArrayUtils.convert(in, LargeArrayType.INT);
            case INT:
                return (IntLargeArray) in;
            case LONG:
                return (IntLargeArray) LargeArrayUtils.convert(in, LargeArrayType.INT);
            case FLOAT:
                return convertToIntLargeArray((FloatLargeArray) in, normalize, min, max);
            case DOUBLE:
                return convertToIntLargeArray((DoubleLargeArray) in, normalize, min, max);
            case COMPLEX_FLOAT:
                return convertToIntLargeArray((ComplexFloatLargeArray) in, normalize, min, max);
            case COMPLEX_DOUBLE:
                return convertToIntLargeArray((ComplexDoubleLargeArray) in, normalize, min, max);
            case STRING:
                return (IntLargeArray) LargeArrayUtils.convert(in, LargeArrayType.INT);
            case OBJECT:
                return convertToIntLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts a FloatLargeArray to IntLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return int array
     */
    private static IntLargeArray convertToIntLargeArray(final FloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                return new IntLargeArray(n, (int) round(Integer.MIN_VALUE - d * (in.getFloat(0) - min)), true);
            } else {
                return new IntLargeArray(n, (int) round(in.getFloat(0)), true);
            }
        }
        final IntLargeArray out = new IntLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setInt(i, (int) round(Integer.MIN_VALUE - d * (in.getFloat(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setInt(i, (int) round(in.getFloat(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexFloatLargeArray to IntLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return int array
     */
    private static IntLargeArray convertToIntLargeArray(final ComplexFloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final FloatLargeArray re = in.getRealArray();
        final FloatLargeArray im = in.getImaginaryArray();

        if (in.isConstant()) {
            float v1 = re.getFloat(0);
            float v2 = im.getFloat(0);
            if (normalize) {
                float d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                return new IntLargeArray(n, (int) round(Integer.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)), true);
            } else {
                return new IntLargeArray(n, (int) round(sqrt(v1 * v1 + v2 * v2)), true);
            }
        }
        final IntLargeArray out = new IntLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setInt(i, (int) round(Integer.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setInt(i, (int) round(sqrt(v1 * v1 + v2 * v2)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexDoubleLargeArray to IntLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return int array
     */
    private static IntLargeArray convertToIntLargeArray(final ComplexDoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final DoubleLargeArray re = in.getRealArray();
        final DoubleLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            double v1 = re.getDouble(0);
            double v2 = im.getDouble(0);
            if (normalize) {
                float d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                return new IntLargeArray(n, (int) round(Integer.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)), true);
            } else {
                return new IntLargeArray(n, (int) round(sqrt(v1 * v1 + v2 * v2)), true);
            }
        }
        final IntLargeArray out = new IntLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        double d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setInt(i, (int) round(Integer.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setInt(i, (int) round(sqrt(v1 * v1 + v2 * v2)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a DoubleLargeArray to IntLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return int array
     */
    private static IntLargeArray convertToIntLargeArray(final DoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                return new IntLargeArray(n, (int) round(Integer.MIN_VALUE - d * (in.getDouble(0) - min)), true);
            } else {
                return new IntLargeArray(n, (int) round(in.getDouble(0)), true);
            }
        }
        final IntLargeArray out = new IntLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        double d = (Integer.MAX_VALUE - Integer.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setInt(i, (int) round(Integer.MIN_VALUE - d * (in.getDouble(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setInt(i, (int) round(in.getDouble(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an ObjectLargeArray to IntLargeArray.
     *
     * @param in input array
     *
     * @return int array
     */
    private static IntLargeArray convertToIntLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new IntLargeArray(n, (int) round(((DataObjectInterface) in.get(0)).toFloat()), true);
        }
        final IntLargeArray out = new IntLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setInt(i, (int) round(((DataObjectInterface) in.get(i)).toFloat()));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to LongLargeArray.
     *
     * @param in input array
     *
     * @return long array
     */
    public static LongLargeArray convertToLongLargeArray(LargeArray in)
    {
        return convertToLongLargeArray(in, false, 0, 1);
    }

    /**
     * Converts any LargeArray to LongLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return long array
     */
    public static LongLargeArray convertToLongLargeArray(LargeArray in, boolean normalize, float min, float max)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
                return (LongLargeArray) LargeArrayUtils.convert(in, LargeArrayType.LONG);
            case LONG:
                return (LongLargeArray) in;
            case FLOAT:
                return convertToLongLargeArray((FloatLargeArray) in, normalize, min, max);
            case DOUBLE:
                return convertToLongLargeArray((DoubleLargeArray) in, normalize, min, max);
            case COMPLEX_FLOAT:
                return convertToLongLargeArray((ComplexFloatLargeArray) in, normalize, min, max);
            case COMPLEX_DOUBLE:
                return convertToLongLargeArray((ComplexDoubleLargeArray) in, normalize, min, max);
            case STRING:
                return (LongLargeArray) LargeArrayUtils.convert(in, LargeArrayType.LONG);
            case OBJECT:
                return convertToLongLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts a FloatLargeArray to LongLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return long array
     */
    private static LongLargeArray convertToLongLargeArray(final FloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                return new LongLargeArray(n, (long) round(Long.MIN_VALUE - d * (in.getFloat(0) - min)), true);
            } else {
                return new LongLargeArray(n, (long) round(in.getFloat(0)), true);
            }
        }
        final LongLargeArray out = new LongLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setLong(i, (long) round(Long.MIN_VALUE - d * (in.getFloat(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setLong(i, (long) round(in.getFloat(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexFloatLargeArray to LongLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return long array
     */
    private static LongLargeArray convertToLongLargeArray(final ComplexFloatLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final FloatLargeArray re = in.getRealArray();
        final FloatLargeArray im = in.getImaginaryArray();

        if (in.isConstant()) {
            float v1 = re.getFloat(0);
            float v2 = im.getFloat(0);
            if (normalize) {
                float d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                return new LongLargeArray(n, (long) round(Long.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)), true);
            } else {
                return new LongLargeArray(n, (long) round(sqrt(v1 * v1 + v2 * v2)), true);
            }
        }
        final LongLargeArray out = new LongLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        float d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setLong(i, (int) round(Long.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            float v1 = re.getFloat(i);
                            float v2 = im.getFloat(i);
                            out.setLong(i, (long) round(sqrt(v1 * v1 + v2 * v2)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexDoubleLargeArray to LongLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return long array
     */
    private static LongLargeArray convertToLongLargeArray(final ComplexDoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        final DoubleLargeArray re = in.getRealArray();
        final DoubleLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            double v1 = re.getDouble(0);
            double v2 = im.getDouble(0);
            if (normalize) {
                float d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                return new LongLargeArray(n, (long) round(Long.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)), true);
            } else {
                return new LongLargeArray(n, (long) round(sqrt(v1 * v1 + v2 * v2)), true);
            }
        }
        final LongLargeArray out = new LongLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        double d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setLong(i, (long) round(Long.MIN_VALUE - d * (sqrt(v1 * v1 + v2 * v2) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            double v1 = re.getDouble(i);
                            double v2 = im.getDouble(i);
                            out.setLong(i, (long) round(sqrt(v1 * v1 + v2 * v2)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a DoubleLargeArray to LongLargeArray.
     *
     * @param in        input array
     * @param normalize if true, then the values are normalized
     * @param min       minimal value for normalization used only if normalize =
     *                  true, it cannot be larger than the minimum value of the input array
     * @param max       maximal value for normalization used only if normalize =
     *                  true, it cannot be smaller than the maximum value of the input array
     *
     * @return long array
     */
    private static LongLargeArray convertToLongLargeArray(final DoubleLargeArray in, final boolean normalize, final float min, final float max)
    {
        long n = in.length();
        if (in.isConstant()) {
            if (normalize) {
                float d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                return new LongLargeArray(n, (long) round(Long.MIN_VALUE - d * (in.getDouble(0) - min)), true);
            } else {
                return new LongLargeArray(n, (long) round(in.getDouble(0)), true);
            }
        }
        final LongLargeArray out = new LongLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    if (normalize) {
                        double d = (Long.MAX_VALUE - Long.MIN_VALUE) / (max - min);
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setLong(i, (long) round(Long.MIN_VALUE - d * (in.getDouble(i) - min)));
                        }
                    } else {
                        for (long i = firstIdx; i < lastIdx; i++) {
                            out.setLong(i, (long) round(in.getDouble(i)));
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an ObjectLargeArray to LongLargeArray.
     *
     * @param in input array
     *
     * @return long array
     */
    private static LongLargeArray convertToLongLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new LongLargeArray(n, (long) round(((DataObjectInterface) in.get(0)).toFloat()), true);
        }
        final LongLargeArray out = new LongLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setLong(i, (long) round(((DataObjectInterface) in.get(i)).toFloat()));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to FloatLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    public static FloatLargeArray convertToFloatLargeArray(final LargeArray in)
    {
        switch (in.getType()) {
            case FLOAT:
                return (FloatLargeArray) in;
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
                return (FloatLargeArray) LargeArrayUtils.convert(in, LargeArrayType.FLOAT);
            case COMPLEX_FLOAT:
                return convertToFloatLargeArray((ComplexFloatLargeArray) in);
            case COMPLEX_DOUBLE:
                return convertToFloatLargeArray((ComplexDoubleLargeArray) in);
            case OBJECT:
                return convertToFloatLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts a ComplexFloatLargeArray to FloatLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    private static FloatLargeArray convertToFloatLargeArray(final ComplexFloatLargeArray in)
    {
        long n = in.length();
        final FloatLargeArray re = in.getRealArray();
        final FloatLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            float v1 = re.getFloat(0);
            float v2 = im.getFloat(0);
            return new FloatLargeArray(n, (float) sqrt(v1 * v1 + v2 * v2), true);
        }
        final FloatLargeArray out = new FloatLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        float v1 = re.getFloat(i);
                        float v2 = im.getFloat(i);
                        out.setFloat(i, (float) sqrt(v1 * v1 + v2 * v2));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexDoubleLargeArray to FloatLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    private static FloatLargeArray convertToFloatLargeArray(final ComplexDoubleLargeArray in)
    {
        long n = in.length();
        final DoubleLargeArray re = in.getRealArray();
        final DoubleLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            double v1 = re.getDouble(0);
            double v2 = im.getDouble(0);
            return new FloatLargeArray(n, (float) sqrt(v1 * v1 + v2 * v2), true);
        }
        final FloatLargeArray out = new FloatLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        double v1 = re.getDouble(i);
                        double v2 = im.getDouble(i);
                        out.setFloat(i, (float) sqrt(v1 * v1 + v2 * v2));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an ObjectLargeArray to FloatLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    private static FloatLargeArray convertToFloatLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new FloatLargeArray(n, (float) ((DataObjectInterface) in.get(0)).toFloat(), true);
        }
        final FloatLargeArray out = new FloatLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setFloat(i, (float) ((DataObjectInterface) in.get(i)).toFloat());
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to DoubleLargeArray.
     *
     * @param in input array
     *
     * @return double array
     */
    public static DoubleLargeArray convertToDoubleLargeArray(final LargeArray in)
    {
        switch (in.getType()) {
            case DOUBLE:
                return (DoubleLargeArray) in;
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case STRING:
                return (DoubleLargeArray) LargeArrayUtils.convert(in, LargeArrayType.DOUBLE);
            case COMPLEX_FLOAT:
                return convertToDoubleLargeArray((ComplexFloatLargeArray) in);
            case COMPLEX_DOUBLE:
                return convertToDoubleLargeArray((ComplexDoubleLargeArray) in);
            case OBJECT:
                return convertToDoubleLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts a ComplexFloatLargeArray to DoubleLargeArray.
     *
     * @param in input array
     *
     * @return double array
     */
    private static DoubleLargeArray convertToDoubleLargeArray(ComplexFloatLargeArray in)
    {

        long n = in.length();
        final FloatLargeArray re = in.getRealArray();
        final FloatLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            float v1 = re.getFloat(0);
            float v2 = im.getFloat(0);
            return new DoubleLargeArray(n, sqrt(v1 * v1 + v2 * v2), true);
        }
        final DoubleLargeArray out = new DoubleLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        float v1 = re.getFloat(i);
                        float v2 = im.getFloat(i);
                        out.setDouble(i, sqrt(v1 * v1 + v2 * v2));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts a ComplexDoubleLargeArray to DoubleLargeArray.
     *
     * @param in input array
     *
     * @return double array
     */
    private static DoubleLargeArray convertToDoubleLargeArray(ComplexDoubleLargeArray in)
    {

        long n = in.length();
        final DoubleLargeArray re = in.getRealArray();
        final DoubleLargeArray im = in.getImaginaryArray();
        if (in.isConstant()) {
            double v1 = re.getDouble(0);
            double v2 = im.getDouble(0);
            return new DoubleLargeArray(n, sqrt(v1 * v1 + v2 * v2), true);
        }
        final DoubleLargeArray out = new DoubleLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        double v1 = re.getDouble(i);
                        double v2 = im.getDouble(i);
                        out.setDouble(i, sqrt(v1 * v1 + v2 * v2));
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts an ObjectLargeArray to DoubleLargeArray.
     *
     * @param in input array
     *
     * @return double array
     */
    private static DoubleLargeArray convertToDoubleLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new DoubleLargeArray(n, ((DataObjectInterface) in.get(0)).toFloat(), true);
        }
        final DoubleLargeArray out = new DoubleLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setDouble(i, ((DataObjectInterface) in.get(i)).toFloat());
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to ComplexFloatLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    public static ComplexFloatLargeArray convertToComplexFloatLargeArray(final LargeArray in)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case STRING:
            case COMPLEX_DOUBLE:
                return (ComplexFloatLargeArray) LargeArrayUtils.convert(in, LargeArrayType.COMPLEX_FLOAT);
            case COMPLEX_FLOAT:
                return (ComplexFloatLargeArray) in;
            case OBJECT:
                return convertToComplexFloatLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts an ObjectLargeArray to ComplexFloatLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    private static ComplexFloatLargeArray convertToComplexFloatLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new ComplexFloatLargeArray(n, new float[]{(float) ((DataObjectInterface) in.get(0)).toFloat(), 0f}, true);
        }
        final ComplexFloatLargeArray out = new ComplexFloatLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setFloat(i, (float) ((DataObjectInterface) in.get(i)).toFloat());
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

    /**
     * Converts any LargeArray to ComplexDoubleLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    public static ComplexDoubleLargeArray convertToComplexDoubleLargeArray(final LargeArray in)
    {
        switch (in.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case STRING:
            case COMPLEX_FLOAT:
                return (ComplexDoubleLargeArray) LargeArrayUtils.convert(in, LargeArrayType.COMPLEX_DOUBLE);
            case COMPLEX_DOUBLE:
                return (ComplexDoubleLargeArray) in;
            case OBJECT:
                return convertToComplexDoubleLargeArray((ObjectLargeArray) in);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Converts an ObjectLargeArray to ComplexDoubleLargeArray.
     *
     * @param in input array
     *
     * @return float array
     */
    private static ComplexDoubleLargeArray convertToComplexDoubleLargeArray(final ObjectLargeArray in)
    {
        long n = in.length();
        if (in.isConstant()) {
            return new ComplexDoubleLargeArray(n, new double[]{(float) ((DataObjectInterface) in.get(0)).toFloat(), 0}, true);
        }
        final ComplexDoubleLargeArray out = new ComplexDoubleLargeArray(n, false);
        int nthreads = (int) min(ConcurrencyUtils.getNumberOfThreads(), n);
        long k = n / nthreads;
        Future<?>[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? n : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Runnable()
            {
                @Override
                public void run()
                {
                    for (long i = firstIdx; i < lastIdx; i++) {
                        out.setDouble(i, (float) ((DataObjectInterface) in.get(i)).toFloat());
                    }

                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            throw new IllegalStateException(ex);
        }
        return out;
    }

}

/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.Set;
import java.util.List;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import org.visnow.jscic.Field;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jlargearrays.LogicLargeArray;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jscic.IrregularField;
import org.visnow.jscic.PointField;
import org.visnow.jscic.dataarrays.DataObjectInterface;

/**
 * Test regularField generator. Generates certain set of fields and write them
 * to temporary directory.
 * <p>
 * TODO Time unit support.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 *
 */
public class TestFieldGenerator {

    /**
     * Supported regularField dimensions. Adding Integers other than 1, 2 or 3
     * will probably result in spectacular failures.
     */
    private static final Set<Integer> SUPPORTED_DIMENSIONS;

    static {
        SUPPORTED_DIMENSIONS = new TreeSet<>();

        SUPPORTED_DIMENSIONS.add(1);
        SUPPORTED_DIMENSIONS.add(2);
        SUPPORTED_DIMENSIONS.add(3);
    }

    /**
     * Supported types of components. Hope the following set will be equal
     * DataArrayType someday.
     */
    private static final Set<DataArrayType> SUPPORTED_TYPES;

    static {
        SUPPORTED_TYPES = new TreeSet<>();

        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_LOGIC);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_BYTE);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_SHORT);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_INT);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_LONG);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_FLOAT);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_DOUBLE);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_COMPLEX);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_STRING);
        SUPPORTED_TYPES.add(DataArrayType.FIELD_DATA_OBJECT);
    }

    /**
     * Supported vector spaces. Any positive integer should do, but most use
     * cases lie in 1, 2, 3.
     */
    private static final Set<Integer> SUPPORTED_VECLENS;

    static {
        SUPPORTED_VECLENS = new TreeSet<>();

        SUPPORTED_VECLENS.add(1);
        SUPPORTED_VECLENS.add(2);
        SUPPORTED_VECLENS.add(3);
        SUPPORTED_VECLENS.add(5);
        SUPPORTED_VECLENS.add(9);
        SUPPORTED_VECLENS.add(16);
    }

    /**
     * Base size of data set. Assumed veclen = 1, and single timestep. Keep it
     * at least 60 (and complex enough - not a prime for sure), so that it can
     * be destributed between three dimensions.
     */
    private static final int LENGTH = 60;

    /**
     * List of timesteps to generate. Each component with "time" feature would
     * generate some data for these.
     */
    private static final ArrayList<Float> TIMESTEPS;

    static {
        TIMESTEPS = new ArrayList<>();

        TIMESTEPS.add(0.0f);
        TIMESTEPS.add(0.78f);
        TIMESTEPS.add(1.92f);
        TIMESTEPS.add(2.99f);
        TIMESTEPS.add(9.12f);
    }

    /**
     * Field geometry. Represents possible regularField geometries.
     *
     * @see #addGeometry
     */
    private static enum Geometry {
        AFFINE_ORTHOGONAL("afforthogonal"),
        AFFINE_SHEARED("affsheared"),
        EXPLICIT_TIME("timeexplicit"),
        EXPLICIT("explicit");

        private final String name;

        private Geometry(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    /**
     * Field mask. Represents possible types of regularField masks.
     *
     * @see #addMask
     */
    private static enum Mask {
        CONSTANT("masked"),
        TIME_DEPENDENT("timemasked");

        private final String name;

        private Mask(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    /**
     * List of created fields. Holds all of the test fields created in
     * constructor.
     */
    private final List<Field> output;

    /**
     * Public constructor. Creates list of fields, which can be obtained by
     * "get" method.
     *
     * @see #get()
     */
    public TestFieldGenerator() {
        output = new ArrayList<>();

        for (Integer dim : SUPPORTED_DIMENSIONS) {
            for (Geometry geom : Geometry.values()) {
                RegularField regularField = createRegularField(dim, geom);

                output.add(regularField);
                IrregularField irregularField1 = FieldUtils.convertToIrregular(regularField);
                output.add(irregularField1);
                IrregularField irregularField2 = irregularField1.cloneDeep();
                FieldUtils.convertNodeDataToCellData(irregularField2);
                output.add(irregularField2);
                for (Mask mask : Mask.values()) {
                    addMask(dim, mask, createRegularField(dim, geom));
                    output.add(regularField);
                    irregularField1 = FieldUtils.convertToIrregular(regularField);
                    output.add(irregularField1);
                    irregularField2 = irregularField1.cloneDeep();
                    FieldUtils.convertNodeDataToCellData(irregularField2);
                    output.add(irregularField2);
                }
            }
        }
        output.add(createPointField(Geometry.EXPLICIT));
        output.add(createPointField(Geometry.EXPLICIT_TIME));
        output.add(createPointField(Geometry.EXPLICIT, Mask.CONSTANT));
        output.add(createPointField(Geometry.EXPLICIT_TIME, Mask.CONSTANT));
        output.add(createPointField(Geometry.EXPLICIT, Mask.TIME_DEPENDENT));
        output.add(createPointField(Geometry.EXPLICIT_TIME, Mask.TIME_DEPENDENT));

    }

    /**
     * Creates dims array. In other words - distributes SIZE into particular
     * dimensions. Only dimensions 1, 2 and 3 ar supported for now.
     *
     * @param dim Field dimension; must be in the list of SUPPORTED_DIMENSIONS
     *
     * @see #SUPPORTED_DIMENSIONS
     *
     * @return LDims array
     */
    private static long[] createDims(int dim) {
        final long FIRST_DIMENSION = 5;
        final long SECOND_DIMENSION = 3;

        long[] dims = null;
        switch (dim) {
            case 1:
                dims = new long[]{LENGTH};
                break;
            case 2:
                dims = new long[]{FIRST_DIMENSION, LENGTH / FIRST_DIMENSION};
                break;
            case 3:
                dims = new long[]{FIRST_DIMENSION, SECOND_DIMENSION, LENGTH / FIRST_DIMENSION / SECOND_DIMENSION};
                break;
            default:
                throw new RuntimeException("Not supported dimension: " + dim);
        }
        return dims;
    }

    /**
     * Creates a regular regularField. The regularField is created based on only
     * dimension and geometry type (geometry is added to the regularField at the
     * end). List of components is defined inside
     *
     * @param dim Field dimension; must be in the list of SUPPORTED_DIMENSIONS
     *
     * @see #SUPPORTED_DIMENSIONS
     *
     * @param geom Type of geometry to be added to the resulting regular
     * regularField
     *
     * @see #addGeometry
     *
     * @return Resulting regular regularField
     */
    private static RegularField createRegularField(int dim, Geometry geom) {
        RegularField regularField = new RegularField(createDims(dim));

        /* Also serves as a name of the file (with .vnf extension and some tuning) */
        regularField.setName("testfield_" + dim + "d");

        for (DataArrayType type : DataArrayType.values()) {
            if (!SUPPORTED_TYPES.contains(type)) {
                continue;
            }

            for (Integer veclen : SUPPORTED_VECLENS) {
                regularField.addComponent(createComponent(type, veclen, false, false, false));
                regularField.addComponent(createComponent(type, veclen, true, false, false));
                regularField.addComponent(createComponent(type, veclen, false, true, false));
                regularField.addComponent(createComponent(type, veclen, true, true, false));
                regularField.addComponent(createComponent(type, veclen, false, false, true));
                regularField.addComponent(createComponent(type, veclen, true, false, true));
                regularField.addComponent(createComponent(type, veclen, false, true, true));
                regularField.addComponent(createComponent(type, veclen, true, true, true));
            }
        }
        addGeometry(dim, geom, regularField);
        return regularField;
    }

    /**
     * Creates a pointField of size LENGTH. List of components is defined inside
     *
     * @param geom Type of geometry (if EXPLICIT_TIME, time coords are created,
     * otherwise single geometry is created)
     *
     * @return Resulting PointField
     */
    private static PointField createPointField(Geometry geometry) {
        PointField pointField = new PointField(LENGTH);
        FloatLargeArray coords = new FloatLargeArray(3 * LENGTH);
        for (long i = 0; i < 3 * LENGTH; i++) {
            coords.set(i, (float) Math.random());
        }
        if (geometry == Geometry.EXPLICIT_TIME) {
            ArrayList<LargeArray> coordsSeries = new ArrayList<>();
            ArrayList<Float> timeSeries = new ArrayList<>();
            for (Float t : TIMESTEPS) {
                FloatLargeArray crds = new FloatLargeArray(3 * LENGTH);
                for (long i = 0; i < 3 * LENGTH; i++) {
                    float f = coords.get(i);
                    crds.set(i, f + .1f * t * f * f);
                }
                coordsSeries.add(crds);
                timeSeries.add(t + 0.12f); // Delay timesteps a bit, so that they don't match the ones found originally in the data
            }
            pointField.setCoords(new TimeData(timeSeries, coordsSeries, timeSeries.get(0)));
        } else {
            pointField.setCoords(coords, 0);
        }
        for (DataArrayType type : DataArrayType.values()) {
            if (SUPPORTED_TYPES.contains(type)) {
                for (Integer veclen : SUPPORTED_VECLENS) {
                    pointField.addComponent(createComponent(type, veclen, false, false, false));
                    pointField.addComponent(createComponent(type, veclen, true, false, false));
                    pointField.addComponent(createComponent(type, veclen, false, true, false));
                    pointField.addComponent(createComponent(type, veclen, true, true, false));
                    pointField.addComponent(createComponent(type, veclen, false, false, true));
                    pointField.addComponent(createComponent(type, veclen, true, false, true));
                    pointField.addComponent(createComponent(type, veclen, false, true, true));
                    pointField.addComponent(createComponent(type, veclen, true, true, true));
                }
            }
        }
        return pointField;
    }

    /**
     * Creates a pointField of size LENGTH. List of components is defined inside
     *
     * @param geom Type of geometry (if EXPLICIT_TIME, time coords are created,
     * otherwise single geometry is created)
     * @param mask (CONSTANT - constant mask, TIME_DEPENDENT - time mask
     * @return Resulting PointField
     */
    private static PointField createPointField(Geometry geometry, Mask mask) {
        PointField pointField = createPointField(geometry);
        if (mask == Mask.CONSTANT) {
            LogicLargeArray valid = new LogicLargeArray(LENGTH);
            for (long i = 0; i < LENGTH; i++) {
                valid.setBoolean(i, Math.random() > .7);
            }
            pointField.addMask(valid);
        }
        if (mask == Mask.TIME_DEPENDENT) {
            ArrayList<LargeArray> maskSeries = new ArrayList<>();
            ArrayList<Float> maskTimeSeries = new ArrayList<>();
            for (Float t : TIMESTEPS) {
                LogicLargeArray valid = new LogicLargeArray(LENGTH);
                for (long i = 0; i < LENGTH; i++) {
                    valid.setBoolean(i, Math.random() > .7);
                }
                maskSeries.add(valid);
                maskTimeSeries.add(t + 0.08f);
            }
            pointField.setMask(new TimeData(maskTimeSeries, maskSeries, maskTimeSeries.get(0)));
        }
        return pointField;
    }

    private static class DataObjectFloat implements DataObjectInterface, java.io.Serializable {

        private static final long serialVersionUID = 8838577772896081809L;

        private final float value;

        public DataObjectFloat(float v) {
            this.value = v;
        }

        @Override
        public float toFloat() {
            return value;
        }

        @Override
        public int hashCode() {
            return 29 * super.hashCode() + new Float(value).hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o == null || !(o instanceof DataObjectFloat)) {
                return false;
            }
            DataObjectFloat daf = (DataObjectFloat) o;
            return this.toFloat() == daf.toFloat();
        }

    }

    private static ObjectLargeArray generateRandomObjectLargeArray(long length) {
        Random r = new Random(0);
        ObjectLargeArray la = new ObjectLargeArray(length);
        for (long i = 0; i < length; i++) {
            la.set(i, new DataObjectFloat(r.nextFloat()));
        }
        return la;
    }

    /**
     * Creates a component. Component of a given type (in future of any
     * JLargeArrays supported type; see SUPPORTED_TYPES for now) is being filled
     * with random values.
     * <p>
     * <p>
     * Naming conventions:</br>
     * Component is named [type][veclen][features] (example is below), where
     * [features] can be:
     * <ul>
     * <li><b>t</b> - component contains several timesteps (defined by
     * TIMESTEPS)</li>
     * <li><b>p</b> - component has some physical extents (always the same, no
     * matter the type)</li>
     * <li><b>u</b> - component has a unit (always the same, no matter the
     * type)</li>
     * </ul>
     * For example - shorts9tp - component of type short, with vector length of
     * 9, with several timesteps and hard-coded physical extents.
     * </p>
     *
     * @param type Type of resulting DataArray
     *
     * @see #SUPPORTED_TYPES
     *
     * @param veclen Vector length
     *
     * @see #SUPPORTED_VECLENS
     *
     * @param time Set to true to generate several timesteps
     *
     * @see #TIMESTEPS
     *
     * @param phys Set to true to add physical extents (note they are hard-coded
     * for now, not random)
     *
     * @param unit Set to true to add component unit (note it is hard-coded for
     * now, not random)
     *
     * @return Resulting DataArray (regularField component)
     */
    private static DataArray createComponent(DataArrayType type, int veclen, boolean time, boolean phys, boolean unit) {
        String suffix = "s" + veclen
                + ((time) ? "t" : "")
                + ((phys) ? "p" : "")
                + ((unit) ? "u" : "");

        int length = LENGTH * veclen;

        /* Generate the data */
        Object data;
        if (time) {
            ArrayList<LargeArray> dataSeries = new ArrayList<>();

            if (type != DataArrayType.FIELD_DATA_OBJECT) {
                for (Float t : TIMESTEPS) {
                    dataSeries.add(LargeArrayUtils.generateRandom(type.toLargeArrayType(), length));
                }
            } else {
                for (Float t : TIMESTEPS) {
                    dataSeries.add(generateRandomObjectLargeArray(length));
                }
            }
            data = new TimeData(TIMESTEPS, dataSeries, TIMESTEPS.get(0));
        } else {
            if (type != DataArrayType.FIELD_DATA_OBJECT) {
                data = LargeArrayUtils.generateRandom(type.toLargeArrayType(), length);
            } else {
                data = generateRandomObjectLargeArray(length);
            }
        }
        /* Create and modify the component */
        DataArray component = DataArray.create(data, veclen, type + suffix);
        if (phys) {
            component.setPreferredRanges(-12.4, 199.2, -10.3, 101.2);
        }
        if (unit) {
            component.setUnit("kg/m/m");
        }

        return component;
    }

    /**
     * Creates geometry. Also adds it to a given regularField.
     *
     * @param dim Field dimension; must be in the list of SUPPORTED_DIMENSIONS
     *
     * @see #SUPPORTED_DIMENSIONS
     *
     * @param geom Geometry to be created
     *
     * @param field Resulting geometry will be added to this RegularField
     *
     * @return Input regularField with added geometry (return regularField would
     * have the same reference as input one)
     */
    private static void addGeometry(int dim, Geometry geom, RegularField field) {
        /* Giving proper name is a crucial step, as file names are generated from regularField names.
         * If some fields generated from different parameters share the same
         * name, the file will be overwritten and one of the fields lost. */
        field.setName(field.getName() + "_" + geom);

        float[][] affine = new float[4][3];
        switch (geom) {
            case AFFINE_ORTHOGONAL:
                affine[3] = new float[]{1.4f, 11.2f, 2.97f};
                affine[0] = new float[]{6.12f, 0.0f, 0.0f};
                affine[1] = new float[]{0.0f, 4.89f, 0.0f};
                affine[2] = new float[]{0.0f, 0.0f, 5.43f};
                field.setAffine(affine);
                break;
            case AFFINE_SHEARED:
                affine[3] = new float[]{-0.4f, -3.112f, 3.97f};
                affine[0] = new float[]{1.24f, 5.12f, -1.11f};
                affine[1] = new float[]{4.87f, -1.331f, -1.12f};
                affine[2] = new float[]{-0.74f, -1.21f, 4.43f};
                field.setAffine(affine);
                break;
            case EXPLICIT:
            case EXPLICIT_TIME:
                ArrayList<LargeArray> coordsSeries = new ArrayList<>();
                ArrayList<Float> timeSeries = new ArrayList<>();

                long[] dims = field.getLDims();

                for (Float t : TIMESTEPS) {
                    if (geom == Geometry.EXPLICIT) /* Produce only one timestep, when geometry is not defined as time-dependent */ {
                        if (!Objects.equals(t, TIMESTEPS.get(0))) {
                            continue;
                        }
                    }

                    float r, phi, z;
                    final float innerRadius = 5.0f + t;
                    final float outerRadius = 10.0f + 10.0f * t;
                    final float[][] extents = {{-4.0f - t, -2.3f - t, -1.11f - t}, {2.3f + 2.0f * t, 3.9f + 2.0f * t, 4.7f + 2.0f * t}};
                    final float[] sizes = new float[]{extents[1][0] - extents[0][0], extents[1][1] - extents[0][1], extents[1][2] - extents[0][2]};

                    FloatLargeArray coords = new FloatLargeArray(3 * LENGTH);
                    switch (dim) {
                        case 3:
                            for (int k = 0; k < dims[2]; ++k) {
                                for (int j = 0; j < dims[1]; ++j) {
                                    for (int i = 0; i < dims[0]; ++i) {
                                        r = innerRadius + (outerRadius - innerRadius) / (float) (dims[1] - 1) * (float) j;
                                        phi = 2.0f * (float) Math.PI / (float) (dims[0] - 1) * (float) i * (1.0f / 3.0f);
                                        z = extents[0][2] + sizes[2] / (float) (dims[2] - 1) * (float) k;

                                        coords.set(3 * (i + dims[0] * (j + dims[1] * k)) + 0, r * (float) Math.cos(phi)); // X coordinate
                                        coords.set(3 * (i + dims[0] * (j + dims[1] * k)) + 1, r * (float) Math.sin(phi)); // Y coordinate
                                        coords.set(3 * (i + dims[0] * (j + dims[1] * k)) + 2, z); // Z coordinate
                                    }
                                }
                            }
                            break;
                        case 2:
                            z = -12.395f;
                            for (int j = 0; j < dims[1]; ++j) {
                                for (int i = 0; i < dims[0]; ++i) {
                                    r = innerRadius + (outerRadius - innerRadius) / (float) (dims[1] - 1) * (float) j;
                                    phi = 2.0f * (float) Math.PI / (float) (dims[0] - 1) * (float) i * (1.0f / 3.0f);

                                    coords.set(3 * (i + dims[0] * j) + 0, z + 3.0f * (float) i); // X coordinate
                                    coords.set(3 * (i + dims[0] * j) + 1, r * (float) Math.cos(phi)); // Y coordinate
                                    coords.set(3 * (i + dims[0] * j) + 2, r * (float) Math.sin(phi)); // Z coordinate
                                }
                            }
                            break;
                        case 1:
                            r = -12.23f;
                            z = -8.936f;
                            for (int i = 0; i < dims[0]; ++i) {
                                phi = 2.0f * (float) Math.PI / (float) (dims[0] - 1) * (float) i * (1.0f / 3.0f);

                                coords.set(3 * i + 0, r * (float) Math.cos(phi)); // X coordinate
                                coords.set(3 * i + 1, z + 0.1f * (float) i + t); // Y coordinate
                                coords.set(3 * i + 2, r * (float) Math.sin(phi)); // Z coordinate
                            }
                            break;
                        default:
                            throw new RuntimeException("Not supported field dimension " + dim);
                    }
                    if (geom == Geometry.EXPLICIT) {
                        field.setCurrentCoords(coords);
                    }
                    coordsSeries.add(coords);
                    timeSeries.add(t + 0.12f); // Delay timesteps a bit, so that they don't match the ones found originally in the data
                }
                field.setCoords(new TimeData(timeSeries, coordsSeries, timeSeries.get(0)));
                break;
            default:
                throw new RuntimeException("Not recognized type of geometry");
        }
    }

    /**
     * Creates sample mask. Also adds it to a given regularField.
     *
     * @param dim Field dimension; must be in the list of SUPPORTED_DIMENSIONS
     *
     * @see #SUPPORTED_DIMENSIONS
     *
     * @param mask Mask type to be created
     *
     * @param field Resulting mask will be added to this RegularField
     *
     * @return Input regularField with added mask (return regularField would
     * have the same reference as input one)
     */
    private static void addMask(int dim, Mask mask, RegularField field) {
        /* Giving proper name is a crucial step, as file names are generated from regularField names.
         * If some fields generated from different parameters share the same
         * name, the file will be overwritten and one of the fields lost. */
        field.setName(field.getName() + "_" + mask);

        long[] dims = createDims(dim);

        long ii;
        float x0, y0, z0;

        ArrayList<LargeArray> maskSeries = new ArrayList<>();
        ArrayList<Float> timeSeries = new ArrayList<>();

        for (Float t : TIMESTEPS) {
            if (mask == Mask.CONSTANT) /* Produce only one timestep, when mask is not defined as time-dependent */ {
                if (!Objects.equals(t, TIMESTEPS.get(0))) {
                    continue;
                }
            }

            LogicLargeArray maskArray = new LogicLargeArray(LENGTH);
            switch (dim) {
                case 3:
                    x0 = (float) dims[0] / 3.0f;
                    y0 = (float) dims[1] / 3.0f;
                    z0 = (float) dims[2] / 3.0f;

                    for (long i = 0; i < dims[0]; ++i) {
                        for (long j = 0; j < dims[1]; ++j) {
                            for (long k = 0; k < dims[2]; ++k) {
                                ii = i + dims[0] * (j + dims[1] * k);

                                float x = (float) i;
                                float y = (float) j - t / 3.5f;
                                float z = (float) k;

                                if (x / x0 + y / y0 + z / z0 <= 1.0f) {
                                    maskArray.set(ii, false);
                                } else {
                                    maskArray.set(ii, true);
                                }
                            }
                        }
                    }
                    break;
                case 2:
                    x0 = (float) dims[0] * 0.33f;
                    y0 = (float) dims[1] * 0.67f;

                    for (long i = 0; i < dims[0]; ++i) {
                        for (long j = 0; j < dims[1]; ++j) {
                            ii = i + dims[0] * j;

                            float x = (float) i;
                            float y = (float) j - t / 3.5f;

                            if (x / x0 + y / y0 <= 1.0f) {
                                maskArray.set(ii, false);
                            } else {
                                maskArray.set(ii, true);
                            }
                        }
                    }
                    break;
                case 1:
                    for (long i = 0; i < maskArray.length(); ++i) {
                        if (i % (3 + (int) Math.floor(t / 3.0f)) == 0) {
                            maskArray.set(i, false);
                        } else {
                            maskArray.set(i, true);
                        }
                    }
                    break;
                default:
                    throw new RuntimeException("Not supported dimension: " + dim);
            }
            if (mask == Mask.CONSTANT) {
                field.setCurrentMask(maskArray);
            }
            maskSeries.add(maskArray);
            timeSeries.add(t + 0.27f); // Delay timesteps a bit, so that they don't match the ones found originally in the data
        }
        field.setMask(new TimeData(timeSeries, maskSeries, timeSeries.get(0)));
    }

    /**
     * Get list of fields. Currently only regular ones.
     *
     * @return List of fields
     */
    public List<Field> get() {
        return new ArrayList<>(output);
    }
}

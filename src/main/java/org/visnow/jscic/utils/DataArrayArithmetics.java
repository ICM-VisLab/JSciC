/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * Arithmetical operations on DataArrays.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class DataArrayArithmetics
{

    /**
     * <table border="1">
     * <caption>Addition of two DataArrays (da1 + da2) computed in a single precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar + scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar + vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector + scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>addition of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 + da2
     */
    public static DataArray addF(DataArray da1, DataArray da2)
    {
        return addF(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Addition of two DataArrays (da1 + da2) computed in a single precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar + scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar + vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector + scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>addition of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 + da2
     */
    public static DataArray addF(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.ADD.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Addition of two DataArrays (da1 + da2) computed in a double precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar + scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar + vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector + scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>addition of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 + da2
     */
    public static DataArray addD(DataArray da1, DataArray da2)
    {
        return addD(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Addition of two DataArrays (da1 + da2) computed in a double precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar + scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar + vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector + scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>addition of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 + da2
     */
    public static DataArray addD(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.ADD.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Computes a*x+b in a single precision, where a and b are constant data arrays. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>x</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>axpy of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise axpy of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param a input DataArray (constant)
     * @param x input DataArray
     * @param b input DataArray (constant)
     *
     * @return a*x+b
     */
    public static DataArray axpyF(DataArray a, DataArray x, DataArray b)
    {
        return axpyF(a, x, b, true);
    }

    /**
     * <table border="1">
     * <caption>Computes a*x+b in a single precision, where a and b are constant data arrays.</caption>
     * <tr>
     * <td><b>x</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>axpy of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise axpy of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param a           input DataArray (constant)
     * @param x           input DataArray
     * @param b           input DataArray (constant)
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return a*x+b
     */
    public static DataArray axpyF(DataArray a, DataArray x, DataArray b, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{a, x, b};
        DataArrayType outType;
        if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX || b.getType() == DataArrayType.FIELD_DATA_COMPLEX || x.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.AXPY.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Computes a*x+b in a single precision, where a and b are constant data arrays. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>x</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>axpy of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise axpy of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param a input DataArray (constant)
     * @param x input DataArray
     * @param b input DataArray (constant)
     *
     * @return a*x+b
     */
    public static DataArray axpyD(DataArray a, DataArray x, DataArray b)
    {
        return axpyD(a, x, b, true);
    }

    /**
     * <table border="1">
     * <caption>Computes a*x+b in a single precision, where a and b are constant data arrays.</caption>
     * <tr>
     * <td><b>x</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>axpy of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise axpy of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param a           input DataArray (constant)
     * @param x           input DataArray
     * @param b           input DataArray (constant)
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return a*x+b
     */
    public static DataArray axpyD(DataArray a, DataArray x, DataArray b, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{a, x, b};
        DataArrayType outType;
        if (a.getType() == DataArrayType.FIELD_DATA_COMPLEX || b.getType() == DataArrayType.FIELD_DATA_COMPLEX || x.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.AXPY.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Subtraction of two DataArrays (da1 - da2) computed in a single precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar - scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar - vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector - scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>subtraction of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 - da2
     */
    public static DataArray diffF(DataArray da1, DataArray da2)
    {
        return diffF(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Subtraction of two DataArrays (da1 - da2) computed in a single precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar - scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar - vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector - scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>subtraction of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 - da2
     */
    public static DataArray diffF(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.DIFF.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Subtraction of two DataArrays (da1 - da2) computed in a double precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar - scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar - vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector - scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>subtraction of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 - da2
     */
    public static DataArray diffD(DataArray da1, DataArray da2)
    {
        return diffD(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Subtraction of two DataArrays (da1 - da2) computed in a double precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar - scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar - vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector - scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>subtraction of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 - da2
     */
    public static DataArray diffD(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.DIFF.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Multiplication of two DataArrays (da1 * da2) computed in a single precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar * scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar * vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector * scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>dot product of two vectors of equal length</td><td>scalar</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 * da2
     */
    public static DataArray multF(DataArray da1, DataArray da2)
    {
        return multF(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Multiplication of two DataArrays (da1 * da2) computed in a single precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar * scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar * vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector * scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>dot product of two vectors of equal length</td><td>scalar</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 * da2
     */
    public static DataArray multF(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.MULT.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Multiplication of two DataArrays (da1 * da2) computed in a double precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar * scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar * vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector * scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>dot product of two vectors of equal length</td><td>scalar</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 * da2
     */
    public static DataArray multD(DataArray da1, DataArray da2)
    {
        return multD(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Multiplication of two DataArrays (da1 * da2) computed in a double precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar * scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar * vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector * scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>dot product of two vectors of equal length</td><td>scalar</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 * da2
     */
    public static DataArray multD(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.MULT.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Division of two DataArrays (da1 / da2) computed in a single precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar / scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar / vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector / scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise division of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 / da2
     */
    public static DataArray divF(DataArray da1, DataArray da2)
    {
        return divF(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Division of two DataArrays (da1 / da2) computed in a single precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar / scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar / vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector / scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise division of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 / da2
     */
    public static DataArray divF(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.DIV.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Division of two DataArrays (da1 / da2) computed in a double precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar / scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar / vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector / scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise division of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1 / da2
     */
    public static DataArray divD(DataArray da1, DataArray da2)
    {
        return divD(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Division of two DataArrays (da1 / da2) computed in a double precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar / scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>scalar / vector</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>vector / scalar</td><td>vector</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise division of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1 / da2
     */
    public static DataArray divD(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.DIV.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Power of DataArray (da ^ n) computed in a single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>power of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise power of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     * @param n  exponent
     *
     * @return da^n
     */
    public static DataArray powF(DataArray da, double n)
    {
        return powF(da, n, true);
    }

    /**
     * <table border="1">
     * <caption>Power of DataArray (da ^ n) computed in a single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>power of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise power of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param n           exponent
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return da^n
     */
    public static DataArray powF(DataArray da, double n, boolean ignoreUnits)
    {
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, 1, n);
        DataArray[] in = new DataArray[]{da, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.POW.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Power of two DataArrays (da1 ^ da2) computed in a single precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar ^ scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>element-wise power of scalar and vector</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>element-wise power of vector and scalar</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise power of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1^da2
     */
    public static DataArray powF(DataArray da1, DataArray da2)
    {
        return powF(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Power of two DataArrays (da1 ^ da2) computed in a single precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar ^ scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>element-wise power of scalar and vector</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>element-wise power of vector and scalar</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise power of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1^da2
     */
    public static DataArray powF(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.POW.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Power of DataArray computed in a double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>power of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise power of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     * @param n  exponent
     *
     * @return da^n
     */
    public static DataArray powD(DataArray da, double n)
    {
        return powD(da, n, true);
    }

    /**
     * <table border="1">
     * <caption>Power of DataArray computed in a double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>power of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise power of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param n           exponent
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return da^n
     */
    public static DataArray powD(DataArray da, double n, boolean ignoreUnits)
    {
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, 1, n);
        DataArray[] in = new DataArray[]{da, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.POW.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Power of two DataArrays (da1 ^ da2) computed in a double precision. Units associated with input data arrays are ignored.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar ^ scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>element-wise power of scalar and vector</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>element-wise power of vector and scalar</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise power of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1 input DataArray
     * @param da2 input DataArray
     *
     * @return da1^da2
     */
    public static DataArray powD(DataArray da1, DataArray da2)
    {
        return powD(da1, da2, true);
    }

    /**
     * <table border="1">
     * <caption>Power of two DataArrays (da1 ^ da2) computed in a double precision.</caption>
     * <tr>
     * <td><b>da1</b></td><td><b>da2</b></td> <td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>scalar</td> <td>scalar ^ scalar</td><td>scalar</td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>vector</td> <td>element-wise power of scalar and vector</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>scalar</td> <td>element-wise power of vector and scalar</td><td>-</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>vector</td> <td>element-wise power of two vectors of equal length</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da1         input DataArray
     * @param da2         input DataArray
     * @param ignoreUnits if true, then units associated with input data arrays are ignored
     *
     * @return da1^da2
     */
    public static DataArray powD(DataArray da1, DataArray da2, boolean ignoreUnits)
    {
        DataArray[] in = new DataArray[]{da1, da2};
        DataArrayType outType = DataArrayOperator.getLargestNumericType(in);
        if (outType != DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.POW.evaluate(outType, in, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Negation of DataArray computed in a single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>negation of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise negation of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return -da
     */
    public static DataArray negF(DataArray da)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.NEG.evaluate(outType, new DataArray[]{da}, false);
    }

    /**
     * <table border="1">
     * <caption>Negation of DataArray computed in a double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>negation of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise negation of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return -da
     */
    public static DataArray negD(DataArray da)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.NEG.evaluate(outType, new DataArray[]{da}, false);
    }

    /**
     * <table border="1">
     * <caption>Square root of DataArray in single precision. For complex arrays the principal square root is returned. The unit associated with the input data array is
     * ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>square root of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise square root of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return sqrt(da)
     */
    public static DataArray sqrtF(DataArray da)
    {
        return sqrtF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Square root of DataArray in single precision. For complex arrays the principal square root is returned.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>square root of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise square root of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return sqrt(da)
     */
    public static DataArray sqrtF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.SQRT.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Square root of DataArray in double precision. For complex arrays the principal square root is returned. The unit associated with the input data array is
     * ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>square root of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise square root of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return sqrt(da)
     */
    public static DataArray sqrtD(DataArray da)
    {
        return sqrtD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Square root of DataArray in double precision. For complex arrays the principal square root is returned.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>square root of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise square root of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return sqrt(da)
     */
    public static DataArray sqrtD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.SQRT.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Natural logarithm of DataArray in single precision. For complex arrays the principal value logarithm is returned. The unit associated with the input data
     * array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>natural logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise natural logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return log(da)
     */
    public static DataArray logF(DataArray da)
    {
        return logF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Natural logarithm of DataArray in single precision. For complex arrays the principal value logarithm is returned.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>natural logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise natural logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return log(da)
     */
    public static DataArray logF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.LOG.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Natural logarithm of DataArray in double precision. For complex arrays the principal value logarithm is returned. The unit associated with the input data
     * array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>natural logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise natural logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return log(da)
     */
    public static DataArray logD(DataArray da)
    {
        return logD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Natural logarithm of DataArray in double precision. For complex arrays the principal value logarithm is returned.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>natural logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise natural logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return log(da)
     */
    public static DataArray logD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.LOG.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Base 10 logarithm of DataArray in single precision. For complex arrays the principal value logarithm is returned. The unit associated with the input data
     * array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>base-10 logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise base-10 logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return log10(da)
     */
    public static DataArray log10F(DataArray da)
    {
        return log10F(da, true);
    }

    /**
     * <table border="1">
     * <caption>Base 10 logarithm of DataArray in single precision. For complex arrays the principal value logarithm is returned.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>base-10 logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise base-10 logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return log10(da)
     */
    public static DataArray log10F(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.LOG10.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Base 10 logarithm of DataArray in double precision. For complex arrays the principal value logarithm is returned. The unit associated with the input data
     * array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>base-10 logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise base-10 logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return log10(da)
     */
    public static DataArray log10D(DataArray da)
    {
        return log10D(da, true);
    }

    /**
     * <table border="1">
     * <caption>Base 10 logarithm of DataArray in double precision. For complex arrays the principal value logarithm is returned.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>base-10 logarithm of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise base-10 logarithm of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return log10(da)
     */
    public static DataArray log10D(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.LOG10.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Exponent of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>exponent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise exponent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return exp(da)
     */
    public static DataArray expF(DataArray da)
    {
        return expF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Exponent of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>exponent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise exponent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return exp(da)
     */
    public static DataArray expF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.EXP.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Exponent of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>exponent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise exponent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return exp(da)
     */
    public static DataArray expD(DataArray da)
    {
        return expD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Exponent of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>exponent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise exponent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return exp(da)
     */
    public static DataArray expD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.EXP.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Absolute value of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>absolute value of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>Euclidean norm of vector</td><td>scalar</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return abs(da)
     */
    public static DataArray absF(DataArray da)
    {
        return DataArrayOperator.ABS.evaluate(DataArrayType.FIELD_DATA_FLOAT, new DataArray[]{da}, false);
    }

    /**
     * <table border="1">
     * <caption>Absolute value of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>absolute value of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>Euclidean norm of vector</td><td>scalar</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return abs(da)
     */
    public static DataArray absD(DataArray da)
    {
        return DataArrayOperator.ABS.evaluate(DataArrayType.FIELD_DATA_DOUBLE, new DataArray[]{da}, false);
    }

    /**
     * <table border="1">
     * <caption>Sine of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return sin(da)
     */
    public static DataArray sinF(DataArray da)
    {
        return sinF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Sine of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return sin(da)
     */
    public static DataArray sinF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.SIN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Sine of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da input DataArray
     *
     * @return sin(da)
     */
    public static DataArray sinD(DataArray da)
    {
        return sinD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Sine of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return sin(da)
     */
    public static DataArray sinD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.SIN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Cosine of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da input DataArray
     *
     * @return cos(da)
     */
    public static DataArray cosF(DataArray da)
    {
        return cosF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Cosine of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return cos(da)
     */
    public static DataArray cosF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.COS.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Cosine of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return cos(da)
     */
    public static DataArray cosD(DataArray da)
    {
        return cosD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Cosine of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return cos(da)
     */
    public static DataArray cosD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.COS.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Tangent of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return tan(da)
     */
    public static DataArray tanF(DataArray da)
    {
        return tanF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Tangent of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return tan(da)
     */
    public static DataArray tanF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.TAN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Tangent of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return tan(da)
     */
    public static DataArray tanD(DataArray da)
    {
        return tanD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Tangent of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return tan(da)
     */
    public static DataArray tanD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.TAN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Inverse sine of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return asin(da)
     */
    public static DataArray asinF(DataArray da)
    {
        return asinF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Inverse sine of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return asin(da)
     */
    public static DataArray asinF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.ASIN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Inverse sine of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return asin(da)
     */
    public static DataArray asinD(DataArray da)
    {
        return asinD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Inverse sine of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse sine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse sine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return asin(da)
     */
    public static DataArray asinD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.ASIN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Inverse cosine of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return acos(da)
     */
    public static DataArray acosF(DataArray da)
    {
        return acosF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Inverse cosine of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return acos(da)
     */
    public static DataArray acosF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.ACOS.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Inverse cosine of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return acos(da)
     */
    public static DataArray acosD(DataArray da)
    {
        return acosD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Inverse cosine of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse cosine of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse cosine of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return acos(da)
     */
    public static DataArray acosD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.ACOS.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Inverse tangent of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return atan(da)
     */
    public static DataArray atanF(DataArray da)
    {
        return atanF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Inverse tangent of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return atan(da)
     */
    public static DataArray atanF(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_FLOAT;
        }
        return DataArrayOperator.ATAN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Inverse tangent of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return atan(da)
     */
    public static DataArray atanD(DataArray da)
    {
        return atanD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Inverse tangent of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>inverse tangent of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise inverse tangent of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return atan(da)
     */
    public static DataArray atanD(DataArray da, boolean ignoreUnits)
    {
        DataArrayType outType;
        if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
            outType = DataArrayType.FIELD_DATA_COMPLEX;
        } else {
            outType = DataArrayType.FIELD_DATA_DOUBLE;
        }
        return DataArrayOperator.ATAN.evaluate(outType, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Signum of DataArray in single precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>signum of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise signum of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return signum(da)
     */
    public static DataArray signumF(DataArray da)
    {
        return signumF(da, true);
    }

    /**
     * <table border="1">
     * <caption>Signum of DataArray in single precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>signum of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise signum of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return signum(da)
     */
    public static DataArray signumF(DataArray da, boolean ignoreUnits)
    {
        return DataArrayOperator.SIGNUM.evaluate(DataArrayType.FIELD_DATA_FLOAT, new DataArray[]{da}, ignoreUnits);
    }

    /**
     * <table border="1">
     * <caption>Signum of DataArray in double precision. The unit associated with the input data array is ignored.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>signum of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise signum of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da input DataArray
     *
     * @return signum(da)
     */
    public static DataArray signumD(DataArray da)
    {
        return signumD(da, true);
    }

    /**
     * <table border="1">
     * <caption>Signum of DataArray in double precision.</caption>
     * <tr>
     * <td><b>da</b></td><td><b>calculates</b></td><td><b>result</b></td>
     * </tr>
     * <tr>
     * <td>scalar</td><td>signum of scalar<td>scalar</td>
     * </tr>
     * <tr>
     * <td>vector</td><td>element-wise signum of vector</td><td>vector</td>
     * </tr>
     * </table>
     *
     * @param da          input DataArray
     * @param ignoreUnits if true, then the unit associated with the input data array is ignored
     *
     * @return signum(da)
     */
    public static DataArray signumD(DataArray da, boolean ignoreUnits)
    {
        return DataArrayOperator.SIGNUM.evaluate(DataArrayType.FIELD_DATA_DOUBLE, new DataArray[]{da}, ignoreUnits);
    }
}

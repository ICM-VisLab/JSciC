/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.commons.math3.util.FastMath;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jscic.dataarrays.DataObjectInterface;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.FloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.ObjectLargeArray;
import org.visnow.jlargearrays.LargeArrayUtils;
import static org.visnow.jlargearrays.LargeArrayUtils.create;
import org.visnow.jlargearrays.LogicLargeArray;

/**
 * Array slice utilities.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class SliceUtils
{

    /**
     * Returns a 1D slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D slice
     */
    public static LargeArray get1DSlice(final LargeArray data, final long start, final long n, final long step, final long veclen)
    {
        if (start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n - 1) * step * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n-1) * step * veclen >= data.length()");
        }

        final LargeArray out = LargeArrayUtils.create(data.getType(), n * veclen, false);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i, j, l;
                    long k = firstIdx * veclen;

                    for (i = firstIdx, l = start * veclen + firstIdx * step * veclen; i < lastIdx; i++, l += step * veclen) {
                        for (j = 0; j < veclen; j++) {
                            out.set(k, data.get(l + j));
                            k++;
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long i, j, l;
            long k = 0;
            for (i = 0, l = start * veclen; i < n; i++, l += step * veclen) {
                for (j = 0; j < veclen; j++) {
                    out.set(k, data.get(l + j));
                    k++;
                }
            }
        }
        return out;
    }

    /**
     * Returns a 1D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D float slice
     */
    public static FloatLargeArray get1DFloatSlice(final LargeArray data, final long start, final long n, final long step, final long veclen)
    {
        if (start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n - 1) * step * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n-1) * step * veclen >= data.length()");
        }
        switch (data.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case STRING:
                final FloatLargeArray out = new FloatLargeArray(n * veclen, false);
                Future<?>[] futures;
                int nthreads = ConcurrencyUtils.getNumberOfThreads();
                final long length = n;
                nthreads = (int) min(nthreads, length);
                futures = new Future[nthreads];
                long kk = length / nthreads;
                for (int jj = 0; jj < nthreads; jj++) {
                    final long firstIdx = jj * kk;
                    final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
                    futures[jj] = ConcurrencyUtils.submit(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            long i, j, l;
                            long k = firstIdx * veclen;

                            for (i = firstIdx, l = start * veclen + firstIdx * step * veclen; i < lastIdx; i++, l += step * veclen) {
                                for (j = 0; j < veclen; j++) {
                                    out.setFloat(k, data.getFloat(l + j));
                                    k++;
                                }
                            }
                        }
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(futures);
                } catch (InterruptedException | ExecutionException ex) {
                    long i, j, l;
                    long k = 0;
                    for (i = 0, l = start * veclen; i < n; i++, l += step * veclen) {
                        for (j = 0; j < veclen; j++) {
                            out.setFloat(k, data.getFloat(l + j));
                            k++;
                        }
                    }
                }
                return out;
            case COMPLEX_FLOAT:
                return get1DFloatSlice((ComplexFloatLargeArray) data, start, n, step, veclen);
            case COMPLEX_DOUBLE:
                return get1DFloatSlice((ComplexDoubleLargeArray) data, start, n, step, veclen);
            case OBJECT:
                return get1DFloatSlice((ObjectLargeArray) data, start, n, step, veclen);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Returns a 1D slice of norms of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D slice of norms
     */
    public static FloatLargeArray get1DNormSlice(final LargeArray data, final long start, final long n, final long step, final long veclen)
    {
        if (start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n - 1) * step * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n-1) * step * veclen >= data.length()");
        }
        return LargeArrayMath.vectorNorms(get1DFloatSlice(data, start, n, step, veclen), (int) veclen);
    }

    /**
     * Returns a 2D slice of an array.
     *
     * @param data   input array
     * @param dims   dimensions of the input array
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D slice
     */
    public static LargeArray get2DSlice(LargeArray data, long[] dims, int axis, long slice, long veclen)
    {
        if (dims == null || dims.length < 2 || dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis] || veclen <= 0) {
            throw new IllegalArgumentException("dims == null || dims.length < 2 ||  dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis] || veclen <= 0");
        }
        if (dims.length == 2) {
            if (axis == 1) {
                return get2DSlice(data, slice * dims[0], dims[0], 1, 1, 1, veclen);
            } else {
                return get2DSlice(data, slice, 1, 1, dims[1], dims[0], veclen);
            }
        } else {
            switch (axis) {
                case 0:
                    return get2DSlice(data, slice, dims[1], dims[0], dims[2], dims[0] * dims[1], veclen);
                case 1:
                    return get2DSlice(data, slice * dims[0], dims[0], 1, dims[2], dims[0] * dims[1], veclen);
                case 2:
                    return get2DSlice(data, slice * dims[0] * dims[1], dims[0], 1, dims[1], dims[0], veclen);
                default:
                    throw new IllegalArgumentException("dims == null || dims.length < 2 ||  dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis]");
            }
        }
    }

    /**
     * Returns a 2D slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n0     number of rows in the output slice
     * @param step0  distance between consecutive rows in the input array
     * @param n1     number of columns in the output slice
     * @param step1  distance between consecutive columns in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D slice
     */
    public static LargeArray get2DSlice(final LargeArray data, final long start, final long n0, final long step0, final long n1, final long step1, final long veclen)
    {
        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 < 0 || veclen <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
//        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || veclen <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 < 0 || start * veclen + (n0-1) * step0 * veclen + (n1-1) * step1 * veclen >= data.length()");
        }

        final LargeArray out = LargeArrayUtils.create(data.getType(), n0 * n1 * veclen, false);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n1;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i1, i0, j, l0, l1;
                    long k = firstIdx * n0 * veclen;

                    for (i1 = firstIdx, l1 = start * veclen + firstIdx * step1 * veclen; i1 < lastIdx; i1++, l1 += step1 * veclen) {
                        for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                            for (j = 0; j < veclen; j++) {
                                out.set(k, data.get(l0 + j));
                                k++;
                            }
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long k;
            long i1, l1;
            for (i1 = 0, k = 0, l1 = start * veclen; i1 < n1; i1++, l1 += step1 * veclen) {
                for (long i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                    for (long j = 0; j < veclen; j++, k++) {
                        out.set(k, data.get(l0 + j));
                    }
                }
            }
        }
        return out;

    }

    /**
     * Returns a 2D float slice of an array.
     *
     * @param data   input array
     * @param dims   dimensions of the input array
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D float slice
     */
    public static FloatLargeArray get2DFloatSlice(LargeArray data, long[] dims, int axis, long slice, long veclen)
    {
        if (dims == null || dims.length < 2 || dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis] || veclen <= 0) {
            throw new IllegalArgumentException("dims == null || dims.length < 2 ||  dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis] || veclen <= 0");
        }
        if (dims.length == 2) {
            if (axis == 1) {
                return get2DFloatSlice(data, (long) slice * (long) dims[0], dims[0], 1, 1, 1, veclen);
            } else {
                return get2DFloatSlice(data, slice, 1, 1, dims[1], dims[0], veclen);
            }
        } else {
            switch (axis) {
                case 0:
                    return get2DFloatSlice(data, slice, dims[1], dims[0], dims[2], (long) dims[0] * (long) dims[1], veclen);
                case 1:
                    return get2DFloatSlice(data, (long) slice * (long) dims[0], dims[0], 1, dims[2], (long) dims[0] * (long) dims[1], veclen);
                case 2:
                    return get2DFloatSlice(data, (long) slice * (long) dims[0] * (long) dims[1], dims[0], 1, dims[1], dims[0], veclen);
                default:
                    throw new IllegalArgumentException("dims == null || dims.length < 2 ||  dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis]");
            }
        }
    }

    /**
     * Returns a 2D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n0     number of rows in the output slice
     * @param step0  distance between consecutive rows in the input array
     * @param n1     number of columns in the output slice
     * @param step1  distance between consecutive columns in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D float slice
     */
    public static FloatLargeArray get2DFloatSlice(final LargeArray data, final long start, final long n0, final long step0, final long n1, final long step1, final long veclen)
    {
        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0-1) * step0 * veclen + (n1-1) * step1 * veclen >= data.length()");
        }

        switch (data.getType()) {
            case LOGIC:
            case BYTE:
            case UNSIGNED_BYTE:
            case SHORT:
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case STRING:
                final FloatLargeArray out = new FloatLargeArray(n0 * n1 * veclen, false);
                Future<?>[] futures;
                int nthreads = ConcurrencyUtils.getNumberOfThreads();
                final long length = n1;
                nthreads = (int) min(nthreads, length);
                futures = new Future[nthreads];
                long kk = length / nthreads;
                for (int jj = 0; jj < nthreads; jj++) {
                    final long firstIdx = jj * kk;
                    final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
                    futures[jj] = ConcurrencyUtils.submit(new Runnable()
                    {

                        @Override
                        public void run()
                        {
                            long i1, i0, j, l0, l1;
                            long k = firstIdx * n0 * veclen;

                            for (i1 = firstIdx, l1 = start * veclen + firstIdx * step1 * veclen; i1 < lastIdx; i1++, l1 += step1 * veclen) {
                                for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                                    for (j = 0; j < veclen; j++) {
                                        out.setFloat(k, data.getFloat(l0 + j));
                                        k++;
                                    }
                                }
                            }
                        }
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(futures);
                } catch (InterruptedException | ExecutionException ex) {
                    long i0, l0, i1, l1, j;
                    long k;
                    for (i1 = 0, k = 0, l1 = start * veclen; i1 < n1; i1++, l1 += step1 * veclen) {
                        for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                            for (j = 0; j < veclen; j++, k++) {
                                out.setFloat(k, data.getFloat(l0 + j));
                            }
                        }
                    }
                }
                return out;
            case COMPLEX_FLOAT:
                return get2DFloatSlice((ComplexFloatLargeArray) data, start, n0, step0, n1, step1, veclen);
            case COMPLEX_DOUBLE:
                return get2DFloatSlice((ComplexDoubleLargeArray) data, start, n0, step0, n1, step1, veclen);
            case OBJECT:
                return get2DFloatSlice((ObjectLargeArray) data, start, n0, step0, n1, step1, veclen);
            default:
                throw new IllegalArgumentException("Unsupported array type");
        }
    }

    /**
     * Returns a 2D slice of norms of an array.
     *
     * @param data   input array
     * @param dims   dimensions of the input array
     * @param axis   axis for slicing input array, axis cannot be smaller than 0
     *               or larger than dims.length-1
     * @param slice  slice index in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D slice of norms
     */
    public static FloatLargeArray get2DNormSlice(LargeArray data, long[] dims, int axis, long slice, long veclen)
    {

        if (dims == null || dims.length < 2 || dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis] || veclen <= 0) {
            throw new IllegalArgumentException("dims == null || dims.length < 2 ||  dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis] || veclen <= 0");
        }
        if (dims.length == 2) {
            if (axis == 1) {
                return get2DNormSlice(data, (long) slice * (long) dims[0], dims[0], 1, 1, 1, veclen);
            } else {
                return get2DNormSlice(data, slice, 1, 1, dims[1], dims[0], veclen);
            }
        } else {
            switch (axis) {
                case 0:
                    return get2DNormSlice(data, slice, dims[1], dims[0], dims[2], (long) dims[0] * (long) dims[1], veclen);
                case 1:
                    return get2DNormSlice(data, (long) slice * (long) dims[0], dims[0], 1, dims[2], (long) dims[0] * (long) dims[1], veclen);
                case 2:
                    return get2DNormSlice(data, (long) slice * (long) dims[0] * (long) dims[1], dims[0], 1, dims[1], dims[0], veclen);
                default:
                    throw new IllegalArgumentException("dims == null || dims.length < 2 ||  dims.length > 3 || axis < 0 || axis >= dims.length || slice < 0 || slice >= dims[axis]");
            }
        }
    }

    /**
     * Returns a 2D slice of norms an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n0     number of rows in the output slice
     * @param step0  distance between consecutive rows in the input array
     * @param n1     number of columns in the output slice
     * @param step1  distance between consecutive columns in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D slice of norms
     */
    public static FloatLargeArray get2DNormSlice(final LargeArray data, final long start, final long n0, final long step0, final long n1, final long step1, final long veclen)
    {
        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0-1) * step0 * veclen + (n1-1) * step1 * veclen >= data.length()");
        }

        return LargeArrayMath.vectorNorms(get2DFloatSlice(data, start, n0, step0, n1, step1, veclen), (int) veclen);
    }

    /**
     * Returns a 1D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D float slice
     */
    private static FloatLargeArray get1DFloatSlice(final ComplexFloatLargeArray data, final long start, final long n, final long step, final long veclen)
    {
        if (start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n - 1) * step * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n-1) * step * veclen >= data.length()");
        }

        final FloatLargeArray out = new FloatLargeArray(n * veclen, false);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i, j, l;
                    long k = firstIdx * veclen;

                    for (i = firstIdx, l = start * veclen + firstIdx * step * veclen; i < lastIdx; i++, l += step * veclen) {
                        for (j = 0; j < veclen; j++) {
                            out.setFloat(k, (float) sqrt(data.getRealArray().getFloat(l + j) * data.getRealArray().getFloat(l + j) + data.getImaginaryArray().getFloat(l + j) * data.getImaginaryArray().getFloat(l + j)));
                            k++;
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long i, j, l;
            long k;
            for (i = k = 0, l = start * veclen; i < n; i++, l += step * veclen) {
                for (j = 0; j < veclen; j++, k++) {
                    out.setFloat(k, (float) sqrt(data.getRealArray().getFloat(l + j) * data.getRealArray().getFloat(l + j) + data.getImaginaryArray().getFloat(l + j) * data.getImaginaryArray().getFloat(l + j)));
                }
            }
        }
        return out;
    }

    /**
     * Returns a 1D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n      number of elements in the output slice
     * @param step   distance between consecutive elements in the input array
     * @param veclen vector length of the input array
     *
     * @return 1D float slice
     */
    private static FloatLargeArray get1DFloatSlice(final ObjectLargeArray data, final long start, final long n, final long step, final long veclen)
    {
        if (start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n - 1) * step * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n <= 0 || step <= 0 || veclen <= 0 || start * veclen + (n-1) * step * veclen >= data.length()");
        }

        final FloatLargeArray out = new FloatLargeArray(n * veclen, false);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i, j, l;
                    long k = firstIdx * veclen;

                    for (i = firstIdx, l = start * veclen + firstIdx * step * veclen; i < lastIdx; i++, l += step * veclen) {
                        for (j = 0; j < veclen; j++) {
                            out.setFloat(k, ((DataObjectInterface) data.get(l + j)).toFloat());
                            k++;
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long i, j, l;
            long k;
            for (i = 0, k = 0, l = start * veclen; i < n; i++, l += step * veclen) {
                for (j = 0; j < veclen; j++, k++) {
                    out.setFloat(k, ((DataObjectInterface) data.get(l + j)).toFloat());
                }
            }
        }
        return out;
    }

    /**
     * Returns a 2D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n0     number of rows in the output slice
     * @param step0  distance between consecutive rows in the input array
     * @param n1     number of columns in the output slice
     * @param step1  distance between consecutive columns in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D float slice
     */
    private static FloatLargeArray get2DFloatSlice(final ComplexFloatLargeArray data, final long start, final long n0, final long step0, final long n1, final long step1, final long veclen)
    {

        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0-1) * step0 * veclen + (n1-1) * step1 * veclen >= data.length()");
        }

        final FloatLargeArray out = new FloatLargeArray(n0 * n1 * veclen, false);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n1;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i1, i0, j, l0, l1;
                    long k = firstIdx * n0 * veclen;

                    for (i1 = firstIdx, l1 = start * veclen + firstIdx * step1 * veclen; i1 < lastIdx; i1++, l1 += step1 * veclen) {
                        for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                            for (j = 0; j < veclen; j++) {
                                float[] v = data.getComplexFloat(l0 + j);
                                out.setFloat(k, (float) sqrt(v[0] * v[0] + v[1] * v[1]));
                                k++;
                            }
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long i0, l0, i1, l1, j;
            long k;
            for (i1 = 0, k = 0, l1 = start * veclen; i1 < n1; i1++, l1 += step1 * veclen) {
                for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                    for (j = 0; j < veclen; j++, k++) {
                        float[] v = data.getComplexFloat(l0 + j);
                        out.setFloat(k, (float) sqrt(v[0] * v[0] + v[1] * v[1]));
                    }
                }
            }
        }
        return out;
    }

    /**
     * Returns a 2D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n0     number of rows in the output slice
     * @param step0  distance between consecutive rows in the input array
     * @param n1     number of columns in the output slice
     * @param step1  distance between consecutive columns in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D float slice
     */
    private static FloatLargeArray get2DFloatSlice(final ComplexDoubleLargeArray data, final long start, final long n0, final long step0, final long n1, final long step1, final long veclen)
    {

        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0-1) * step0 * veclen + (n1-1) * step1 * veclen >= data.length()");
        }

        final FloatLargeArray out = new FloatLargeArray(n0 * n1 * veclen, false);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n1;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i1, i0, j, l0, l1;
                    long k = firstIdx * n0 * veclen;

                    for (i1 = firstIdx, l1 = start * veclen + firstIdx * step1 * veclen; i1 < lastIdx; i1++, l1 += step1 * veclen) {
                        for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                            for (j = 0; j < veclen; j++) {
                                float[] v = data.getComplexFloat(l0 + j);
                                out.setFloat(k, (float) sqrt(v[0] * v[0] + v[1] * v[1]));
                                k++;
                            }
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long i0, l0, i1, l1, j;
            long k;
            for (i1 = 0, k = 0, l1 = start * veclen; i1 < n1; i1++, l1 += step1 * veclen) {
                for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                    for (j = 0; j < veclen; j++, k++) {
                        float[] v = data.getComplexFloat(l0 + j);
                        out.setFloat(k, (float) sqrt(v[0] * v[0] + v[1] * v[1]));
                    }
                }
            }
        }
        return out;
    }

    /**
     * Returns a 2D float slice of an array.
     *
     * @param data   input array
     * @param start  number of elements to skip from the beginning of the input
     *               array
     * @param n0     number of rows in the output slice
     * @param step0  distance between consecutive rows in the input array
     * @param n1     number of columns in the output slice
     * @param step1  distance between consecutive columns in the input array
     * @param veclen vector length of the input array
     *
     * @return 2D float slice
     */
    private static FloatLargeArray get2DFloatSlice(final ObjectLargeArray data, final long start, final long n0, final long step0, final long n1, final long step1, final long veclen)
    {

        if (start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0 - 1) * step0 * veclen + (n1 - 1) * step1 * veclen >= data.length()) {
            throw new IllegalArgumentException("start < 0 || n0 <= 0 || step0 <= 0 || n1 <= 0 || step1 <= 0 || start * veclen + (n0-1) * step0 * veclen + (n1-1) * step1 * veclen >= data.length()");
        }

        final FloatLargeArray out = new FloatLargeArray(n0 * n1 * veclen);
        Future<?>[] futures;
        int nthreads = ConcurrencyUtils.getNumberOfThreads();
        final long length = n1;
        nthreads = (int) min(nthreads, length);
        futures = new Future[nthreads];
        long kk = length / nthreads;
        for (int jj = 0; jj < nthreads; jj++) {
            final long firstIdx = jj * kk;
            final long lastIdx = (jj == nthreads - 1) ? length : firstIdx + kk;
            futures[jj] = ConcurrencyUtils.submit(new Runnable()
            {

                @Override
                public void run()
                {
                    long i1, i0, j, l0, l1;
                    long k = firstIdx * n0 * veclen;

                    for (i1 = firstIdx, l1 = start * veclen + firstIdx * step1 * veclen; i1 < lastIdx; i1++, l1 += step1 * veclen) {
                        for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                            for (j = 0; j < veclen; j++) {
                                DataObjectInterface obj = (DataObjectInterface) data.get(l0 + j);
                                out.setFloat(k, obj.toFloat());
                                k++;
                            }
                        }
                    }
                }
            });
        }
        try {
            ConcurrencyUtils.waitForCompletion(futures);
        } catch (InterruptedException | ExecutionException ex) {
            long i0, l0, i1, l1, j;
            long k;
            for (i1 = 0, k = 0, l1 = start * veclen; i1 < n1; i1++, l1 += step1 * veclen) {
                for (i0 = 0, l0 = l1; i0 < n0; i0++, l0 += step0 * veclen) {
                    for (j = 0; j < veclen; j++, k++) {
                        DataObjectInterface obj = (DataObjectInterface) data.get(l0 + j);
                        out.setFloat(k, obj.toFloat());
                    }
                }
            }
        }
        return out;

    }

    /**
     * Returns all elements of the specified source array for which the corresponding mask element is equal to 1.
     *
     * @param src    the source array
     * @param veclen vector length of the source array
     * @param mask   the mask array
     * 
     * @return selection of elements from the source array
     */
    public static LargeArray select(final LargeArray src, int veclen, final LogicLargeArray mask)
    {

        if (src.length() != mask.length() * veclen) {
            throw new IllegalArgumentException("src.length() != mask.length() * veclen");
        }
        long length = mask.length();
        long count = 0;
        int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
        long k = length / nthreads;
        Future[] futures = new Future[nthreads];
        for (int j = 0; j < nthreads; j++) {
            final long firstIdx = j * k;
            final long lastIdx = (j == nthreads - 1) ? length : firstIdx + k;
            futures[j] = ConcurrencyUtils.submit(new Callable()
            {
                @Override
                public Long call()
                {
                    long count = 0;
                    for (long k = firstIdx; k < lastIdx; k++) {
                        if (mask.getByte(k) == 1) count++;
                    }
                    return count;
                }
            });
        }
        try {
            for (int j = 0; j < nthreads; j++) {
                count += (Long) (futures[j].get());
            }
        } catch (InterruptedException | ExecutionException ex) {
            for (long j = 0; j < length; j++) {
                if (mask.getByte(j) == 1) count++;
            }
        }

        if (count <= 0) return null;

        LargeArray res = create(src.getType(), count * veclen, false);
        for (long i = 0, l = 0; i < length; i++) {
            if (mask.getByte(i) == 1) {
                LargeArrayUtils.arraycopy(src, i * veclen, res, l * veclen, veclen);
                l += 1;
            }
        }
        return res;
    }
}

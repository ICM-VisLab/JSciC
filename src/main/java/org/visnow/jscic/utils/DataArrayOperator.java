/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.utils;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import org.apache.commons.math3.util.FastMath;
import static org.apache.commons.math3.util.FastMath.*;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.ConcurrencyUtils;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayArithmetics;
import org.visnow.jlargearrays.LargeArrayOperator;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;
import ucar.units.MultiplyException;
import ucar.units.OperationException;
import ucar.units.Unit;

/**
 *
 * Arithmetical operations on DataArrays.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public enum DataArrayOperator
{
    ABS()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(final DataArrayType outputType, final DataArray[] args, final boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, false, true);
            DataArray[] input = new DataArray[1];
            String outUnit = processUnits(args, input, ignoreUnits);
            TimeData outTd;
            int veclen = input[0].getVectorLength();
            if (veclen == 1) {
                double[] physMappingCoeffs = input[0].getPhysicalMappingCoefficients();
                TimeData td = input[0].getTimeData();
                ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                for (Float time : timeSeries) {
                    LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                    dataSeries.add(LargeArrayArithmetics.abs(a, outType.toLargeArrayType()));
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else {
                Function<byte[], Byte> logicByteFun = (x) -> {
                    double out = 0;
                    for (int i = 0; i < x.length; i++) {
                        out += (double) x[i] * (double) x[i];
                    }
                    return (byte) sqrt(out);
                };
                Function<short[], Short> unsignedByteShortFun = (x) -> {
                    double out = 0;
                    for (int i = 0; i < x.length; i++) {
                        out += (double) x[i] * (double) x[i];
                    }
                    return (short) sqrt(out);
                };
                Function<int[], Integer> intFun = (x) -> {
                    double out = 0;
                    for (int i = 0; i < x.length; i++) {
                        out += (double) x[i] * (double) x[i];
                    }
                    return (int) sqrt(out);
                };
                Function<long[], Long> longFun = (x) -> {
                    double out = 0;
                    for (int i = 0; i < x.length; i++) {
                        out += (double) x[i] * (double) x[i];
                    }
                    return (long) sqrt(out);
                };
                Function<float[], Float> floatFun = (x) -> {
                    return VectorMath.vectorNorm(x);
                };
                Function<double[], Double> doubleFun = (x) -> {
                    return VectorMath.vectorNorm(x);
                };
                Function<float[][], float[]> complexFloatFun = (x) -> {
                    double norm = 0;
                    for (int v = 0; v < x.length; v++) {
                        float[] elem_a = x[v];
                        norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                    }
                    return new float[]{(float) FastMath.sqrt(norm), 0};
                };
                Function<float[][], Float> complexFloatRealFun = (x) -> {
                    double norm = 0;
                    for (int v = 0; v < x.length; v++) {
                        float[] elem_a = x[v];
                        norm += elem_a[0] * elem_a[0] + elem_a[1] * elem_a[1];
                    }
                    return (float) FastMath.sqrt(norm);
                };
                outTd = evaluateSingleArgFunction(input[0], outType, logicByteFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, complexFloatRealFun);
            }
            return DataArray.create(outTd, 1, "ABS", outUnit, input[0].getUserData());
        }
        //</editor-fold>
    },
    ACOS()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.acos(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "ACOS", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    ADD()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, -1, 2, false, false);
            int nargs = args.length;
            DataArray[] input = new DataArray[nargs];
            String outUnit = processUnits(args, input, ignoreUnits);
            TimeData outTd;
            boolean equalveclen = true;
            int vl = input[0].getVectorLength();
            int maxvl = vl;
            for (int i = 1; i < nargs; i++) {
                int tmp = input[i].getVectorLength();
                if (vl != tmp) {
                    equalveclen = false;
                }
                if (maxvl < tmp) {
                    maxvl = tmp;
                }
                if (!equalveclen && maxvl > 1 && tmp > 1 && maxvl != tmp) {
                    throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length.");
                }
            }
            if (equalveclen) {
                TimeData[] td = new TimeData[nargs];
                LargeArray[] la = new LargeArray[nargs];
                double[][] physMappingCoeffs = new double[nargs][];
                ArrayList<Float> timeSeriesTmp = new ArrayList<>();
                for (int i = 0; i < nargs; i++) {
                    td[i] = input[i].getTimeData();
                    timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
                    physMappingCoeffs[i] = input[i].getPhysicalMappingCoefficients();
                }
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                for (Float time : timeSeries) {
                    for (int i = 0; i < nargs; i++) {
                        la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
                    }
                    LargeArray res = LargeArrayOperator.ADD.evaluate(outType.toLargeArrayType(), la);
                    dataSeries.add(res);
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else {
                final int maxvlf = maxvl;
                Function<byte[][], byte[]> logicFun = (in) -> {
                    int ndata = in.length;
                    byte[] out = new byte[maxvlf];
                    for (int i = 0; i < ndata; i++) {
                        byte[] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[0];
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[v];
                            }
                        }
                    }
                    return out;
                };
                Function<short[][], short[]> unsignedByteShortFun = (in) -> {
                    int ndata = in.length;
                    short[] out = new short[maxvlf];
                    for (int i = 0; i < ndata; i++) {
                        short[] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[0];
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[v];
                            }
                        }
                    }
                    return out;
                };
                Function<int[][], int[]> intFun = (in) -> {
                    int ndata = in.length;
                    int[] out = new int[maxvlf];
                    for (int i = 0; i < ndata; i++) {
                        int[] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[0];
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[v];
                            }
                        }
                    }
                    return out;
                };
                Function<long[][], long[]> longFun = (in) -> {
                    int ndata = in.length;
                    long[] out = new long[maxvlf];
                    for (int i = 0; i < ndata; i++) {
                        long[] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[0];
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[v];
                            }
                        }
                    }
                    return out;
                };
                Function<float[][], float[]> floatFun = (in) -> {
                    int ndata = in.length;
                    float[] out = new float[maxvlf];
                    for (int i = 0; i < ndata; i++) {
                        float[] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[0];
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[v];
                            }
                        }
                    }
                    return out;
                };
                Function<double[][], double[]> doubleFun = (in) -> {
                    int ndata = in.length;
                    double[] out = new double[maxvlf];
                    for (int i = 0; i < ndata; i++) {
                        double[] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[0];
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] += elem[v];
                            }
                        }
                    }
                    return out;
                };
                Function<float[][][], float[][]> complexFloatFun = (in) -> {
                    int ndata = in.length;
                    float[][] out = new float[maxvlf][2];
                    for (int i = 0; i < ndata; i++) {
                        float[][] elem = in[i];
                        if (elem.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = LargeArrayArithmetics.complexAdd(out[v], elem[0]);
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = LargeArrayArithmetics.complexAdd(out[v], elem[v]);
                            }
                        }
                    }
                    return out;
                };
                outTd = evaluateVarArgFunction(input, outType, maxvl, logicFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, null);
            }
            return DataArray.create(outTd, maxvl, "ADD", outUnit, null);
        }
        //</editor-fold>
    },
    ASIN()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.asin(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "ASIN", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    ATAN()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.atan(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "ATAN", in.getUnit(), in.getUserData());
        }
    },
    AXPY()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 3, -1, false, false);
            DataArray a = args[0];
            DataArray x = args[1];
            DataArray b = args[2];
            if (a == null || !a.isNumeric() || !a.isConstant() || b == null || !b.isNumeric() || !b.isConstant() || x == null || !x.isNumeric() || a.getNElements() != x.getNElements() || b.getNElements() != x.getNElements()) {
                throw new IllegalArgumentException("Invalid arguments.");
            }
            String outUnit = "1";
            if (!(ignoreUnits || (a.isUnitless() && x.isUnitless() && b.isUnitless()))) {
                Unit ua = null, uap = null, ux = null, uxp = null, ub = null, ubp = null, uaxpy = null;
                if (UnitUtils.isValidUnit(a.getUnit())) {
                    ua = UnitUtils.getUnit(a.getUnit());
                    uap = ua.getDerivedUnit();
                    a = UnitUtils.deepUnitConvertD(a, uap.toString());
                }
                if (UnitUtils.isValidUnit(b.getUnit())) {
                    ub = UnitUtils.getUnit(b.getUnit());
                    ubp = ub.getDerivedUnit();
                    b = UnitUtils.deepUnitConvertD(b, ubp.toString());
                }
                if (UnitUtils.isValidUnit(x.getUnit())) {
                    ux = UnitUtils.getUnit(x.getUnit());
                    uxp = ux.getDerivedUnit();
                    x = UnitUtils.deepUnitConvertD(x, uxp.toString());
                }
                if (uap != null && uxp != null) {
                    try {
                        uaxpy = uap.multiplyBy(uxp);
                    } catch (MultiplyException ex) {
                        throw new IllegalArgumentException("Incompatible or invalid units: " + a.getUnit() + " and " + x.getUnit());
                    }
                } else if (uap == null && uxp != null) {
                    uaxpy = uxp;
                } else if (uap != null && uxp == null) {
                    uaxpy = uap;
                } else {
                    uaxpy = null;
                }
                if (uaxpy != null && ubp != null && !uaxpy.isCompatible(ubp)) {
                    throw new IllegalArgumentException("Incompatible or invalid units: " + uaxpy.getDerivedUnit().toString() + " and " + b.getUnit());
                } else if ((uaxpy != null && ubp == null) || (uaxpy == null && ubp != null)) {
                    throw new IllegalArgumentException("Incompatible or invalid units: " + a.getUnit() + " and " + x.getUnit() + " and " + b.getUnit());
                } else if (uaxpy != null && ubp != null) {
                    outUnit = uaxpy.getDerivedUnit().toString();
                }
                if (!b.isUnitless()) {
                    b = UnitUtils.deepUnitConvertD(b, outUnit);
                }
            }
            DataArray[] input = new DataArray[]{a, x, b};
            TimeData outTd;
            int nargs = input.length;
            int vl = x.getVectorLength();
            if (vl == 1) {
                TimeData[] td = new TimeData[nargs];
                LargeArray[] la = new LargeArray[nargs];
                double[][] physMappingCoeffs = new double[nargs][];
                ArrayList<Float> timeSeriesTmp = new ArrayList<>();
                for (int i = 0; i < nargs; i++) {
                    td[i] = input[i].getTimeData();
                    timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
                    physMappingCoeffs[i] = input[i].getPhysicalMappingCoefficients();
                }
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                for (Float time : timeSeries) {
                    for (int i = 0; i < nargs; i++) {
                        la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
                    }
                    LargeArray res = LargeArrayOperator.AXPY.evaluate(outType.toLargeArrayType(), la);
                    dataSeries.add(res);
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else {
                Function<byte[][], byte[]> logicFun = (in) -> {
                    int veclen = in[1].length;
                    byte[] out = new byte[veclen];
                    for (int v = 0; v < veclen; v++) {
                        out[v] += in[0][0] * in[1][v] + in[2][0];
                    }
                    return out;
                };
                Function<short[][], short[]> unsignedByteShortFun = (in) -> {
                    int veclen = in[1].length;
                    short[] out = new short[veclen];
                    for (int v = 0; v < veclen; v++) {
                        out[v] += in[0][0] * in[1][v] + in[2][0];
                    }
                    return out;
                };
                Function<int[][], int[]> intFun = (in) -> {
                    int veclen = in[1].length;
                    int[] out = new int[veclen];
                    for (int v = 0; v < veclen; v++) {
                        out[v] += in[0][0] * in[1][v] + in[2][0];
                    }
                    return out;
                };
                Function<long[][], long[]> longFun = (in) -> {
                    int veclen = in[1].length;
                    long[] out = new long[veclen];
                    for (int v = 0; v < veclen; v++) {
                        out[v] += in[0][0] * in[1][v] + in[2][0];
                    }
                    return out;
                };
                Function<float[][], float[]> floatFun = (in) -> {
                    int veclen = in[1].length;
                    float[] out = new float[veclen];
                    for (int v = 0; v < veclen; v++) {
                        out[v] += in[0][0] * in[1][v] + in[2][0];
                    }
                    return out;
                };
                Function<double[][], double[]> doubleFun = (in) -> {
                    int veclen = in[1].length;
                    double[] out = new double[veclen];
                    for (int v = 0; v < veclen; v++) {
                        out[v] += in[0][0] * in[1][v] + in[2][0];
                    }
                    return out;
                };
                Function<float[][][], float[][]> complexFloatFun = (in) -> {
                    int veclen = in[1].length;
                    float[][] out = new float[veclen][];
                    float[] elem_a = in[0][0];
                    float[] elem_b = in[2][0];
                    for (int v = 0; v < veclen; v++) {
                        out[v] = LargeArrayArithmetics.complexAdd(LargeArrayArithmetics.complexMult(elem_a, in[1][v]), elem_b);
                    }
                    return out;
                };
                outTd = evaluateVarArgFunction(input, outType, vl, logicFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, null);
            }
            return DataArray.create(outTd, vl, "AXPY", outUnit, null);
        }
        //</editor-fold>
    },
    COS()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.cos(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "COS", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    DIFF()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 2, -1, false, false);
            DataArray[] input = new DataArray[2];
            String outUnit = processUnits(args, input, ignoreUnits);
            DataArray in1 = input[0];
            DataArray in2 = input[1];
            TimeData outTd;
            int vl1 = in1.getVectorLength();
            int vl2 = in2.getVectorLength();
            final int maxvlf = FastMath.max(vl1, vl2);
            if (vl1 == vl2) {
                TimeData td1 = in1.getTimeData();
                TimeData td2 = in2.getTimeData();
                double[] physMappingCoeffs1 = in1.getPhysicalMappingCoefficients();
                double[] physMappingCoeffs2 = in2.getPhysicalMappingCoefficients();
                ArrayList<Float> timeSeriesTmp = new ArrayList<>();
                timeSeriesTmp.addAll((ArrayList<Float>) td1.getTimesAsList().clone());
                timeSeriesTmp.addAll((ArrayList<Float>) td2.getTimesAsList().clone());
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                LargeArray[] la = new LargeArray[2];
                for (Float time : timeSeries) {
                    la[0] = td1.getPhysicalValue(time, physMappingCoeffs1);
                    la[1] = td2.getPhysicalValue(time, physMappingCoeffs2);
                    LargeArray res = LargeArrayOperator.DIFF.evaluate(outType.toLargeArrayType(), la);
                    dataSeries.add(res);
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else if ((vl1 == 1 && vl2 > 1) || (vl1 > 1 && vl2 == 1)) {
                Function<byte[][], byte[]> logicFun = (in) -> {
                    byte[] out = new byte[maxvlf];
                    byte[] elem1 = in[0];
                    byte[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (byte) (elem1[0] - elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (byte) (elem1[v] - elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (byte) (elem1[v] - elem2[v]);
                        }
                    }
                    return out;
                };
                Function<short[][], short[]> unsignedByteShortFun = (in) -> {
                    short[] out = new short[maxvlf];
                    short[] elem1 = in[0];
                    short[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (short) (elem1[0] - elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (short) (elem1[v] - elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (short) (elem1[v] - elem2[v]);
                        }
                    }
                    return out;
                };
                Function<int[][], int[]> intFun = (in) -> {
                    int[] out = new int[maxvlf];
                    int[] elem1 = in[0];
                    int[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] - elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[v]);
                        }
                    }
                    return out;
                };
                Function<long[][], long[]> longFun = (in) -> {
                    long[] out = new long[maxvlf];
                    long[] elem1 = in[0];
                    long[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] - elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[v]);
                        }
                    }
                    return out;
                };
                Function<float[][], float[]> floatFun = (in) -> {
                    float[] out = new float[maxvlf];
                    float[] elem1 = in[0];
                    float[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] - elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[v]);
                        }
                    }
                    return out;
                };
                Function<double[][], double[]> doubleFun = (in) -> {
                    double[] out = new double[maxvlf];
                    double[] elem1 = in[0];
                    double[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] - elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] - elem2[v]);
                        }
                    }
                    return out;
                };
                Function<float[][][], float[][]> complexFloatFun = (in) -> {
                    float[][] out = new float[maxvlf][];
                    float[][] elem1 = in[0];
                    float[][] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = LargeArrayArithmetics.complexDiff(elem1[0], elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = LargeArrayArithmetics.complexDiff(elem1[v], elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = LargeArrayArithmetics.complexDiff(elem1[v], elem2[v]);
                        }
                    }
                    return out;
                };
                outTd = evaluateVarArgFunction(input, outType, maxvlf, logicFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, null);
            } else {
                throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length.");
            }
            return DataArray.create(outTd, maxvlf, "DIFF", outUnit, null);
        }
        //</editor-fold>
    },
    DIV()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 2, -1, false, false);
            DataArray in1 = args[0];
            DataArray in2 = args[1];
            String outUnit = "1";
            if (!(ignoreUnits || (in1.isUnitless() && in2.isUnitless()))) {
                if (!in1.isUnitless() && !in2.isUnitless()) {
                    Unit unit1 = UnitUtils.getUnit(in1.getUnit());
                    Unit unit2 = UnitUtils.getUnit(in2.getUnit());
                    try {
                        outUnit = unit1.divideBy(unit2).toString();
                        if (outUnit.isEmpty()) {
                            outUnit = "1";
                        } else {
                            outUnit = UnitUtils.getDerivedUnit(outUnit);
                        }
                    } catch (OperationException ex) {
                        throw new IllegalArgumentException("Incompatible or invalid units: " + in1.getUnit() + " and " + in2.getUnit());
                    }
                    in1 = UnitUtils.deepUnitConvertD(in1, unit1.getDerivedUnit().toString());
                    in2 = UnitUtils.deepUnitConvertD(in2, unit2.getDerivedUnit().toString());
                } else if ((!in1.isUnitless() && !UnitUtils.isValidUnit(in1.getUnit())) || (!in2.isUnitless() && !UnitUtils.isValidUnit(in2.getUnit()))) {
                    throw new IllegalArgumentException("Incompatible or invalid units: " + in1.getUnit() + " and " + in2.getUnit());
                } else if (in1.isUnitless() && UnitUtils.isValidUnit(in2.getUnit())) {
                    Unit unit2 = UnitUtils.getUnit(in2.getUnit());
                    outUnit = UnitUtils.getDerivedUnit("1/(" + in2.getUnit() + ")");
                    in2 = UnitUtils.deepUnitConvertD(in2, unit2.getDerivedUnit().toString());
                } else if (in2.isUnitless() && UnitUtils.isValidUnit(in1.getUnit())) {
                    outUnit = UnitUtils.getDerivedUnit(in1.getUnit());
                    in1 = UnitUtils.deepUnitConvertD(in1, outUnit);
                }
            }
            TimeData outTd;
            int vl1 = in1.getVectorLength();
            int vl2 = in2.getVectorLength();
            final int maxvlf = FastMath.max(vl1, vl2);
            if (vl1 == vl2) {
                TimeData td1 = in1.getTimeData();
                TimeData td2 = in2.getTimeData();
                double[] physMappingCoeffs1 = in1.getPhysicalMappingCoefficients();
                double[] physMappingCoeffs2 = in2.getPhysicalMappingCoefficients();
                ArrayList<Float> timeSeriesTmp = new ArrayList<>();
                timeSeriesTmp.addAll((ArrayList<Float>) td1.getTimesAsList().clone());
                timeSeriesTmp.addAll((ArrayList<Float>) td2.getTimesAsList().clone());
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                for (Float time : timeSeries) {
                    LargeArray la1 = td1.getPhysicalValue(time, physMappingCoeffs1);
                    LargeArray la2 = td2.getPhysicalValue(time, physMappingCoeffs2);
                    LargeArray res = LargeArrayArithmetics.div(la1, la2, outType.toLargeArrayType());
                    dataSeries.add(res);
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else if ((vl1 == 1 && vl2 > 1) || (vl1 > 1 && vl2 == 1)) {
                Function<byte[][], byte[]> logicFun = (in) -> {
                    byte[] out = new byte[maxvlf];
                    byte[] elem1 = in[0];
                    byte[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (byte) (elem1[0] / elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (byte) (elem1[v] / elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (byte) (elem1[v] / elem2[v]);
                        }
                    }
                    return out;
                };
                Function<short[][], short[]> unsignedByteShortFun = (in) -> {
                    short[] out = new short[maxvlf];
                    short[] elem1 = in[0];
                    short[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (short) (elem1[0] / elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (short) (elem1[v] / elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (short) (elem1[v] / elem2[v]);
                        }
                    }
                    return out;
                };
                Function<int[][], int[]> intFun = (in) -> {
                    int[] out = new int[maxvlf];
                    int[] elem1 = in[0];
                    int[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] / elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[v]);
                        }
                    }
                    return out;
                };
                Function<long[][], long[]> longFun = (in) -> {
                    long[] out = new long[maxvlf];
                    long[] elem1 = in[0];
                    long[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] / elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[v]);
                        }
                    }
                    return out;
                };
                Function<float[][], float[]> floatFun = (in) -> {
                    float[] out = new float[maxvlf];
                    float[] elem1 = in[0];
                    float[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] / elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[v]);
                        }
                    }
                    return out;
                };
                Function<double[][], double[]> doubleFun = (in) -> {
                    double[] out = new double[maxvlf];
                    double[] elem1 = in[0];
                    double[] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[0] / elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = (elem1[v] / elem2[v]);
                        }
                    }
                    return out;
                };
                Function<float[][][], float[][]> complexFloatFun = (in) -> {
                    float[][] out = new float[maxvlf][];
                    float[][] elem1 = in[0];
                    float[][] elem2 = in[1];
                    if (elem1.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = LargeArrayArithmetics.complexDiv(elem1[0], elem2[v]);
                        }
                    } else if (elem2.length == 1) {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = LargeArrayArithmetics.complexDiv(elem1[v], elem2[0]);
                        }
                    } else {
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = LargeArrayArithmetics.complexDiv(elem1[v], elem2[v]);
                        }
                    }
                    return out;
                };
                outTd = evaluateVarArgFunction(new DataArray[]{in1, in2}, outType, maxvlf, logicFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, null);
            } else {
                throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length.");
            }
            return DataArray.create(outTd, maxvlf, "DIV", outUnit, null);
        }
        //</editor-fold>
    },
    EXP()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.exp(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "EXP", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    LOG()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.log(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "LOG", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    LOG10()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.log10(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "LOG10", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    MULT()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args, boolean ignoreUnits)
        {
            DataArrayType outType = checkArguments(outputType, args, -1, 2, false, false);
            int nargs = args.length;
            DataArray[] input = new DataArray[nargs];
            for (int i = 0; i < nargs; i++) {
                input[i] = args[i];
            }

            Unit outUnit = null;
            if (!ignoreUnits) {
                boolean unitless = input[0].isUnitless();
                if (!unitless) {
                    outUnit = UnitUtils.getUnit(input[0].getUnit()).getDerivedUnit();
                    input[0] = UnitUtils.deepUnitConvertD(input[0], outUnit.toString());
                }

                for (int i = 1; i < nargs; i++) {
                    if (!(ignoreUnits || (unitless && input[i].isUnitless()))) {
                        if (!input[i].isUnitless()) {
                            if (outUnit == null) {
                                outUnit = UnitUtils.getUnit(input[i].getUnit()).getDerivedUnit();
                            } else {
                                try {
                                    outUnit = (outUnit.multiplyBy(UnitUtils.getUnit(input[i].getUnit()))).getDerivedUnit();
                                } catch (MultiplyException ex) {
                                    throw new IllegalArgumentException("Incompatible or invalid units: " + outUnit != null ? outUnit.toString() : "1" + " and " + UnitUtils.getUnit(input[i].getUnit()).toString());
                                }
                            }
                            input[i] = UnitUtils.deepUnitConvertD(input[i], UnitUtils.getUnit(input[i].getUnit()).getDerivedUnit().toString());
                        }
                    }
                }
            }
            TimeData outTd;
            boolean equalveclen = true;
            int vl = input[0].getVectorLength();
            int maxvl = vl;
            for (int i = 1; i < nargs; i++) {
                int tmp = input[i].getVectorLength();
                if (vl != tmp) {
                    equalveclen = false;
                }
                if (maxvl < tmp) {
                    maxvl = tmp;
                }
                if (!equalveclen && maxvl > 1 && tmp > 1 && maxvl != tmp) {
                    throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length.");
                }
            }
            if (equalveclen && maxvl == 1) {
                TimeData[] td = new TimeData[nargs];
                LargeArray[] la = new LargeArray[nargs];
                double[][] physMappingCoeffs = new double[nargs][];
                ArrayList<Float> timeSeriesTmp = new ArrayList<>();
                for (int i = 0; i < nargs; i++) {
                    td[i] = input[i].getTimeData();
                    timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
                    physMappingCoeffs[i] = input[i].getPhysicalMappingCoefficients();
                }
                ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                for (Float time : timeSeries) {
                    for (int i = 0; i < nargs; i++) {
                        la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
                    }
                    LargeArray res = LargeArrayOperator.MULT.evaluate(outType.toLargeArrayType(), la);
                    dataSeries.add(res);
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else {
                final boolean equalveclenf = equalveclen;
                final int maxvlf = maxvl;
                Function<byte[][], byte[]> logicFun = (in) -> {
                    int ndata = in.length;
                    byte[] out;
                    if (equalveclenf) { // dot product
                        out = new byte[1];
                        for (int v = 0; v < maxvlf; v++) {
                            byte tmp = 1;
                            for (int i = 0; i < ndata; i++) {
                                tmp *= in[i][v];
                            }
                            out[0] += tmp;
                        }
                    } else {
                        out = new byte[maxvlf];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = 1;
                        }
                        for (int i = 0; i < ndata; i++) {
                            byte[] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[0];
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[v];
                                }
                            }
                        }
                    }
                    return out;
                };
                Function<short[][], short[]> unsignedByteShortFun = (in) -> {
                    int ndata = in.length;
                    short[] out;
                    if (equalveclenf) { // dot product
                        out = new short[1];
                        for (int v = 0; v < maxvlf; v++) {
                            short tmp = 1;
                            for (int i = 0; i < ndata; i++) {
                                tmp *= in[i][v];
                            }
                            out[0] += tmp;
                        }
                    } else {
                        out = new short[maxvlf];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = 1;
                        }
                        for (int i = 0; i < ndata; i++) {
                            short[] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[0];
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[v];
                                }
                            }
                        }
                    }
                    return out;
                };
                Function<int[][], int[]> intFun = (in) -> {
                    int ndata = in.length;
                    int[] out;
                    if (equalveclenf) { // dot product
                        out = new int[1];
                        for (int v = 0; v < maxvlf; v++) {
                            int tmp = 1;
                            for (int i = 0; i < ndata; i++) {
                                tmp *= in[i][v];
                            }
                            out[0] += tmp;
                        }
                    } else {
                        out = new int[maxvlf];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = 1;
                        }
                        for (int i = 0; i < ndata; i++) {
                            int[] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[0];
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[v];
                                }
                            }
                        }
                    }
                    return out;
                };
                Function<long[][], long[]> longFun = (in) -> {
                    int ndata = in.length;
                    long[] out;
                    if (equalveclenf) { // dot product
                        out = new long[1];
                        for (int v = 0; v < maxvlf; v++) {
                            long tmp = 1;
                            for (int i = 0; i < ndata; i++) {
                                tmp *= in[i][v];
                            }
                            out[0] += tmp;
                        }
                    } else {
                        out = new long[maxvlf];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = 1;
                        }
                        for (int i = 0; i < ndata; i++) {
                            long[] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[0];
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[v];
                                }
                            }
                        }
                    }
                    return out;
                };
                Function<float[][], float[]> floatFun = (in) -> {
                    int ndata = in.length;
                    float[] out;
                    if (equalveclenf) { // dot product
                        out = new float[1];
                        for (int v = 0; v < maxvlf; v++) {
                            float tmp = 1;
                            for (int i = 0; i < ndata; i++) {
                                tmp *= in[i][v];
                            }
                            out[0] += tmp;
                        }
                    } else {
                        out = new float[maxvlf];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = 1;
                        }
                        for (int i = 0; i < ndata; i++) {
                            float[] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[0];
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[v];
                                }
                            }
                        }
                    }
                    return out;
                };
                Function<double[][], double[]> doubleFun = (in) -> {
                    int ndata = in.length;
                    double[] out;
                    if (equalveclenf) { // dot product
                        out = new double[1];
                        for (int v = 0; v < maxvlf; v++) {
                            double tmp = 1;
                            for (int i = 0; i < ndata; i++) {
                                tmp *= in[i][v];
                            }
                            out[0] += tmp;
                        }
                    } else {
                        out = new double[maxvlf];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v] = 1;
                        }
                        for (int i = 0; i < ndata; i++) {
                            double[] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[0];
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] *= elem[v];
                                }
                            }
                        }
                    }
                    return out;
                };
                Function<float[][][], float[][]> complexFloatFun = (in) -> {
                    int ndata = in.length;
                    float[][] out;
                    if (equalveclenf) { // dot product
                        out = new float[1][2];
                        for (int v = 0; v < maxvlf; v++) {
                            float[] tmp = {1, 0};
                            for (int i = 0; i < ndata; i++) {
                                tmp = LargeArrayArithmetics.complexMult(tmp, in[i][v]);
                            }
                            out[0] = LargeArrayArithmetics.complexAdd(out[0], tmp);
                        }
                    } else {
                        out = new float[maxvlf][2];
                        for (int v = 0; v < maxvlf; v++) {
                            out[v][0] = 1;
                            out[v][1] = 0;
                        }
                        for (int i = 0; i < ndata; i++) {
                            float[][] elem = in[i];
                            if (elem.length == 1) {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] = LargeArrayArithmetics.complexMult(out[v], elem[0]);
                                }
                            } else {
                                for (int v = 0; v < maxvlf; v++) {
                                    out[v] = LargeArrayArithmetics.complexMult(out[v], elem[v]);
                                }
                            }
                        }
                    }
                    return out;
                };
                outTd = evaluateVarArgFunction(input, outType, equalveclen ? 1 : maxvl, logicFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, null);
            }
            return DataArray.create(outTd, equalveclen ? 1 : maxvl, "MULT", outUnit != null ? outUnit.toString() : "1", null);
        }
        //</editor-fold>
    },
    NEG()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(final DataArrayType outputType, final DataArray[] args, final boolean ignoreUnits
        )
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, false, false);
            DataArray[] input = new DataArray[1];
            String outUnit = processUnits(args, input, ignoreUnits);
            TimeData outTd;
            double[] physMappingCoeffs = input[0].getPhysicalMappingCoefficients();
            TimeData td = input[0].getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.neg(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, input[0].getVectorLength(), "NEG", outUnit, input[0].getUserData());
        }
        //</editor-fold>
    },
    POW()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args,
                                  boolean ignoreUnits
        )
        {
            DataArrayType outType = checkArguments(outputType, args, 2, -1, false, false);
            DataArray in1 = args[0];
            DataArray in2 = args[1];
            int vl1 = in1.getVectorLength();
            int vl2 = in2.getVectorLength();
            final int maxvlf = FastMath.max(vl1, vl2);
            TimeData outTd;
            double n = 0;
            boolean constCase = false;
            if (in2.isConstant() && !in2.isTimeDependant()) {
                n = in2.getDoubleElement(0)[0];
                constCase = true;
            }
            String outUnit = constCase ? in1.getUnit() : "1";
            if (!ignoreUnits && !(in1.isUnitless() && in2.isUnitless()) && !constCase) {
                throw new IllegalArgumentException("Cannot perform this operation on two arrays with units.");
            } else if (constCase) {
                if (!(ignoreUnits || in1.isUnitless())) {
                    if (!in1.isUnitless() && n == FastMath.round(n)) {
                        try {
                            Unit unit = UnitUtils.getUnit(in1.getUnit());
                            outUnit = UnitUtils.getDerivedUnit(unit.raiseTo((int) n).toString());
                            in1 = UnitUtils.deepUnitConvertD(in1, unit.getDerivedUnit().toString());
                        } catch (OperationException ex) {
                            throw new IllegalArgumentException("Invalid unit " + in1.getUnit());
                        }
                    } else if (!in1.isUnitless() && n != FastMath.round(n)) {
                        throw new IllegalArgumentException("Cannot raise unit " + in1.getUnit() + " to a non-integer exponent " + n);
                    }
                }
                double[] physMappingCoeffs = in1.getPhysicalMappingCoefficients();
                TimeData td = in1.getTimeData();
                ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
                ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                for (Float time : timeSeries) {
                    LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                    dataSeries.add(LargeArrayArithmetics.pow(a, n, outType.toLargeArrayType()));
                }
                outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            } else {
                if (vl1 == vl2) {
                    TimeData td1 = in1.getTimeData();
                    TimeData td2 = in2.getTimeData();
                    double[] physMappingCoeffs1 = in1.getPhysicalMappingCoefficients();
                    double[] physMappingCoeffs2 = in2.getPhysicalMappingCoefficients();
                    ArrayList<Float> timeSeriesTmp = new ArrayList<>();
                    timeSeriesTmp.addAll((ArrayList<Float>) td1.getTimesAsList().clone());
                    timeSeriesTmp.addAll((ArrayList<Float>) td2.getTimesAsList().clone());
                    ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
                    ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
                    for (Float time : timeSeries) {
                        LargeArray la1 = td1.getPhysicalValue(time, physMappingCoeffs1);
                        LargeArray la2 = td2.getPhysicalValue(time, physMappingCoeffs2);
                        LargeArray res = LargeArrayArithmetics.pow(la1, la2, outType.toLargeArrayType());
                        dataSeries.add(res);
                    }
                    outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
                } else if ((vl1 == 1 && vl2 > 1) || (vl1 > 1 && vl2 == 1)) {
                    Function<byte[][], byte[]> logicFun = (in) -> {
                        byte[] out = new byte[maxvlf];
                        byte[] elem1 = in[0];
                        byte[] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (byte) (FastMath.pow(elem1[0], elem2[v]));
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (byte) (FastMath.pow(elem1[v], elem2[0]));
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (byte) (FastMath.pow(elem1[v], elem2[v]));
                            }
                        }
                        return out;
                    };
                    Function<short[][], short[]> unsignedByteShortFun = (in) -> {
                        short[] out = new short[maxvlf];
                        short[] elem1 = in[0];
                        short[] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (short) (FastMath.pow(elem1[0], elem2[v]));
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (short) (FastMath.pow(elem1[v], elem2[0]));
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (short) (FastMath.pow(elem1[v], elem2[v]));
                            }
                        }
                        return out;
                    };
                    Function<int[][], int[]> intFun = (in) -> {
                        int[] out = new int[maxvlf];
                        int[] elem1 = in[0];
                        int[] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (int) (FastMath.pow(elem1[0], elem2[v]));
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (int) (FastMath.pow(elem1[v], elem2[0]));
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (int) (FastMath.pow(elem1[v], elem2[v]));
                            }
                        }
                        return out;
                    };
                    Function<long[][], long[]> longFun = (in) -> {
                        long[] out = new long[maxvlf];
                        long[] elem1 = in[0];
                        long[] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (long) (FastMath.pow(elem1[0], elem2[v]));
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (long) (FastMath.pow(elem1[v], elem2[0]));
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (long) (FastMath.pow(elem1[v], elem2[v]));
                            }
                        }
                        return out;
                    };
                    Function<float[][], float[]> floatFun = (in) -> {
                        float[] out = new float[maxvlf];
                        float[] elem1 = in[0];
                        float[] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (float) (FastMath.pow(elem1[0], elem2[v]));
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (float) (FastMath.pow(elem1[v], elem2[0]));
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = (float) (FastMath.pow(elem1[v], elem2[v]));
                            }
                        }
                        return out;
                    };
                    Function<double[][], double[]> doubleFun = (in) -> {
                        double[] out = new double[maxvlf];
                        double[] elem1 = in[0];
                        double[] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = FastMath.pow(elem1[0], elem2[v]);
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = FastMath.pow(elem1[v], elem2[0]);
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = FastMath.pow(elem1[v], elem2[v]);
                            }
                        }
                        return out;
                    };
                    Function<float[][][], float[][]> complexFloatFun = (in) -> {
                        float[][] out = new float[maxvlf][];
                        float[][] elem1 = in[0];
                        float[][] elem2 = in[1];
                        if (elem1.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = LargeArrayArithmetics.complexPow(elem1[0], elem2[v]);
                            }
                        } else if (elem2.length == 1) {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = LargeArrayArithmetics.complexPow(elem1[v], elem2[0]);
                            }
                        } else {
                            for (int v = 0; v < maxvlf; v++) {
                                out[v] = LargeArrayArithmetics.complexPow(elem1[v], elem2[v]);
                            }
                        }
                        return out;
                    };
                    outTd = evaluateVarArgFunction(new DataArray[]{in1, in2}, outType, maxvlf, logicFun, unsignedByteShortFun, unsignedByteShortFun, intFun, longFun, floatFun, doubleFun, complexFloatFun, null);
                } else {
                    throw new IllegalArgumentException("Cannot perform this operation on two vectors of different length.");
                }
            }
            return DataArray.create(outTd, maxvlf, "POW", outUnit, null);
        }
        //</editor-fold>
    },
    SIGNUM()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(final DataArrayType outputType, final DataArray[] args, final boolean ignoreUnits
        )
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, false, false);
            if (args[0].getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                throw new IllegalArgumentException("Cannot perform this operation on a complex array.");
            }
            String outUnit = ignoreUnits ? args[0].getUnit() : "1";
            TimeData outTd;
            double[] physMappingCoeffs = args[0].getPhysicalMappingCoefficients();
            TimeData td = args[0].getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.signum(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, args[0].getVectorLength(), "SIGNUM", outUnit, args[0].getUserData());
        }
        //</editor-fold>
    },
    SIN()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args,
                                  boolean ignoreUnits
        )
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.sin(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "SIN", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    SQRT()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args,
                                  boolean ignoreUnits
        )
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.sqrt(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "SQRT", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    },
    TAN()
    //<editor-fold defaultstate="collapsed"> 
    {
        @Override
        public DataArray evaluate(DataArrayType outputType, DataArray[] args,
                                  boolean ignoreUnits
        )
        {
            DataArrayType outType = checkArguments(outputType, args, 1, -1, true, false);
            DataArray in = args[0];
            if (!ignoreUnits && !in.isUnitless()) {
                throw new IllegalArgumentException("Cannot perform this operation on data array with units");
            }
            TimeData outTd;
            double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
            TimeData td = in.getTimeData();
            ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
            ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
            for (Float time : timeSeries) {
                LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
                dataSeries.add(LargeArrayArithmetics.tan(a, outType.toLargeArrayType()));
            }
            outTd = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
            return DataArray.create(outTd, in.getVectorLength(), "TAN", in.getUnit(), in.getUserData());
        }
        //</editor-fold>
    };

    /**
     * Abstract methods for defining arithmetical operations on DataArrays.
     *
     * @param outputType  output array type
     * @param args        arguments
     * @param ignoreUnits if true, then units are ignored
     *
     * @return output array
     */
    abstract public DataArray evaluate(DataArrayType outputType,
                                       DataArray[] args, boolean ignoreUnits
    );

    public static DataArrayType getLargestNumericType(DataArray[] args)
    {
        DataArrayType largestType;
        if (args == null || args.length == 0) {
            throw new IllegalArgumentException("Invalid arguments.");
        }
        largestType = args[0].getType();
        if (!largestType.isNumericType()) {
            throw new IllegalArgumentException("Only numeric types are supported.");
        }
        for (int i = 1; i < args.length; i++) {
            if (!args[i].getType().isNumericType()) {
                throw new IllegalArgumentException("Only numeric types are supported.");
            }
            if (args[i].getType().ordinal() > largestType.ordinal()) {
                largestType = args[i].getType();
            }
        }
        return largestType;
    }

    private static DataArrayType checkArguments(DataArrayType outputType, DataArray[] args, int expectedExactLength, int expectedMinimumLength, boolean floatingPointOperation, boolean complexToReal)
    {
        if (outputType != null && !outputType.isNumericType()) {
            throw new IllegalArgumentException("Invalid output type.");
        }
        if (expectedExactLength > 0) {
            if (args == null || args.length != expectedExactLength) {
                throw new IllegalArgumentException("Invalid arguments.");
            }
        } else {
            if (args == null || args.length < expectedMinimumLength) {
                throw new IllegalArgumentException("Invalid arguments.");
            }
        }
        DataArrayType outType = outputType;
        if (outType == null) {
            outType = selectBestNumericOutputType(args, floatingPointOperation, complexToReal);
        }
        return outType;
    }

    private static DataArrayType selectBestNumericOutputType(DataArray[] args, boolean floatingPointOperation, boolean complexToReal)
    {
        DataArrayType largestInType = getLargestNumericType(args);

        if (floatingPointOperation && largestInType.toLargeArrayType().isIntegerNumericType()) {
            if (largestInType.equals(DataArrayType.FIELD_DATA_LONG)) {
                return DataArrayType.FIELD_DATA_DOUBLE;
            } else {
                return DataArrayType.FIELD_DATA_FLOAT;
            }
        }
        if (largestInType == DataArrayType.FIELD_DATA_COMPLEX && complexToReal) {
            return DataArrayType.FIELD_DATA_FLOAT;
        }
        return largestInType;
    }

    private static TimeData evaluateSingleArgFunction(DataArray in,
                                                      DataArrayType outType,
                                                      Function<byte[], Byte> logicFun,
                                                      Function<short[], Short> unsignedByteFun,
                                                      Function<short[], Short> shortFun,
                                                      Function<int[], Integer> intFun,
                                                      Function<long[], Long> longFun,
                                                      Function<float[], Float> floatFun,
                                                      Function<double[], Double> doubleFun,
                                                      Function<float[][], float[]> complexFloatFun,
                                                      Function<float[][], Float> complexFloatRealFun)
    {

        if (in == null) {
            throw new IllegalArgumentException("Input array is null.");
        }
        if (!in.isNumeric()) {
            throw new IllegalArgumentException("Input array is non-numeric.");
        }
        if (!outType.isNumericType()) {
            throw new IllegalArgumentException("Output type is not numeric.");
        }

        switch (outType) {
            case FIELD_DATA_LOGIC:
                return evaluateSingleArgFunctionLogic(in, outType, logicFun);
            case FIELD_DATA_BYTE:
                return evaluateSingleArgFunctionUnsignedByte(in, outType, unsignedByteFun);
            case FIELD_DATA_SHORT:
                return evaluateSingleArgFunctionShort(in, outType, shortFun);
            case FIELD_DATA_INT:
                return evaluateSingleArgFunctionInteger(in, outType, intFun);
            case FIELD_DATA_LONG:
                return evaluateSingleArgFunctionLong(in, outType, longFun);
            case FIELD_DATA_FLOAT:
                if (in.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                    return evaluateSingleArgFunctionComplexFloatReal(in, outType, complexFloatRealFun);
                } else {
                    return evaluateSingleArgFunctionFloat(in, outType, floatFun);
                }
            case FIELD_DATA_DOUBLE:
                return evaluateSingleArgFunctionDouble(in, outType, doubleFun);
            case FIELD_DATA_COMPLEX:
                return evaluateSingleArgFunctionComplexFloat(in, outType, complexFloatFun);
            default:
                throw new IllegalArgumentException("Not supported output type.");
        }
    }

    private static void applyLogicFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<byte[], Byte> fun)
    {
        byte[] elem = new byte[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getByte(i * veclen + v);
            }
            out.setByte(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionLogic(DataArray in, DataArrayType outType, Function<byte[], Byte> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyLogicFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyLogicFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyLogicFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyUnsignedByteFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<short[], Short> fun)
    {
        short[] elem = new short[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getUnsignedByte(i * veclen + v);
            }
            out.setUnsignedByte(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionUnsignedByte(DataArray in, DataArrayType outType, Function<short[], Short> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyUnsignedByteFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyUnsignedByteFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyUnsignedByteFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyShortFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<short[], Short> fun)
    {
        short[] elem = new short[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getShort(i * veclen + v);
            }
            out.setShort(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionShort(DataArray in, DataArrayType outType, Function<short[], Short> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyShortFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyShortFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyShortFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyIntegerFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<int[], Integer> fun)
    {
        int[] elem = new int[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getInt(i * veclen + v);
            }
            out.setInt(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionInteger(DataArray in, DataArrayType outType, Function<int[], Integer> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyIntegerFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyIntegerFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyIntegerFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyLongFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<long[], Long> fun)
    {
        long[] elem = new long[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getLong(i * veclen + v);
            }
            out.setLong(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionLong(DataArray in, DataArrayType outType, Function<long[], Long> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyLongFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyLongFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyLongFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyFloatFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<float[], Float> fun)
    {
        float[] elem = new float[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getFloat(i * veclen + v);
            }
            out.setFloat(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionFloat(DataArray in, DataArrayType outType, Function<float[], Float> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyFloatFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyFloatFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyFloatFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyDoubleFun(LargeArray out, LargeArray in, int veclen, long firstIdx, long lastIdx, Function<double[], Double> fun)
    {
        double[] elem = new double[veclen];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getDouble(i * veclen + v);
            }
            out.setDouble(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionDouble(DataArray in, DataArrayType outType, Function<double[], Double> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyDoubleFun(res, a, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyDoubleFun(res, a, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyDoubleFun(res, a, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyComplexFloatFun(LargeArray out, ComplexFloatLargeArray in, int veclen, long firstIdx, long lastIdx, Function<float[][], float[]> fun)
    {
        ComplexFloatLargeArray outComplex = (ComplexFloatLargeArray) out;
        float[][] elem = new float[veclen][];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getComplexFloat(i * veclen + v);
            }
            outComplex.setComplexFloat(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionComplexFloat(DataArray in, DataArrayType outType, Function<float[][], float[]> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            ComplexFloatLargeArray aComplex = (ComplexFloatLargeArray) LargeArrayUtils.convert(a, LargeArrayType.COMPLEX_FLOAT);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyComplexFloatFun(res, aComplex, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyComplexFloatFun(res, aComplex, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyComplexFloatFun(res, aComplex, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyComplexFloatRealFun(LargeArray out, ComplexFloatLargeArray in, int veclen, long firstIdx, long lastIdx, Function<float[][], Float> fun)
    {
        float[][] elem = new float[veclen][];
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int v = 0; v < veclen; v++) {
                elem[v] = in.getComplexFloat(i * veclen + v);
            }
            out.setFloat(i, fun.apply(elem));
        }
    }

    private static TimeData evaluateSingleArgFunctionComplexFloatReal(DataArray in, DataArrayType outType, Function<float[][], Float> fun)
    {
        TimeData td = in.getTimeData();
        ArrayList<Float> timeSeries = (ArrayList<Float>) td.getTimesAsList().clone();
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        final int veclen = in.getVectorLength();
        double[] physMappingCoeffs = in.getPhysicalMappingCoefficients();
        LargeArrayType largeArrayOutType = outType.toLargeArrayType();

        for (Float time : timeSeries) {
            final LargeArray a = td.getPhysicalValue(time, physMappingCoeffs);
            ComplexFloatLargeArray aComplex = (ComplexFloatLargeArray) LargeArrayUtils.convert(a, LargeArrayType.COMPLEX_FLOAT);
            long length = a.length() / veclen;
            final LargeArray res = LargeArrayUtils.create(largeArrayOutType, length, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyComplexFloatRealFun(res, aComplex, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyComplexFloatRealFun(res, aComplex, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyComplexFloatRealFun(res, aComplex, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static String processUnits(DataArray[] in, DataArray[] out, boolean ignoreUnits)
    {
        String outUnit = null;
        if (!ignoreUnits) {
            boolean unitless = in[0].isUnitless();
            if (!unitless) {
                outUnit = UnitUtils.getDerivedUnit(in[0].getUnit());
                out[0] = UnitUtils.deepUnitConvertD(in[0], outUnit);
            } else {
                out[0] = in[0];
            }

            for (int i = 1; i < in.length; i++) {
                if ((unitless && !in[i].isUnitless()) || (!unitless && in[i].isUnitless())) {
                    throw new IllegalArgumentException("Incompatible or invalid units: " + outUnit + " and " + in[i].getUnit());
                } else if (unitless && in[i].isUnitless()) {
                    out[i] = in[i];
                } else {
                    if (!UnitUtils.areValidAndCompatibleUnits(outUnit, in[i].getUnit())) {
                        throw new IllegalArgumentException("Incompatible or invalid units: " + outUnit + " and " + in[i].getUnit());
                    }
                    out[i] = UnitUtils.deepUnitConvertD(in[i], outUnit);
                }
            }
        } else {
            for (int i = 0; i < in.length; i++) {
                out[i] = in[i];
            }
            outUnit = "1";
        }
        return outUnit;
    }

    private static void applyLogicFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<byte[][], byte[]> fun)
    {
        int nData = in.length;
        byte[][] args = new byte[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new byte[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getByte(i * inVeclen[n] + v);
                }
            }
            byte[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setByte(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionLogic(DataArray[] in, DataArrayType outType, int outVecLength, Function<byte[][], byte[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyLogicFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyLogicFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyLogicFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyUnsignedByteFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<short[][], short[]> fun)
    {
        int nData = in.length;
        short[][] args = new short[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new short[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getUnsignedByte(i * inVeclen[n] + v);
                }
            }
            short[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setUnsignedByte(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionUnsignedByte(DataArray[] in, DataArrayType outType, int outVecLength, Function<short[][], short[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyUnsignedByteFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyUnsignedByteFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyUnsignedByteFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyShortFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<short[][], short[]> fun)
    {
        int nData = in.length;
        short[][] args = new short[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new short[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getShort(i * inVeclen[n] + v);
                }
            }
            short[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setShort(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionShort(DataArray[] in, DataArrayType outType, int outVecLength, Function<short[][], short[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyShortFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyShortFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyShortFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyIntegerFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<int[][], int[]> fun)
    {
        int nData = in.length;
        int[][] args = new int[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new int[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getInt(i * inVeclen[n] + v);
                }
            }
            int[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setInt(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionInteger(DataArray[] in, DataArrayType outType, int outVecLength, Function<int[][], int[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyIntegerFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyIntegerFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyIntegerFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyLongFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<long[][], long[]> fun)
    {
        int nData = in.length;
        long[][] args = new long[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new long[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getLong(i * inVeclen[n] + v);
                }
            }
            long[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setLong(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionLong(DataArray[] in, DataArrayType outType, int outVecLength, Function<long[][], long[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyLongFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyLongFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyLongFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyFloatFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<float[][], float[]> fun)
    {
        int nData = in.length;
        float[][] args = new float[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new float[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getFloat(i * inVeclen[n] + v);
                }
            }
            float[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setFloat(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionFloat(DataArray[] in, DataArrayType outType, int outVecLength, Function<float[][], float[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyFloatFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyFloatFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyFloatFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyDoubleFun(LargeArray out, LargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<double[][], double[]> fun)
    {
        int nData = in.length;
        double[][] args = new double[nData][];
        for (int n = 0; n < nData; n++) {
            args[n] = new double[inVeclen[n]];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getDouble(i * inVeclen[n] + v);
                }
            }
            double[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setDouble(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionDouble(DataArray[] in, DataArrayType outType, int outVecLength, Function<double[][], double[]> fun)
    {

        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        LargeArray[] la = new LargeArray[ndata];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = td[i].getPhysicalValue(time, physMappingCoeffs[i]);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyDoubleFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyDoubleFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyDoubleFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyComplexFloatFun(LargeArray out, ComplexFloatLargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<float[][][], float[][]> fun)
    {
        int nData = in.length;
        ComplexFloatLargeArray outComplex = (ComplexFloatLargeArray) out;
        float[][][] args = new float[nData][][];
        for (int n = 0; n < nData; n++) {
            args[n] = new float[inVeclen[n]][];
        };
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getComplexFloat(i * inVeclen[n] + v);
                }
            }
            float[][] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                outComplex.setComplexFloat(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionComplexFloat(DataArray[] in, DataArrayType outType, int outVecLength, Function<float[][][], float[][]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        ComplexFloatLargeArray[] la = new ComplexFloatLargeArray[in.length];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = (ComplexFloatLargeArray) LargeArrayUtils.convert(td[i].getPhysicalValue(time, physMappingCoeffs[i]), LargeArrayType.COMPLEX_FLOAT);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyComplexFloatFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyComplexFloatFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyComplexFloatFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static void applyComplexFloatRealFun(LargeArray out, ComplexFloatLargeArray[] in, int[] inVeclen, long firstIdx, long lastIdx, Function<float[][][], float[]> fun)
    {
        int nData = in.length;
        float[][][] args = new float[nData][][];
        for (int n = 0; n < nData; n++) {
            args[n] = new float[inVeclen[n]][];
        }
        for (long i = firstIdx; i < lastIdx; i++) {
            for (int n = 0; n < nData; n++) {
                for (int v = 0; v < inVeclen[n]; v++) {
                    args[n][v] = in[n].getComplexFloat(i * inVeclen[n] + v);
                }
            }
            float[] elem = fun.apply(args);
            int outVeclen = elem.length;
            for (int j = 0; j < outVeclen; j++) {
                out.setFloat(i * outVeclen + j, elem[j]);
            }
        }
    }

    private static TimeData evaluateVarArgFunctionComplexFloatReal(DataArray[] in, DataArrayType outType, int outVecLength, Function<float[][][], float[]> fun)
    {
        int ndata = in.length;
        TimeData[] td = new TimeData[ndata];
        ComplexFloatLargeArray[] la = new ComplexFloatLargeArray[in.length];
        final int[] veclen = new int[ndata];
        double[][] physMappingCoeffs = new double[ndata][];
        long length = 0;
        ArrayList<Float> timeSeriesTmp = new ArrayList<>();
        for (int i = 0; i < ndata; i++) {
            td[i] = in[i].getTimeData();
            timeSeriesTmp.addAll((ArrayList<Float>) td[i].getTimesAsList().clone());
            veclen[i] = in[i].getVectorLength();
            physMappingCoeffs[i] = in[i].getPhysicalMappingCoefficients();
        }
        ArrayList<Float> timeSeries = new ArrayList(new TreeSet<>(timeSeriesTmp));
        ArrayList<LargeArray> dataSeries = new ArrayList<>(timeSeries.size());
        length = in[0].getNElements();
        for (Float time : timeSeries) {
            for (int i = 0; i < ndata; i++) {
                la[i] = (ComplexFloatLargeArray) LargeArrayUtils.convert(td[i].getPhysicalValue(time, physMappingCoeffs[i]), LargeArrayType.COMPLEX_FLOAT);
            }
            final LargeArray res = LargeArrayUtils.create(outType.toLargeArrayType(), length * outVecLength, false);
            int nthreads = (int) FastMath.min(length, ConcurrencyUtils.getNumberOfThreads());
            if (nthreads < 2 || length < ConcurrencyUtils.getConcurrentThreshold()) {
                applyComplexFloatRealFun(res, la, veclen, 0, length, fun);
            } else {
                Future[] threads = new Future[nthreads];
                for (int j = 0; j < nthreads; j++) {
                    final long firstIdx = (j * length) / nthreads;
                    final long lastIdx = ((j + 1) * length) / nthreads;
                    threads[j] = ConcurrencyUtils.submit(() -> {
                        applyComplexFloatRealFun(res, la, veclen, firstIdx, lastIdx, fun);
                    });
                }
                try {
                    ConcurrencyUtils.waitForCompletion(threads);
                } catch (InterruptedException | ExecutionException ex) {
                    applyComplexFloatRealFun(res, la, veclen, 0, length, fun);
                }
            }
            dataSeries.add(res);
        }
        return new TimeData(timeSeries, dataSeries, timeSeries.get(0));
    }

    private static TimeData evaluateVarArgFunction(DataArray[] arrays,
                                                   DataArrayType outType,
                                                   int outVecLength,
                                                   Function<byte[][], byte[]> logicFun,
                                                   Function<short[][], short[]> unsignedByteFun,
                                                   Function<short[][], short[]> shortFun,
                                                   Function<int[][], int[]> intFun,
                                                   Function<long[][], long[]> longFun,
                                                   Function<float[][], float[]> floatFun,
                                                   Function<double[][], double[]> doubleFun,
                                                   Function<float[][][], float[][]> complexFloatFun,
                                                   Function<float[][][], float[]> complexFloatRealFun)
    {

        if (arrays == null || arrays.length == 0) {
            throw new IllegalArgumentException("Input arrays are null or empty.");
        }
        long length = arrays[0].getNElements();
        for (DataArray array : arrays) {
            if (array == null || !array.isNumeric() || array.getNElements() != length) {
                throw new IllegalArgumentException("Some input arrays are null, non-numeric, or have wrong number of elements.");
            }
        }
        if (!outType.isNumericType()) {
            throw new IllegalArgumentException("Output type is not numeric.");
        }

        switch (outType) {
            case FIELD_DATA_LOGIC:
                return evaluateVarArgFunctionLogic(arrays, outType, outVecLength, logicFun);
            case FIELD_DATA_BYTE:
                return evaluateVarArgFunctionUnsignedByte(arrays, outType, outVecLength, unsignedByteFun);
            case FIELD_DATA_SHORT:
                return evaluateVarArgFunctionShort(arrays, outType, outVecLength, shortFun);
            case FIELD_DATA_INT:
                return evaluateVarArgFunctionInteger(arrays, outType, outVecLength, intFun);
            case FIELD_DATA_LONG:
                return evaluateVarArgFunctionLong(arrays, outType, outVecLength, longFun);
            case FIELD_DATA_FLOAT:
                if (getLargestNumericType(arrays) == DataArrayType.FIELD_DATA_COMPLEX) {
                    return evaluateVarArgFunctionComplexFloatReal(arrays, outType, outVecLength, complexFloatRealFun);
                } else {
                    return evaluateVarArgFunctionFloat(arrays, outType, outVecLength, floatFun);
                }
            case FIELD_DATA_DOUBLE:
                return evaluateVarArgFunctionDouble(arrays, outType, outVecLength, doubleFun);
            case FIELD_DATA_COMPLEX:
                return evaluateVarArgFunctionComplexFloat(arrays, outType, outVecLength, complexFloatFun);
            default:
                throw new IllegalArgumentException("Not supported output type.");
        }
    }

}

/*
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jscic.cells;

/**
 * Regular hexahedron cell. Used to describe a cell in a regular field.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class RegularHex extends Hex
{

    private static final long serialVersionUID = 8746967192446934788L;

    private boolean even;

    /**
     * Constructor of a regular hexahedron.
     *
     * <pre>
     *  i4----------i7
     *  |\          |\
     *  |  \           \
     *  |    \      |    \
     *  |     i5----------i6
     *  |     |     |     |
     *  |     |           |
     *  i0- - | - - i3    |
     *   \    |      \    |
     *     \  |        \  |
     *       \|          \|
     *        i1----------i2
     *
     *
     * Indices are ordered so that
     *  i0&lt;(i1,i2,i3,i4,i5,i6,i7), i1&lt;i3&lt;i4
     * </pre>
     *
     * @param i0          node index
     * @param i1          node index
     * @param i2          node index
     * @param i3          node index
     * @param i4          node index
     * @param i5          node index
     * @param i6          node index
     * @param i7          node index
     * @param orientation significant if cell is of dim nspace or is a face of a cell of dim nspace,
     *                    true if vectors <code>i1-i0, i3-i0, i4-i0 </code>form positively oriented reper
     * @param even        is parity of i0 + i1 + i2 (indices of the cell in mesh)
     */
    public RegularHex(int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7,
                      byte orientation, boolean even)
    {
        super(i0, i1, i2, i3, i4, i5, i6, i7, orientation);
        this.even = even;
    }

    /**
     * Constructor of a regular hexahedron.
     *
     * <pre>
     *  i4----------i7
     *  |\          |\
     *  |  \           \
     *  |    \      |    \
     *  |     i5----------i6
     *  |     |     |     |
     *  |     |           |
     *  i0- - | - - i3    |
     *   \    |      \    |
     *     \  |        \  |
     *       \|          \|
     *        i1----------i2
     *
     *
     * Indices are ordered so that
     *  i0&lt;(i1,i2,i3,i4,i5,i6,i7), i1&lt;i3&lt;i4
     * </pre>
     *
     * @param i0   node index
     * @param i1   node index
     * @param i2   node index
     * @param i3   node index
     * @param i4   node index
     * @param i5   node index
     * @param i6   node index
     * @param i7   node index
     * @param even is parity of i0 + i1 + i2 (indices of the cell in mesh)
     */
    public RegularHex(int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, boolean even)
    {
        this(i0, i1, i2, i3, i4, i5, i6, i7, (byte) 1, even);
    }

    @Override
    public Cell cloneDeep()
    {
        return new RegularHex(vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], vertices[6], vertices[7], orientation, even);
    }

    @Override
    public Cell[] triangulation()
    {
        if (even)
            return new Tetra[]{
                new Tetra(vertices[0], vertices[2], vertices[7], vertices[5], orientation),
                new Tetra(vertices[1], vertices[2], vertices[0], vertices[5], orientation),
                new Tetra(vertices[3], vertices[0], vertices[2], vertices[7], orientation),
                new Tetra(vertices[6], vertices[5], vertices[7], vertices[2], orientation),
                new Tetra(vertices[5], vertices[4], vertices[7], vertices[0], orientation)
            };
        else
            return new Tetra[]{
                new Tetra(vertices[1], vertices[3], vertices[4], vertices[6], orientation),
                new Tetra(vertices[0], vertices[1], vertices[3], vertices[4], orientation),
                new Tetra(vertices[2], vertices[3], vertices[1], vertices[6], orientation),
                new Tetra(vertices[5], vertices[4], vertices[1], vertices[6], orientation),
                new Tetra(vertices[7], vertices[6], vertices[4], vertices[3], orientation)
            };
    }

    @Override
    public int[][] triangulationVertices()
    {
        if (even)
            return new int[][]{{vertices[0], vertices[2], vertices[7], vertices[5]},
                               {vertices[1], vertices[2], vertices[0], vertices[5]},
                               {vertices[3], vertices[0], vertices[2], vertices[7]},
                               {vertices[6], vertices[5], vertices[7], vertices[2]},
                               {vertices[5], vertices[4], vertices[7], vertices[0]}};
        else
            return new int[][]{{vertices[1], vertices[3], vertices[4], vertices[6]},
                               {vertices[0], vertices[1], vertices[3], vertices[4]},
                               {vertices[2], vertices[3], vertices[1], vertices[6]},
                               {vertices[5], vertices[4], vertices[1], vertices[6]},
                               {vertices[7], vertices[6], vertices[4], vertices[3]}};
    }

    /**
     * Generates the array of tetrahedra vertices from the array of regular hex
     * vertices.
     *
     * @param vertices array of vertices
     * @param even     selects one of two possible triangulations.
     *                 For a hexahedron at (i,j,k) position in a regular field, even is true if i+j+k is even, false otherwise.
     *
     * @return array of tetrahedra vertices
     */
    public static int[][] triangulationVertices(int[] vertices, boolean even)
    {
        if (even)
            return new int[][]{{vertices[0], vertices[2], vertices[7], vertices[5]},
                               {vertices[1], vertices[2], vertices[0], vertices[5]},
                               {vertices[3], vertices[0], vertices[2], vertices[7]},
                               {vertices[6], vertices[5], vertices[7], vertices[2]},
                               {vertices[5], vertices[4], vertices[7], vertices[0]}};
        else
            return new int[][]{{vertices[1], vertices[3], vertices[4], vertices[6]},
                               {vertices[0], vertices[1], vertices[3], vertices[4]},
                               {vertices[2], vertices[3], vertices[1], vertices[6]},
                               {vertices[5], vertices[4], vertices[1], vertices[6]},
                               {vertices[7], vertices[6], vertices[4], vertices[3]}};
    }

    /**
     * Generates the array of indices of tetrahedra vertices in triangulation of the
     * hexahedron in a regular field.
     *
     * @param even selects one of two possible triangulations.
     *             For a hexahedron at (i,j,k) position in a regular field, even is true if i+j+k is even, false otherwise.
     *
     * @return array of indices of tetrahedra vertices in triangulation
     */
    public static int[][] triangulationIndices(boolean even)
    {
        if (even)
            return new int[][]{{0, 2, 7, 5},
                               {1, 2, 0, 5},
                               {3, 0, 2, 7},
                               {6, 5, 7, 2},
                               {5, 4, 7, 0}};
        else
            return new int[][]{{1, 3, 4, 6},
                               {0, 1, 3, 4},
                               {2, 3, 1, 6},
                               {5, 4, 1, 6},
                               {7, 6, 4, 3}};
    }

}
